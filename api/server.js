'use strict';

const express = require('express');
const fetch = require('isomorphic-fetch');
// const router = require('express').Router();
const bodyParser = require('body-parser');
const methodOverride = require('method-override');
const { verifyToken } = require('./middleware/identity');
// const request = require('request');
// const fs = require('fs');
require('dotenv').config({
    path: `${process.cwd()}/.env${process.env.NODE_ENV === 'development' ? '.dev' : ''}`
});
const port = process.env.PORT || 3003
// const sslPort = process.env.SSL_PORT || 3446
// const ssl_port = process.env.SSL_PORT || 3002
let app = express(),
    // httpServer = require('http').createServer(app),
    // httpsServer = require('https').createServer({
    //     key: fs.readFileSync(`${process.cwd()}/ssl/redcode.vn.key`),
    //     cert: fs.readFileSync(`${process.cwd()}/ssl/redcode.vn.pem`),
    //     requestCert: false,
    //     rejectUnauthorized: false
    // }, app),
    config = require('config'),
    cors = require('cors'),
    db = require('./db'),
    helmet = require('helmet');
// const helper = require('./lib/helper');
// var cluster = require('cluster');
const { errorHandler } = require('./middleware');
// const restrict = require('./middleware/restrict-access');

// const RESTRICTED_HOSTS = config.get('restrictedHosts');

const UPLOAD_PATH = `./uploads`;

// server.setSecure(credentials);

// if (cluster.isMaster) {
//     cluster.fork();
//     cluster.on('exit', function(worker) {
//         console.log('Worker %d died', worker.id);
//         cluster.fork();
//     });
// } else if (cluster.isWorker) {

app.use(helmet());
app.use(bodyParser.urlencoded({ limit: '50mb', extended: true, keepExtensions: true, uploadDir: UPLOAD_PATH }));
app.use(bodyParser.json({ limit: '50mb', extended: true }));
app.use(cors());
app.use(methodOverride());
app.use(errorHandler);
app.use('/uploads', express.static(UPLOAD_PATH));

app.use('/mbnd/', verifyToken, (req, res) => {
    console.log(process.env.MBND_DOMAIN + req.url);

    fetch(process.env.MBND_DOMAIN + req.url, {
        method: req.method,
        headers: {
            Authorization: process.env.MBND_TOKEN,
            "Content-Type": "application/json",
            "Accept": "application/json"
        },
        body: ['PUT', 'POST'].some(m => m === req.method) ? JSON.stringify(req.body) : undefined
    }).then(proxyRes =>
        proxyRes.text().then((json) => res.status(proxyRes.status).send(json))
    )
});

// app.use('/*', restrict(RESTRICTED_HOSTS));

app.get('/', function (req, res, next) {
    res.status(200).send('Welcome to Savills APIs.');
});

require('./routes')(app);

// httpsServer.listen(sslPort, function(){
//     console.log(`Server start ssl on ${sslPort}`);
// });

app.listen(port, function () {
    console.log(`Server start on ${port}`);
});

//     console.log('Worker %d running!', cluster.worker.id);
//     process.on('uncaughtException', function(err) {
//         console.log('Uncaught Exception');
//         console.log(err);
//         process.exit(1);
//     });
// }

process.on('uncaughtException', err => console.error(err.stack));
