const Project =  require('../models/project');
const User =  require('../models/user');
const Organization =  require('../models/organization');
const path = require('path');
const fs = require('fs');
const shell = require('shelljs');
const mongoose = require('mongoose');
const { generateIdSync } = require('../lib/helper');

module.exports = {
    create: function(req, res, next) {
        try {
            let { language } = req;
            const _id = generateIdSync(6);
            // const _id = mongoose.Types.ObjectId();
            // let photos = [];
            // req.body.photos.forEach(p => {
            //     const fileName = path.basename(p);
            //     // const newBaseDir = `${process.cwd()}/uploads/projects/${_id}`;
            //     const newBaseDir = `/uploads/projects/${_id}`;
            //     if(!fs.existsSync(newBaseDir)) shell.mkdir('-p', `${process.cwd()}${newBaseDir}`);
            //     const newPath = `${newBaseDir}/${fileName}`;
            //     // fs.renameSync(`${process.cwd()}${p}`, `${process.cwd()}${newPath}`);
            //     shell.mv('-n', `${process.cwd()}${p}`, `${process.cwd()}${newPath}`);
            //     photos.push(newPath);
            // });
            let { approval, photos, photo, ...otherProps } = req.body;
            if(photos && photos.length > 0 && !photo) photo = photos[0];
            let data = {
                ...otherProps,
                _id,
                photos,
                photo,
                createdBy: req.user._id,
                // updatedBy: req.user._id,
                seller: req.user._id,
                agency: req.user.agency._id
            };
            Project.create(data, { language }, (err, doc, affected) => {
                if(err) return res.status(500).json({ message: err.message || 'Something went wrong' });
                if(affected > 0) res.status(200).json({
                    message: 'Success',
                    result: {
                        _id: doc._id
                    }
                });
                else res.status(500).json({
                    message: 'Something went wrong'
                });
            });
        } catch (err) {
            next(err);
        }
    },
    get: async function(req, res, next) {
        try {
            let { pagination, language, queries, user, credential } = req;
            let { featured, exclude, keyword, sortByStartDate, title, type, createdByEmail, sellerByEmail, agencyByEmail, invesloper, ...otherQueries } = queries || {};
            let moreQueries = {...otherQueries};
            let noResult = false;
            // if(credential) {
            //     moreQueries['_id'] = {
            //         '$in': credential.config.projects.ids
            //     }
            // }
            if(user) {
                // if(!req.master) moreQueries['createdBy'] = user._id;
                // // else moreQueries['agency'] = user.agency._id
                // // moreQueries['createdBy'] = user._id;
                moreQueries['agency'] = user.agency._id
            } else {
                const today = new Date(Date.now());
                moreQueries['publishingStatus'] = 1; //published
                moreQueries['approval'] = 1;
                moreQueries['startDate'] = { $lte: today };
                moreQueries['$or'] = [
                    {
                        $and: [
                            {
                                endDate: {
                                    $exists: true
                                }
                            },
                            {
                                endDate: {
                                    $gte: today
                                }
                            }
                        ]
                    },
                    {
                        endDate: {
                            $exists: false
                        }
                    },
                    {
                        endDate: null
                    }
                ];
            }
            if(exclude) moreQueries['_id'] = {
                $ne: exclude
            };
            if(!!keyword) moreQueries['$or'] = [
                {
                    title: new RegExp(`.*${keyword.trim().toLowerCase()}.*`, 'iu')
                },
                {
                    'location.formattedAddress': new RegExp(`.*${keyword.trim().toLowerCase()}.*`, 'iu')
                }
            ];
            if(!!title) moreQueries['title'] = new RegExp(`.*${title.toLowerCase()}.*`, 'iu');
            // if(!!location) location.forEach(l => {
            //     moreQueries[`location.${l}`] = location[l];
            // });
            if(!!type) moreQueries['type'] = {
                $in: type.split(',')
            };
            if(!!createdByEmail) {
                const rs = await User.findOne({
                    email: createdByEmail
                });
                if(rs) moreQueries['createdBy'] = rs._id;
                else noResult = true;
            }
            if(!!sellerByEmail) {
                const rs = await User.findOne({
                    email: sellerByEmail
                });
                if(rs) moreQueries['seller'] = rs._id;
                else noResult = true;
            }
            if(!!agencyByEmail) {
                const rs = await Organization.findOne({
                    email: agencyByEmail
                });
                if(rs) moreQueries['agency'] = rs._id;
                else noResult = true;
            }
            if(!!invesloper) moreQueries['inveslopers'] = invesloper;
            if(!!sortByStartDate) moreOpts.sort = {
                'startDate': parseInt(sortByStartDate)
            };
            if(!!noResult) res.status(200).json({
                meta: {
                    total: 0,
                    pages: 1,
                    page: pagination.page,
                    limit: pagination.limit
                },
                result: []
            });
            else Project['featured' in queries ? 'getHighlights' : 'get'](moreQueries, { pagination, language }, (err, list) => {
                if(err) return res.status(500).json({ message: err.message || 'Something went wrong' });
                let { docs, ...meta } = list;
                res.status(200).json({
                    meta: docs ? meta : {},
                    result: docs || list
                });
            });
        } catch (err) {
            next(err);
        }
    },
    search: async function(req, res, next) {
        try {
            let { pagination, language, queries, user, credential } = req;
            let { keyword } = queries || {};
            let moreQueries = {};
            if(credential) {
                moreQueries['_id'] = {
                    '$in': credential.config.projects.ids
                }
            }
            if(user) {
                // if(!req.master) moreQueries['createdBy'] = user._id;
                // // else moreQueries['agency'] = user.agency._id
                // // moreQueries['createdBy'] = user._id;
                moreQueries['agency'] = user.agency._id
            } else {
                const today = new Date(Date.now());
                moreQueries['publishingStatus'] = 1; //published
                moreQueries['approval'] = 1;
                moreQueries['startDate'] = { $lte: today };
                moreQueries['$or'] = [
                    {
                        $and: [
                            {
                                endDate: {
                                    $exists: true
                                }
                            },
                            {
                                endDate: {
                                    $gte: today
                                }
                            }
                        ]
                    },
                    {
                        endDate: {
                            $exists: false
                        }
                    },
                    {
                        endDate: null
                    }
                ];
            }
            if(!!keyword) moreQueries['title'] = new RegExp(`.*${keyword.trim().toLowerCase()}.*`, 'iu');
            Project.get(moreQueries, { pagination, language }, (err, list) => {
                if(err) return res.status(500).json({ message: err.message || 'Something went wrong' });
                let { docs, ...meta } = list;
                res.status(200).json({
                    meta: docs ? meta : {},
                    result: docs || list
                });
            });
        } catch (err) {
            next(err);
        }
    },
    getById: function(req, res, next) {
        try {
            let { id, language } = req;
            Project.getById(id, { language }, (err, doc) => {
                if(err) return res.status(err.code || 500).json({ message: err.message || 'Something went wrong' });
                res.status(200).json({
                    result: doc
                });
            });
        } catch (err) {
            next(err);
        }
    },
    delete: function(req, res, next) {
        try {
            let { id } = req;
            Project.delete(id, (err, raw) => {
                if(err) return res.status(500).json({ message: err.message || 'Something went wrong' });
                res.status(500).json({
                    message: 'Fail',
                });
            });
        } catch (err) {
            next(err);
        }
    },
    deleteMany: function(req, res, next) {
        try {
            let { deleted } = req.headers;
            const deletedList = deleted.split(',');
            Project.deleteMany(deletedList, (err, raw) => {
                if(err) return res.status(500).json({ message: err.message || 'Something went wrong' });
                if(raw.ok) res.status(200).json({
                    message: 'Success',
                });
                else res.status(500).json({
                    message: 'Fail',
                });
            });
        } catch (err) {
            next(err);
        }
    },
    partialUpdate: function(req, res, next) {
        try {
            let { id, language, user } = req;
            const { approval, publishingStatus, ...otherProps } = req.body;
            let data = {
                ...otherProps,
                updatedBy: user._id,
            };
            Project.partialUpdate(id, data, { language }, (err, raw) => {
                if(err) return res.status(500).json({ message: err.message || 'Something went wrong' });
                res.status(200).json({
                    message: 'Success',
                });
            });
        } catch (err) {
            next(err);
        }
    },
    update: function(req, res, next) {
        try {
            let { id, language, user } = req;
            const { approval, publishingStatus, ...otherProps } = req.body;
            let data = {
                ...otherProps,
                updatedBy: user._id,
            };
            Project.fullUpdate(id, data, { language }, (err, raw) => {
                if(err) return res.status(500).json({ message: err.message || 'Something went wrong' });
                res.status(200).json({
                    message: 'Success',
                });
            });
        } catch (err) {
            next(err);
        }
    },
    publishing: function(req, res, next) {
        try {
            let { id } = req;
            let publishingStatus = req.headers['publishing-status'];
            if(!publishingStatus) return res.status(400).json({
                message: 'publishing-status is required',
            });
            Project.publishing(id, parseInt(publishingStatus), (err, raw) => {
                if(err) return res.status(500).json({ message: err.message || 'Something went wrong' });
                res.status(200).json({
                    message: 'Success',
                });
            });
        } catch (err) {
            next(err);
        }
    },
    approveMany: function(req, res, next) {
        try {
            let { approved } = req.headers;
            const approvedList = approved.split(',');
            Project.approveMany(approvedList, (err, raw) => {
                if(err) return res.status(500).json({ message: err.message || 'Something went wrong' });
                if(raw.ok) res.status(200).json({
                    message: 'Success',
                });
                else res.status(500).json({
                    message: 'Fail',
                });
            });
        } catch (err) {
            next(err);
        }
    },
    approvingForWebsites: function(req, res, next) {
        try {
            let { id, user } = req;
            const { websites, removedWebsites, seller } = req.body;
            let data = {
                websites,
                removedWebsites,
                updatedBy: user._id,
                seller
            };
            Project.approvingForWebsites(id, data, (err, raw) => {
                if(err) return res.status(500).json({ message: err.message || 'Something went wrong' });
                res.status(200).json({
                    message: 'Success',
                });
            });
        } catch (err) {
            next(err);
        }
    },
};