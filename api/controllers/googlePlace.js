const request = require('request');
const config = require('config');
const apiKey = config.get('GoogleApiKeys.Places');

module.exports = {
    search: function(req, res, next) {
        try {
            let { queries } = req;
            request(`https://maps.googleapis.com/maps/api/place/findplacefromtext/json?input=${encodeURIComponent(queries.query)}&inputtype=textquery&fields=formatted_address,name,geometry&key=${apiKey}`, function(error, response, body) {
                if(error) console.log(error);
                if(response) {
                    res.send({
                        code: response.code,
                        message: response.statusMessage,
                        result: JSON.parse(body)
                    });
                }
            });
        } catch (err) {
            next(err);
        }
    },
};