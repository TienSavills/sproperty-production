const fs = require('fs');
const ejs = require('ejs');
const sendEmail = require('../lib/utils/mail-sender');
const { generatePassword, forEachAsync } = require('../lib/helper');
const User =  require('../models/user');
// const UserRole =  require('../models/userRole');
const Organization =  require('../models/organization');
const config = require('config');

module.exports = {
    getProfile: function(req, res, next) {
        try {
            const id = config.get('defaultOrganization.id')
            Organization.getById(id, function(err, profile) {
                if(err) return res.status(500).json({ message: err.message || 'Something went wrong' });
                res.status(200).json({
                    result: profile
                });
            });
        } catch (err) {
            next(err);
        }
    },
    partialUpdate: function(req, res, next) {
        try {
            const id = config.get('defaultOrganization.id')
            Organization.partialUpdate(id, req.body, (err, doc) => {
                if(err) return res.status(500).json({ message: err.message || 'Something went wrong' });
                res.status(200).json({
                    message: 'Success'
                });
            });
        } catch (err) {
            next(err);
        }
    },
};