const Module =  require('../models/module');

module.exports = {
    get: function(req, res, next) {
        try {
            let { pagination, user, queries } = req;
            let moreQueries = {};
            if(!('master' in queries)) moreQueries.system = false;
            Module.get(moreQueries, { pagination }, (err, list) => {
                if(err) return res.status(500).json({ message: err.message || 'Something went wrong' });
                let { docs, ...meta } = list;
                res.status(200).json({
                    meta: docs ? meta : {},
                    result: docs || list
                });
            });
        } catch (err) {
            next(err);
        }
    }
}