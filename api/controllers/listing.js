const Listing =  require('../models/listing');
const path = require('path');
const fs = require('fs');
const shell = require('shelljs');
const { generateIdSync } = require('../lib/helper');
const ejs = require('ejs');
const sendEmail = require('../lib/utils/mail-sender');
const config = require('config');

module.exports = {
    create: function(req, res, next) {
        try {
            const defaultOrganization = config.get('defaultOrganization');
            let { language } = req;
            Listing.create({
                ...req.body,
                agency: defaultOrganization._id
            }, { language }, (err, doc, affected) => {
                if(err) return res.status(500).json({ message: err.message || 'Something went wrong' });
                if(affected > 0) {
                    let email_template = fs.readFileSync(`${__dirname}/../views/email/listing-success.ejs`, 'utf-8');
                    sendEmail(
                        [
                            {
                                name: req.body.fullname,
                                email: req.body.email
                            }
                        ],
                        'Listing successful',
                        ejs.render(email_template, {
                            name: req.body.fullname
                        }),
                        true,
                        // [
                        //     {
                        //         fileName: 'cvi-policy.pdf',
                        //         pathName: outputPath,
                        //         mimeType: 'application/pdf'
                        //     },
                        //     {
                        //         fileName: 'cvi-benefits.pdf',
                        //         pathName: `${process.cwd()}/media/cvi-benefits.pdf`,
                        //         mimeType: 'application/pdf'
                        //     }
                        // ],
                        [],
                        function(err, inf) {
                            if(err) console.log(err);
                            // console.log(inf);
                        }
                    );
                    res.status(200).json({
                        message: 'Success',
                        result: {
                            _id: doc._id
                        }
                    });
                }
                else res.status(500).json({
                    message: 'Something went wrong'
                });
            });
        } catch (err) {
            next(err);
        }
    },
    get: function(req, res, next) {
        try {
            let { pagination, language, queries, user } = req;
            let moreQueries = {...queries};
            if(user) {
                if(!user.roles.some(r => r.level <= 2)) moreQueries['seller'] = user._id;
                else if(!user.roles.some(r => r.level <= 1)) moreQueries['agency'] = user.agency._id;
            }
            Listing.get(moreQueries, { pagination, language }, (err, list) => {
                if(err) return res.status(500).json({ message: err.message || 'Something went wrong' });
                let { docs, ...meta } = list;
                res.status(200).json({
                    meta: docs ? meta : {},
                    result: docs || list
                });
            });
        } catch (err) {
            next(err);
        }
    },
    getById: function(req, res, next) {
        try {
            let { id, language } = req;
            Listing.getById(id, { language }, (err, doc) => {
                if(err) return res.status(500).json({ message: err.message || 'Something went wrong' });
                res.status(200).json({
                    result: doc
                });
            });
        } catch (err) {
            next(err);
        }
    },
    forwarding: function(req, res, next) {
        try {
            let { id } = req;
            let to = req.headers['to'];
            if(!to) return res.status(400).json({
                message: 'Forwarding seller is required',
            });
            Listing.forwarding(id, to, (err, raw) => {
                if(err) return res.status(500).json({ message: err.message || 'Something went wrong' });
                res.status(200).json({
                    message: 'Success',
                });
            });
        } catch (err) {
            next(err);
        }
    },
};