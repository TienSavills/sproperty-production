const Popup =  require('../models/popup');

module.exports = {
    create: function(req, res, next) {
        try {
            // let { language } = req;
            Popup.create(req.body, (err, doc) => {
                if(err) return res.status(500).json({ message: err.message || 'Something went wrong' });
                res.status(200).json({
                    message: 'Success',
                    result: {
                        _id: doc._id
                    }
                });
            });
        } catch (err) {
            next(err);
        }
    },
    get: function(req, res, next) {
        try {
            let {  pagination, language, user } = req;
            let moreQueries = {};
            if(!user) {
                const today = new Date(Date.now());
                moreQueries['publishingStatus'] = 1; //published
                // moreQueries['approval'] = 1;
                moreQueries['startDate'] = { $lte: today };
                moreQueries['$or'] = [
                    {
                        $and: [
                            {
                                endDate: {
                                    $exists: true
                                }
                            },
                            {
                                endDate: {
                                    $gte: today
                                }
                            }
                        ]
                    },
                    {
                        endDate: {
                            $exists: false
                        }
                    },
                    {
                        endDate: null
                    }
                ];
            }
            Popup.get(moreQueries, { pagination, language }, (err, list) => {
                if(err) return res.status(500).json({ message: err.message || 'Something went wrong' });
                let { docs, ...meta } = list;
                res.status(200).json({
                    meta: docs ? meta : {},
                    result: docs || list
                });
            });
        } catch (err) {
            next(err);
        }
    },
    getById: function(req, res, next) {
        try {
            let { id } = req;
            Popup.getById(id, (err, doc) => {
                if(err) return res.status(500).json({ message: err.message || 'Something went wrong' });
                res.status(200).json({
                    result: doc
                });
            });
        } catch (err) {
            next(err);
        }
    },
    delete: function(req, res, next) {
        try {
            let { id } = req;
            Popup.delete(id, (err, raw) => {
                if(err) return res.status(500).json({ message: err.message || 'Something went wrong' });
                res.status(500).json({
                    message: 'Fail',
                });
            });
        } catch (err) {
            next(err);
        }
    },
    deleteMany: function(req, res, next) {
        try {
            let { deleted } = req.headers;
            const deletedList = deleted.split(',');
            Popup.deleteMany(deletedList, (err, raw) => {
                if(err) return res.status(500).json({ message: err.message || 'Something went wrong' });
                if(raw.ok) res.status(200).json({
                    message: 'Success',
                });
                else res.status(500).json({
                    message: 'Fail',
                });
            });
        } catch (err) {
            next(err);
        }
    },
    partialUpdate: function(req, res, next) {
        try {
            let { id, language, user } = req;
            const { approval, publishingStatus, ...otherProps } = req.body;
            let data = {
                ...otherProps,
                updatedBy: user._id,
            };
            Popup.partialUpdate(id, data, (err, raw) => {
                if(err) return res.status(500).json({ message: err.message || 'Something went wrong' });
                res.status(200).json({
                    message: 'Success',
                });
            });
        } catch (err) {
            next(err);
        }
    },
    import: function(req, res, next) {
        try {
            Popup.import(req.body, (err, result, affected) => {
                if(err) return res.status(500).json({ message: err.message || 'Something went wrong' });
                res.status(200).json({
                    message: 'Success'
                });
            });
        } catch (err) {
            next(err);
        }
    },
    publishing: function(req, res, next) {
        try {
            let { id } = req;
            let publishingStatus = req.headers['publishing-status'];
            if(!publishingStatus) return res.status(400).json({
                message: 'publishing-status is required',
            });
            Popup.publishing(id, parseInt(publishingStatus), (err, raw) => {
                if(err) return res.status(500).json({ message: err.message || 'Something went wrong' });
                res.status(200).json({
                    message: 'Success',
                });
            });
        } catch (err) {
            next(err);
        }
    },
};