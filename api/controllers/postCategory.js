const PostCategory =  require('../models/postCategory');

module.exports = {
    create: function(req, res, next) {
        try {
            let { language } = req;
            PostCategory.create(req.body, { language }, (err, doc, affected) => {
                if(err) return res.status(500).json({ message: err.message || 'Something went wrong' });
                if(affected > 0) {
                    res.status(200).json({
                        message: 'Success',
                        result: {
                            _id: doc._id
                        }
                    });
                }
                else {
                    res.status(500).json({
                        message: 'Something went wrong'
                    });
                }
            });
        } catch (err) {
            next(err);
        }
    },
    get: function(req, res, next) {
        try {
            const { module, queries, pagination, language } = req;
            const { parent } = queries;
            let moreQueries = { module };
            if(parent) moreQueries.parent = parseInt(parent);
            if('root' in queries) moreQueries['$or'] = [
                {
                    parent: {
                        $exists: false
                    }
                },
                {
                    parent: null
                }
            ];
            let options = { language };
            if(!('tree' in queries)) options.pagination = pagination;
            PostCategory['tree' in queries ? 'getTree' : 'get'](moreQueries, options, (err, list) => {
                if(err) return res.status(500).json({ message: err.message || 'Something went wrong' });
                if('tree' in queries) res.status(200).json({
                    result: list
                });
                else {
                    let { docs, ...meta } = list;
                    res.status(200).json({
                        meta: docs ? meta : {},
                        result: docs || list
                    });
                }
            });
        } catch (err) {
            next(err);
        }
    },
    getById: function(req, res, next) {
        try {
            let { id, language } = req;
            PostCategory.getById(id, { language }, (err, doc) => {
                if(err) return res.status(500).json({ message: err.message || 'Something went wrong' });
                res.status(200).json({
                    result: doc
                });
            });
        } catch (err) {
            next(err);
        }
    },
    delete: function(req, res, next) {
        try {
            let { id } = req;
            PostCategory.delete(id, (err, raw) => {
                if(err) return res.status(500).json({ message: err.message || 'Something went wrong' });
                res.status(500).json({
                    message: 'Fail',
                });
            });
        } catch (err) {
            next(err);
        }
    },
    deleteMany: function(req, res, next) {
        try {
            let { deleted } = req.headers;
            const deletedList = deleted.split(',');
            PostCategory.deleteMany(deletedList, (err, raw) => {
                if(err) return res.status(500).json({ message: err.message || 'Something went wrong' });
                if(raw.ok) res.status(200).json({
                    message: 'Success',
                });
                else res.status(500).json({
                    message: 'Fail',
                });
            });
        } catch (err) {
            next(err);
        }
    },
    partialUpdate: function(req, res, next) {
        try {
            let { id, language, user } = req;
            const { approval, publishingStatus, ...otherProps } = req.body;
            let data = {
                ...otherProps,
                updatedBy: user._id,
            };
            PostCategory.partialUpdate(id, data, { language }, (err, raw) => {
                if(err) return res.status(500).json({ message: err.message || 'Something went wrong' });
                res.status(200).json({
                    message: 'Success',
                });
            });
        } catch (err) {
            next(err);
        }
    },
    publishing: function(req, res, next) {
        try {
            let { id } = req;
            let publishingStatus = req.headers['publishing-status'];
            if(!publishingStatus) return res.status(400).json({
                message: 'publishing-status is required',
            });
            PostCategory.publishing(id, parseInt(publishingStatus), (err, raw) => {
                if(err) return res.status(500).json({ message: err.message || 'Something went wrong' });
                res.status(200).json({
                    message: 'Success',
                });
            });
        } catch (err) {
            next(err);
        }
    },
};