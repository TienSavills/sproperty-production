const MappingKey = require('../models/mappingKey');

module.exports = {
    create: function (req, res, next) {
        try {
            MappingKey.create(req.body, (err, doc, affected) => {
                if (err) return res.status(500).json({ message: err.message || 'Something went wrong' });
                if (affected > 0) {
                    res.status(200).json({
                        message: 'Success',
                        result: {
                            _id: doc._id
                        }
                    });
                }
                else {
                    res.status(500).json({
                        message: 'Something went wrong'
                    });
                }
            });
        } catch (err) {
            next(err);
        }
    },
    get: function (req, res, next) {
        console.log(req.query.in)
        try {
            MappingKey.get({ _id: { $in: (req.query.in || []).split(',') } }, {}, (err, docs) => {

                if (err) return res.status(500).json({ message: err.message || 'Something went wrong' });

                res.status(200).json({
                    meta: {},
                    result: docs
                });
            });
        } catch (err) {
            next(err);
        }
    },
};