const config = require('config');
const Inbox =  require('../models/inbox');
const path = require('path');
const fs = require('fs');
const shell = require('shelljs');
const { generateIdSync } = require('../lib/helper');
const ejs = require('ejs');
const sendEmail = require('../lib/utils/mail-sender');

module.exports = {
    projectInbox: function(req, res, next) {
        const defaultOrganization = config.get('defaultOrganization');
        try {
            let { language } = req;
            Inbox.create({
                ...req.body,
                title: 'Cần tư vấn dự án',
                refModel: 'projects',
                agency: defaultOrganization._id
            }, { language }, (err, doc, affected) => {
                if(err) return res.status(500).json({ message: err.message || 'Something went wrong' });
                if(affected > 0) {
                    let res_email_template = fs.readFileSync(`${__dirname}/../views/email/inbox-success.ejs`, 'utf-8');
                    sendEmail(
                        [
                            {
                                name: doc.fullname,
                                email: doc.email
                            }
                        ],
                        'Contact delivered',
                        ejs.render(res_email_template),
                        true,
                        [],
                        function(err, inf) {
                            if(err) console.log(err);
                            // console.log(inf);
                        }
                    );
                    let email_template = fs.readFileSync(`${__dirname}/../views/email/inbox-notification.ejs`, 'utf-8');
                    sendEmail(
                        [
                            {
                                name: `${doc.to.firstName} ${doc.to.lastName}`,
                                email: doc.to.email
                            }
                        ],
                        'New inbox',
                        ejs.render(email_template, {
                            userName: `${doc.to.firstName} ${doc.to.lastName}`,
                            customerName: req.body.fullname
                        }),
                        true,
                        [],
                        function(err, inf) {
                            if(err) console.log(err);
                            // console.log(inf);
                        }
                    );
                    res.status(200).json({
                        message: 'Success',
                        result: {
                            _id: doc._id
                        }
                    });
                }
                else res.status(500).json({
                    message: 'Something went wrong'
                });
            });
        } catch (err) {
            next(err);
        }
    },
    propertyInbox: function(req, res, next) {
        try {
            const defaultOrganization = config.get('defaultOrganization');
            let { language } = req;
            Inbox.create({
                ...req.body,
                title: 'Cần tư vấn căn hộ',
                refModel: 'properties',
                agency: defaultOrganization._id
            }, { language }, (err, doc, affected) => {
                if(err) return res.status(500).json({ message: err.message || 'Something went wrong' });
                if(affected > 0) {
                    let res_email_template = fs.readFileSync(`${__dirname}/../views/email/inbox-success.ejs`, 'utf-8');
                    sendEmail(
                        [
                            {
                                name: doc.fullname,
                                email: doc.email
                            }
                        ],
                        'Contact delivered',
                        ejs.render(res_email_template),
                        true,
                        [],
                        function(err, inf) {
                            if(err) console.log(err);
                            // console.log(inf);
                        }
                    );
                    let email_template = fs.readFileSync(`${__dirname}/../views/email/inbox-notification.ejs`, 'utf-8');
                    sendEmail(
                        [
                            {
                                name: `${doc.to.firstName} ${doc.to.lastName}`,
                                email: doc.to.email
                            }
                        ],
                        'New inbox',
                        ejs.render(email_template, {
                            userName: `${doc.to.firstName} ${doc.to.lastName}`,
                            customerName: req.body.fullname
                        }),
                        true,
                        [],
                        function(err, inf) {
                            if(err) console.log(err);
                            // console.log(inf);
                        }
                    );
                    res.status(200).json({
                        message: 'Success',
                        result: {
                            _id: doc._id
                        }
                    });
                }
                else res.status(500).json({
                    message: 'Something went wrong'
                });
            });
        } catch (err) {
            next(err);
        }
    },
    contactInbox: function(req, res, next) {
        try {
            const defaultOrganization = config.get('defaultOrganization');
            let { language } = req;
            Inbox.create({
                ...req.body,
                // title: 'Cần tư vấn',
                title: 'Liên hệ',
                agency: defaultOrganization._id
            }, { language }, (err, doc, affected) => {
                if(err) return res.status(500).json({ message: err.message || 'Something went wrong' });
                if(affected > 0) {
                    let res_email_template = fs.readFileSync(`${__dirname}/../views/email/inbox-success.ejs`, 'utf-8');
                    sendEmail(
                        [
                            {
                                name: doc.fullname,
                                email: doc.email
                            }
                        ],
                        'Contact delivered',
                        ejs.render(res_email_template),
                        true,
                        [],
                        function(err, inf) {
                            if(err) console.log(err);
                            // console.log(inf);
                        }
                    );
                    res.status(200).json({
                        message: 'Success',
                        result: {
                            _id: doc._id
                        }
                    });
                }
                else res.status(500).json({
                    message: 'Something went wrong'
                });
            });
        } catch (err) {
            next(err);
        }
    },
    get: function(req, res, next) {
        try {
            let { pagination, language, queries, user } = req;
            let moreQueries = {...queries};
            if(user) {
                if(!user.roles.some(r => !!r.master)) moreQueries['to'] = user._id;
                else moreQueries['agency'] = user.agency._id
            }
            Inbox.get(moreQueries, { pagination, language }, (err, list) => {
                if(err) return res.status(500).json({ message: err.message || 'Something went wrong' });
                let { docs, ...meta } = list;
                res.status(200).json({
                    meta: docs ? meta : {},
                    result: docs || list
                });
            });
        } catch (err) {
            next(err);
        }
    },
    getById: function(req, res, next) {
        try {
            let { id, language } = req;
            Inbox.getById(id, { language }, (err, doc) => {
                if(err) return res.status(500).json({ message: err.message || 'Something went wrong' });
                res.status(200).json({
                    result: doc
                });
            });
        } catch (err) {
            next(err);
        }
    },
    forwarding: function(req, res, next) {
        try {
            let { id } = req;
            let to = req.headers['to'];
            if(!to) return res.status(400).json({
                message: 'Forwarding seller is required',
            });
            Inbox.forwarding(id, to, (err, raw) => {
                if(err) return res.status(500).json({ message: err.message || 'Something went wrong' });
                res.status(200).json({
                    message: 'Success',
                });
            });
        } catch (err) {
            next(err);
        }
    },
};