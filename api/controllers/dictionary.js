const Dictionary =  require('../models/dictionary');

module.exports = {
    get: function(req, res, next) {
        try {
            const { pagination, queries } = req;
            const { keyword } = queries;
            let moreQueries = {};
            if(!!keyword) moreQueries['value'] = new RegExp(`.*${keyword.trim().toLowerCase()}.*`, 'iu');
            Dictionary.get(moreQueries, { pagination }, (err, list) => {
                if(err) return res.status(500).json({ message: err.message || 'Something went wrong' });
                let { docs, ...meta } = list || {};
                res.status(200).json({
                    meta: docs ? meta : {},
                    result: docs || list || []
                });
            });
        } catch (err) {
            next(err);
        }
    },
    translate: function(req, res, next) {
        try {
            Dictionary.translate(req.body, (err, doc) => {
                if(err) return res.status(500).json({ message: err.message || 'Something went wrong' });
                res.status(200).json({
                    message: 'Success',
                });
            });
        } catch (err) {
            next(err);
        }
    },
    refresh: function(req, res, next) {
        try {
            Dictionary.refresh(req.body.languages, (err, doc) => {
                if(err) return res.status(500).json({ message: err.message || 'Something went wrong' });
                res.status(200).json({
                    message: 'Success',
                });
            });
        } catch (err) {
            next(err);
        }
    },
    delete: function(req, res, next) {
        try {
            let { id } = req;
            Dictionary.delete(id, (err, raw) => {
                if(err) return res.status(500).json({ message: err.message || 'Something went wrong' });
                res.status(500).json({
                    message: 'Fail',
                });
            });
        } catch (err) {
            next(err);
        }
    },
    deleteMany: function(req, res, next) {
        try {
            let { deleted } = req.headers;
            const deletedList = deleted.split(',');
            Dictionary.deleteMany(deletedList, (err, raw) => {
                if(err) return res.status(500).json({ message: err.message || 'Something went wrong' });
                else res.status(200).json({
                    message: 'Success',
                });
            });
        } catch (err) {
            next(err);
        }
    },
    import: function(req, res, next) {
        try {
            Dictionary.import(req.body, (err, affected) => {
                if(err) return res.status(500).json({ message: err.message || 'Something went wrong' });
                res.status(200).json({
                    message: 'Success',
                    success: `${affected} / ${req.body.length}`
                });
            });
        } catch (err) {
            next(err);
        }
    }
};