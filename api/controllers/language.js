const Language =  require('../models/language');

module.exports = {
    create: function(req, res, next) {
        try {
            Language.create(req.body, (err, doc, affected) => {
                if(err) return res.status(500).json({ message: err.message || 'Something went wrong' });
                res.status(200).json({
                    message: 'Success',
                    result: {
                        _id: doc._id
                    }
                });
            });
        } catch (err) {
            next(err);
        }
    },
    get: function(req, res, next) {
        try {
            let { pagination } = req;
            Language.get({}, { pagination }, (err, list) => {
                if(err) return res.status(500).json({ message: err.message || 'Something went wrong' });
                let { docs, ...meta } = list;
                res.status(200).json({
                    meta: docs ? meta : {},
                    result: docs || list
                });
            });
        } catch (err) {
            next(err);
        }
    },
    import: function(req, res, next) {
        try {
            Language.import(req.body, (err, result, affected) => {
                if(err) return res.status(500).json({ message: err.message || 'Something went wrong' });
                else res.status(200).json({
                    message: 'Success'
                });
            });
        } catch(err) {
            next(err);
        }
    }
};