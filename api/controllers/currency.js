const Currency =  require('../models/currency');

module.exports = {
    get: function(req, res, next) {
        try {
            let { language } = req;
            Currency.get({}, { language }, (err, list) => {
                if(err) return res.status(500).json({ message: err.message || 'Something went wrong' });
                let { docs, ...meta } = list;
                res.status(200).json({
                    meta: docs ? meta : {},
                    result: docs || list
                });
            });
        } catch (err) {
            next(err);
        }
    },
    import: function(req, res, next) {
        try {
            Currency.import(req.body, (err, result, affected) => {
                if(err) return res.status(500).json({ message: err.message || 'Something went wrong' });
                res.status(200).json({
                    message: 'Success'
                });
            });
        } catch (err) {
            next(err);
        }
    }
};