const Post =  require('../models/post');
const path = require('path');
const fs = require('fs');
const shell = require('shelljs');
const { generateIdSync } = require('../lib/helper');
const ejs = require('ejs');
const sendEmail = require('../lib/utils/mail-sender');

module.exports = {
    create: function(req, res, next) {
        try {
            let { language } = req;
            const _id = generateIdSync(6);
            let data = {
                ...req.body,
                _id,
                parent: 3,
                createdBy: req.user._id,
            };
            if(req.body.photo) {
                const fileName = path.basename(req.body.photo);
                // const newBaseDir = `${process.cwd()}/uploads/projects/${_id}`;
                const newBaseDir = `/uploads/posts/${_id}`;
                if(!fs.existsSync(newBaseDir)) shell.mkdir('-p', `${process.cwd()}${newBaseDir}`);
                const newPath = `${newBaseDir}/${fileName}`;
                // fs.renameSync(`${process.cwd()}${p}`, `${process.cwd()}${newPath}`);
                shell.mv('-n', `${process.cwd()}${req.body.photo}`, `${process.cwd()}${newPath}`);
                data['photo'] = newPath
            }
            Post.create(data, { language }, (err, doc, affected) => {
                if(err) return res.status(500).json({ message: err.message || 'Something went wrong' });
                if(affected > 0) res.status(200).json({
                    message: 'Success',
                    result: {
                        _id: doc._id
                    }
                });
                else res.status(500).json({
                    message: 'Something went wrong'
                });
            });
        } catch (err) {
            next(err);
        }
    },
    get: function(req, res, next) {
        try {
            let { pagination, language, queries } = req;
            Post.get({...queries, parent: 3}, { pagination, language }, (err, list) => {
                if(err) return res.status(500).json({ message: err.message || 'Something went wrong' });
                let { docs, ...meta } = list;
                res.status(200).json({
                    meta: docs ? meta : {},
                    result: docs || list
                });
            });
        } catch (err) {
            next(err);
        }
    },
    getById: function(req, res, next) {
        try {
            let { id, language } = req;
            Post.getById(id, { language }, (err, doc) => {
                if(err) return res.status(500).json({ message: err.message || 'Something went wrong' });
                res.status(200).json({
                    result: doc
                });
            });
        } catch (err) {
            next(err);
        }
    },
};