const PropertyType =  require('../models/propertyType');

module.exports = {
    get: function(req, res, next) {
        try {
            PropertyType.get({}, { language: req.language }, (err, docs) => {
                if(err) return res.status(500).json({ message: err.message || 'Something went wrong' });
                res.status(200).json({
                    meta: {},
                    result: docs
                });
            });
        } catch (err) {
            next(err);
        }
    },
    import: function(req, res, next) {
        try {
            PropertyType.import(req.body, (err, result, affected) => {
                if(err) return res.status(500).json({ message: err.message || 'Something went wrong' });
                res.status(200).json({
                    message: 'Success'
                });
            });
        } catch (err) {
            next(err);
        }
    }
};