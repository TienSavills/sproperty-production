const PropertySovereignty =  require('../models/propertySovereignty');

module.exports = {
    get: function(req, res, next) {
        try {
            let { pagination, language } = req;
            PropertySovereignty.get({}, { pagination, language }, (err, list) => {
                if(err) return res.status(500).json({ message: err.message || 'Something went wrong' });
                let { docs, ...meta } = list;
                res.status(200).json({
                    meta: docs ? meta : {},
                    result: docs || list
                });
            });
        } catch (err) {
            next(err);
        }
    }
};