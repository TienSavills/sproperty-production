const Media =  require('../models/media');
const path = require('path');
const fs = require('fs');
const shell = require('shelljs');
const crypto = require('crypto');
// const ffmpeg = require('fluent-ffmpeg');
const mimeTypes = {
    'photo': {
        '.png': 'image/png',
        '.jpg': 'image/jpeg'
    },
    'video': {
        '.svg': 'image/svg+xml',
        '.flv': 'video/x-flv',
        '.mp4': 'video/mp4',
        '.3gp': 'video/3gpp',
        '.mov': 'video/quicktime',
        '.avi': 'video/x-msvideo',
        '.wmv': 'video/x-ms-wmv'
    }
};

module.exports = {
    uploadPhoto: function(req, res, next) {
        try {
            const { id } = req;
            if(!req.file) return res.status(400).json({
                message: 'No file received'
            });
            if(Object.values(mimeTypes.photo).indexOf(req.file.mimetype) === -1) return res.status(415).json({
                message: 'Unsupported Media Type'
            });
            const extName = path.extname(req.file.originalname);
            const ext = !!extName ? extName : Object.keys(mimeTypes.photo).find(m => mimeTypes.photo[m] === req.file.mimetype);
            var tmp_path = req.file.path;
            crypto.pseudoRandomBytes(16, function(err, raw) {
                if (err) return res.status(500).json({ message: err.message || 'Something went wrong' });
            
                // set where the file should actually exists - in this case it is in the "images" directory
                if(!fs.existsSync('./uploads/photos')) fs.mkdirSync('./uploads/photos');
                var target_path = `./uploads/photos/${raw.toString('hex')}${ext}`;
                // move the file from the temporary location to the intended location
                // console.log(target_path);
                fs.rename(tmp_path, target_path, function(err) {
                    if (err) return res.status(500).json({ message: err.message || 'Something went wrong' });
                    // delete the temporary file, so that the explicitly set temporary upload dir does not get filled with unwanted files
                    fs.unlink(tmp_path, function() {
                        if (err) return res.status(500).json({ message: err.message || 'Something went wrong' });
                        if(id) {
                            let data = {
                                path: target_path.replace('.', ''),
                                mimeType: req.file.mimeType,
                            };
                            Media.update(id, data, (err, doc) => {
                                if(err) return res.status(500).json({ message: err.message || 'Something went wrong' });
                                res.status(200).json({
                                    message: 'Success',
                                    result: {
                                        _id: doc._id,
                                        path: doc.path
                                    }
                                });
                            });
                        } else {
                            let data = {
                                path: target_path.replace('.', ''),
                                mimeType: req.file.mimeType,
                                // createdBy: req.user._id
                            };
                            if(req.user) data.createdBy = req.user._id;
                            Media.create(data, {}, (err, doc) => {
                                if(err) return res.status(500).json({ message: err.message || 'Something went wrong' });
                                res.status(200).json({
                                    message: 'Success',
                                    result: {
                                        _id: doc._id,
                                        path: doc.path
                                    }
                                });
                            });
                        }
                        
                        // res.send('File uploaded to: ' + target_path + ' - ' + req.file.size + ' bytes');
                    });
                });
            });
        } catch (err) {
            next(err);
        }
    },
    uploadVideo: function(req, res, next) {
        try {
            if(!req.file) return res.status(400).json({
                message: 'No file received'
            });
            if(Object.values(mimeTypes.video).indexOf(req.file.mimetype) === -1) return res.status(415).json({
                message: 'Unsupported Media Type'
            });
            const extName = path.extname(req.file.originalname);
            const ext = !!extName ? extName : Object.keys(mimeTypes.video).find(m => mimeTypes.video[m] === req.file.mimetype);
            var tmp_path = req.file.path;
            crypto.pseudoRandomBytes(16, function(err, raw) {
                if (err) return res.status(500).json({ message: err.message || 'Something went wrong' });
            
                // set where the file should actually exists - in this case it is in the "images" directory
                if(!fs.existsSync('./uploads/videos')) fs.mkdirSync('./uploads/videos');
                var target_path = `./uploads/videos/${raw.toString('hex')}${ext}`;
                // move the file from the temporary location to the intended location
                // console.log(target_path);
                fs.rename(tmp_path, target_path, function(err) {
                    if (err) return res.status(500).json({ message: err.message || 'Something went wrong' });
                    // delete the temporary file, so that the explicitly set temporary upload dir does not get filled with unwanted files
                    fs.unlink(tmp_path, function() {
                        if (err) return res.status(500).json({ message: err.message || 'Something went wrong' });
                        // let vproc = new ffmpeg(target_path).takeScreenshots({
                        //     count: 1,
                        //     timemarks: [ '5' ] // number of seconds
                        // }, './uploads/videos', function(err) {
                        //     console.log('screenshots were saved')
                        // });
                        // vproc.then(function (video) {
                        //     // Callback mode
                        //     video.fnExtractFrameToJPG('./uploads/videos', {
                        //         number : 1,
                        //         file_name : raw.toString('hex')
                        //     }, function (error, files) {
                        //         console.log('Error: ' + error);
                        //         if (!error) console.log('Frames: ' + files);
                        //     });
                        // }, function (err) {
                        //     console.log('Error: ' + err);
                        // });
                        let data = {
                            path: target_path.replace('.', ''),
                            mimeType: req.file.mimetype,
                            // createdBy: req.user._id
                        };
                        if(req.user) data.createdBy = req.user._id;
                        Media.create(data, {}, (err, doc) => {
                            if(err) return res.status(500).json({ message: err.message || 'Something went wrong' });
                            res.status(200).json({
                                message: 'Success',
                                result: {
                                    _id: doc._id,
                                    path: doc.path,
                                    mimeType: doc.mimeType
                                }
                            });
                        });
                        // res.send('File uploaded to: ' + target_path + ' - ' + req.file.size + ' bytes');
                    });
                });
            });
        } catch (err) {
            next(err);
        }
    },
    get: function(req, res, next) {
        try {
            let { pagination, language, queries } = req;
            Media.get({...queries}, { pagination, language }, (err, list) => {
                if(err) return res.status(500).json({ message: err.message || 'Something went wrong' });
                let { docs, ...meta } = list;
                res.status(200).json({
                    meta: docs ? meta : {},
                    result: docs || list
                });
            });
        } catch (err) {
            next(err);
        }
    },
    getById: function(req, res, next) {
        try {
            let { id } = req;
            Media.getById(id, (err, doc) => {
                if(err) return res.status(500).json({ message: err.message || 'Something went wrong' });
                res.status(200).json({
                    result: doc
                });
            });
        } catch (err) {
            next(err);
        }
    },
};