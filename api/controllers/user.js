const fs = require('fs');
const ejs = require('ejs');
const sendEmail = require('../lib/utils/mail-sender');
const { generatePassword, forEachAsync } = require('../lib/helper');
const User =  require('../models/user');
// const UserRole =  require('../models/userRole');
const Organization =  require('../models/organization');
const config = require('config');

module.exports = {
    createMember: async function(req, res, next) {
        try {
            let { user, language } = req;
            if(!req.body.roles || req.body.roles.length < 1) return res.status(400).json({ message: 'User role is required' });
            if(req.body) {
                req.body.agent = req.headers['user-agent'];
                req.body.password = req.body.password || await generatePassword(8);
            }
            User.create({
                ...req.body,
                agency: user.agency || config.get('defaultOrganization.id'),
                createdBy: user._id
            }, { language }, (err, doc, affected) => {
                if(err) return res.status(500).json({ message: err.message || 'Something went wrong' });
                if(affected > 0) {
                    let email_template = fs.readFileSync(`${__dirname}/../views/email/user-register-success.ejs`, 'utf-8');
                    sendEmail(
                        [
                            {
                                name: `${req.body.firstName} ${req.body.lastName}`,
                                email: req.body.email
                            }
                        ],
                        'Register successful',
                        ejs.render(email_template, {
                            fullname: `${req.body.firstName} ${req.body.lastName}`,
                            password: req.body.password
                        }),
                        true,
                        [],
                        function(err, inf) {
                            if(err) console.log(err);
                            // console.log(inf);
                        }
                    );
                    res.status(200).json({
                        message: 'Success',
                        result: {
                            _id: doc._id
                        }
                    });
                }
                else {
                    res.send({
                        code: 500,
                        message: 'Tạo tài khoản không thành công.'
                    });
                }
            });
        } catch (err) {
            next(err);
        }
    },
    createAdmin: async function(req, res, next) {
        try {
            let { user } = req;
            User.findOne({
                email: req.body.email,
                active: true
            }, (err, foundUser) => {
                if(err) return res.status(500).json({ message: err.message || 'Something went wrong' });
                if(!foundUser) return res.status(500).json({ message: 'User not found' });
                User.createAdmin({
                    id: foundUser._id,
                    roles: [
                        ...foundUser.roles,
                        req.body.role
                    ]
                }, (err, doc, affected) => {
                    if(err) return res.status(500).json({ message: err.message || 'Something went wrong' });
                    res.status(200).json({
                        message: 'Success'
                    });
                });
            });
        } catch (err) {
            next(err);
        }
    },
    updateAdmin: async function(req, res, next) {
        try {
            let { user } = req;
            User.findOne({
                email: req.body.email,
                active: true
            }, async (err, foundUser) => {
                if(err) return res.status(500).json({ message: err.message || 'Something went wrong' });
                if(!foundUser) return res.status(500).json({ message: 'User not found' });
                await User.populate(foundUser, 'roles');
                const staffRole = foundUser.roles.filter(r => !r.master);
                User.updateAdmin({
                    id: foundUser._id,
                    roles: [
                        ...staffRole.map(r => r._id),
                        req.body.role
                    ]
                }, (err, doc, affected) => {
                    if(err) return res.status(500).json({ message: err.message || 'Something went wrong' });
                    res.status(200).json({
                        message: 'Success'
                    });
                });
            });
        } catch (err) {
            next(err);
        }
    },
    get: async function(req, res, next) {
        try {
            let { pagination, language, queries, user } = req;
            let { name, agencyByEmail, ...otherQueries } = queries || {};
            let moreQueries = {...otherQueries};
            let noResult = false;
            if(user) {
                if(!user.roles.some(r => r.level <= 1)) moreQueries['createdBy'] = user._id;
                else {
                    moreQueries['_id'] = {
                        '$ne': user._id
                    };
                    moreQueries['agency'] = user.agency._id || user.agency;
                    moreQueries['roles'] = {
                        '$in': ['agency-admin']
                    };
                }
            }
            if(!!name) {
                const fname = name.split(' ').join('|');
                moreQueries['$or'] = [
                    {
                        firstName: new RegExp(fname.toLowerCase(), 'iu')
                    },
                    {
                        lastName: new RegExp(fname.toLowerCase(), 'iu')
                    }
                ];
            }
            if(!!agencyByEmail) {
                const rs = await Organization.findOne({
                    email: agencyByEmail
                });
                if(rs) moreQueries['agency'] = rs._id;
                else noResult = true;
            }
            if(!!noResult) res.status(200).json({
                meta: {
                    total: 0,
                    pages: 1,
                    page: pagination.page,
                    limit: pagination.limit
                },
                result: []
            });
            else User.get(moreQueries, {
                pagination,
                language
            }, (err, list) => {
                if(err) return res.status(500).json({ message: err.message || 'Something went wrong' });
                let { docs, ...meta } = list;
                res.status(200).json({
                    meta: docs ? meta : {},
                    result: docs || list
                });
            });
        } catch (err) {
            next(err);
        }
    },
    getMembers: async function(req, res, next) {
        try {
            let { pagination, language, queries, user } = req;
            let { name, email, agencyByEmail, ...otherQueries } = queries || {};
            let moreQueries = {...otherQueries};
            let noResult = false;
            if(user) {
                moreQueries['_id'] = {
                    '$ne': user._id
                };
                moreQueries['agency'] = user.agency._id || user.agency;
            }
            // moreQueries['createdBy'] = user._id;
            if(!!name) {
                const fname = name.split(' ').join('|');
                moreQueries['$or'] = [
                    {
                        firstName: new RegExp(fname.toLowerCase(), 'iu')
                    },
                    {
                        lastName: new RegExp(fname.toLowerCase(), 'iu')
                    }
                ];
            }
            if(!!email) moreQueries['email'] = email;
            if(!!agencyByEmail) {
                const rs = await Organization.findOne({
                    email: agencyByEmail
                });
                if(rs) moreQueries['agency'] = rs._id;
                else noResult = true;
            }
            if(!!noResult) res.status(200).json({
                meta: {
                    total: 0,
                    pages: 1,
                    page: pagination.page,
                    limit: pagination.limit
                },
                result: []
            });
            else User.get(moreQueries, {
                pagination,
                language,
                // filters: {
                //     master: false
                // }
            }, (err, list) => {
                if(err) return res.status(500).json({ message: err.message || 'Something went wrong' });
                let { docs, ...meta } = list;
                res.status(200).json({
                    meta: docs ? meta : {},
                    result: docs || list
                });
            });
        } catch (err) {
            next(err);
        }
    },
    getAministrators: async function(req, res, next) {
        try {
            let { pagination, language, queries, user } = req;
            let { name, agencyByEmail, ...otherQueries } = queries || {};
            let moreQueries = {...otherQueries};
            let noResult = false;
            moreQueries['_id'] = {
                '$ne': user._id
            };
            moreQueries['agency'] = user.agency._id || user.agency;
            // moreQueries['createdBy'] = user._id;
            if(!!name) {
                const fname = name.split(' ').join('|');
                moreQueries['$or'] = [
                    {
                        firstName: new RegExp(fname.toLowerCase(), 'iu')
                    },
                    {
                        lastName: new RegExp(fname.toLowerCase(), 'iu')
                    }
                ];
            }
            if(!!agencyByEmail) {
                const rs = await Organization.findOne({
                    email: agencyByEmail
                });
                if(rs) moreQueries['agency'] = rs._id;
                else noResult = true;
            }
            if(!!noResult) res.status(200).json({
                meta: {
                    total: 0,
                    pages: 1,
                    page: pagination.page,
                    limit: pagination.limit
                },
                result: []
            });
            else User.get(moreQueries, {
                pagination,
                language,
                filters: {
                    master: true
                }
            }, (err, list) => {
                if(err) return res.status(500).json({ message: err.message || 'Something went wrong' });
                let { docs, ...meta } = list;
                res.status(200).json({
                    meta: docs ? meta : {},
                    result: docs || list
                });
            });
        } catch (err) {
            next(err);
        }
    },
    getSellers: async function(req, res, next) {
        try {
            let { pagination, language, queries, user } = req;
            let { name, email, agencyByEmail, ...otherQueries } = queries || {};
            let moreQueries = {...otherQueries};
            let noResult = false;
            if(user) {
                moreQueries['_id'] = {
                    '$ne': user._id
                };
                moreQueries['agency'] = user.agency._id || user.agency;
            }
            // moreQueries['createdBy'] = user._id;
            if(!!name) {
                const fname = name.split(' ').join('|');
                moreQueries['$or'] = [
                    {
                        firstName: new RegExp(fname.toLowerCase(), 'iu')
                    },
                    {
                        lastName: new RegExp(fname.toLowerCase(), 'iu')
                    }
                ];
            }
            if(!!email) moreQueries['email'] = email;
            if(!!agencyByEmail) {
                const rs = await Organization.findOne({
                    email: agencyByEmail
                });
                if(rs) moreQueries['agency'] = rs._id;
                else noResult = true;
            }
            if(!!noResult) res.status(200).json({
                meta: {
                    total: 0,
                    pages: 1,
                    page: pagination.page,
                    limit: pagination.limit
                },
                result: []
            });
            else User.get(moreQueries, {
                pagination,
                language,
                filters: {
                    system: false //mean sellers
                }
            }, (err, list) => {
                if(err) return res.status(500).json({ message: err.message || 'Something went wrong' });
                let { docs, ...meta } = list;
                res.status(200).json({
                    meta: docs ? meta : {},
                    result: docs || list
                });
            });
        } catch (err) {
            next(err);
        }
    },
    getById: function(req, res, next) {
        try {
            let { id, language } = req;
            User.getById(id, { language }, (err, doc) => {
                if(err) return res.status(500).json({ message: err.message || 'Something went wrong' });
                res.status(200).json({
                    result: doc
                });
            });
        } catch (err) {
            next(err);
        }
    },
    login: function(req, res, next) {
        try {
            // let ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
            var ip = req.headers['origin'] || req.headers['host']
            ip = ip.replace('https://', '').replace('http://', '')
            ip = ip.split(':')[0]
            let ua = req.headers['user-agent'];
            if(req.user) User.login({ username: req.user.username, password: req.user.password, ip, ua }, (err, token) => {
                if(err) return res.status(500).json({ message: err.message || 'Something went wrong' });
                res.status(200).json({
                    message: 'Success',
                    result: {
                        token
                    }
                });
            });
        } catch (err) {
            next(err);
        }
    },
    getProfile: function(req, res, next) {
        try {
            const { language } = req;
            User.getById(req.user._id, { language }, function(err, profile) {
                if(err) return res.status(500).json({ message: err.message || 'Something went wrong' });
                res.status(200).json({
                    result: profile
                });
            });
        } catch (err) {
            next(err);
        }
    },
    update: function(req, res, next) {
        try {
            let { id, language } = req;
            User.updateDetail(id, req.body, { language }, (err, doc) => {
                if(err) return res.status(500).json({ message: err.message || 'Something went wrong' });
                res.status(200).json({
                    message: 'Success'
                });
            });
        } catch (err) {
            next(err);
        }
    },
    resetPassword: async function(req, res, next) {
        try {
            let { id } = req;
            const newPassword = req.headers['new-password'];
            let password = newPassword || await generatePassword(8);
            User.resetPassword(id, password, (err, doc) => {
                if(err) return res.status(500).json({ message: err.message || 'Something went wrong' });
                let email_template = fs.readFileSync(`${__dirname}/../views/email/user-reset-password-success.ejs`, 'utf-8');
                sendEmail(
                    [
                        {
                            name: `${doc.firstName} ${doc.lastName}`,
                            email: doc.email
                        }
                    ],
                    'Reset password successful',
                    ejs.render(email_template, {
                        fullname: `${doc.firstName} ${doc.lastName}`,
                        password
                    }),
                    true,
                    [],
                    function(err, inf) {
                        if(err) console.log(err);
                        // console.log(inf);
                    }
                );
                res.status(200).json({
                    message: 'Success'
                });
            });
        } catch (err) {
            next(err);
        }
    },
    lock: function(req, res, next) {
        try {
            let { id } = req;
            User.lock(id, (err, doc) => {
                if(err) return res.status(500).json({ message: err.message || 'Something went wrong' });
                res.status(200).json({
                    message: 'Success'
                });
            });
        } catch (err) {
            next(err);
        }
    },
    unlock: function(req, res, next) {
        try {
            let { id } = req;
            User.unlock(id, (err, doc) => {
                if(err) return res.status(500).json({ message: err.message || 'Something went wrong' });
                res.status(200).json({
                    message: 'Success'
                });
            });
        } catch (err) {
            next(err);
        }
    },
    delete: function(req, res, next) {
        try {
            let { id } = req;
            User.delete(id, (err, raw) => {
                if(err) return res.status(500).json({ message: err.message || 'Something went wrong' });
                res.status(500).json({
                    message: 'Fail',
                });
            });
        } catch (err) {
            next(err);
        }
    },
    deleteMany: function(req, res, next) {
        try {
            let { deleted } = req.headers;
            const deletedList = deleted.split(',');
            User.deleteMany(deletedList, (err, raw) => {
                if(err) return res.status(500).json({ message: err.message || 'Something went wrong' });
                if(raw.ok) res.status(200).json({
                    message: 'Success',
                });
                else res.status(500).json({
                    message: 'Fail',
                });
            });
        } catch (err) {
            next(err);
        }
    },
    deleteManyAdmin: function(req, res, next) {
        try {
            let { deleted } = req.headers;
            const deletedList = deleted.split(',');
            User.find({
                _id: {
                    $in: deletedList
                }
            }, async (err, foundUsers) => {
                await forEachAsync(foundUsers, async fu => {
                    await User.populate(fu, 'roles');
                    const staffRole = fu.roles.filter(r => !r.master);
                    await User.updateAdmin({
                        id: fu._id,
                        roles: staffRole
                    }, () => {});
                });
                res.status(200).json({
                    message: 'Success'
                });
            });
        } catch (err) {
            next(err);
        }
    },
};