const Menu =  require('../models/menu');

module.exports = {
    create: function(req, res, next) {
        try {
            Menu.create(req.body, (err, doc, affected) => {
                if(err) return res.status(500).json({ message: err.message || 'Something went wrong' });
                if(affected > 0) {
                    res.status(200).json({
                        message: 'Success',
                        result: {
                            _id: doc._id
                        }
                    });
                }
                else {
                    res.status(500).json({
                        message: 'Something went wrong'
                    });
                }
            });
        } catch (err) {
            next(err);
        }
    },
    get: function(req, res, next) {
        try {
            const { pagination, language } = req;
            let cond = {};
            if(language) cond.language = language;
            Menu.get({ ...cond }, { pagination }, (err, list) => {
                if(err) return res.status(500).json({ message: err.message || 'Something went wrong' });
                let { docs, ...meta } = list;
                res.status(200).json({
                    meta: docs ? meta : {},
                    result: docs || list
                });
            });
        } catch (err) {
            next(err);
        }
    },
    getById: function(req, res, next) {
        try {
            let { id } = req;
            Menu.getById(id, (err, doc) => {
                if(err) return res.status(500).json({ message: err.message || 'Something went wrong' });
                res.status(200).json({
                    result: doc
                });
            });
        } catch (err) {
            next(err);
        }
    },
    delete: function(req, res, next) {
        try {
            let { id } = req;
            Menu.delete(id, (err, raw) => {
                if(err) return res.status(500).json({ message: err.message || 'Something went wrong' });
                res.status(500).json({
                    message: 'Fail',
                });
            });
        } catch (err) {
            next(err);
        }
    },
    deleteMany: function(req, res, next) {
        try {
            let { deleted } = req.headers;
            const deletedList = deleted.split(',');
            Menu.deleteMany(deletedList, (err, raw) => {
                if(err) return res.status(500).json({ message: err.message || 'Something went wrong' });
                if(raw.ok) res.status(200).json({
                    message: 'Success',
                });
                else res.status(500).json({
                    message: 'Fail',
                });
            });
        } catch (err) {
            next(err);
        }
    },
    partialUpdate: function(req, res, next) {
        try {
            let { id } = req;
            Menu.partialUpdate(id, req.body, (err, raw) => {
                if(err) return res.status(500).json({ message: err.message || 'Something went wrong' });
                res.status(200).json({
                    message: 'Success',
                });
            });
        } catch (err) {
            next(err);
        }
    },
    addMenuItem: function(req, res, next) {
        try {
            let { id } = req;
            Menu.addMenuItem(id, req.body, (err, raw) => {
                if(err) return res.status(500).json({ message: err.message || 'Something went wrong' });
                res.status(200).json({
                    message: 'Success',
                });
            });
        } catch (err) {
            next(err);
        }
    },
    removeMenuItem: function(req, res, next) {
        try {
            let { id } = req;
            Menu.removeMenuItem(id, req.body, (err, raw) => {
                if(err) return res.status(500).json({ message: err.message || 'Something went wrong' });
                res.status(200).json({
                    message: 'Success',
                });
            });
        } catch (err) {
            next(err);
        }
    },
    updateMenuItem: function(req, res, next) {
        try {
            let { id } = req;
            Menu.updateMenuItem(id, req.body, (err, raw) => {
                if(err) return res.status(500).json({ message: err.message || 'Something went wrong' });
                res.status(200).json({
                    message: 'Success',
                });
            });
        } catch (err) {
            next(err);
        }
    },
};