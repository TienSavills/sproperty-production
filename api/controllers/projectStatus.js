const ProjectStatus =  require('../models/projectStatus');

module.exports = {
    get: function(req, res, next) {
        try {
            ProjectStatus.get({}, { language: req.language }, (err, docs) => {
                if(err) return res.status(500).json({ message: err.message || 'Something went wrong' });
                res.status(200).json({
                    meta: {},
                    result: docs
                });
            });
        } catch (err) {
            next(err);
        }
    }
};