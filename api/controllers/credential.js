const ApiKey =  require('../models/credential');
const User =  require('../models/user');
const { toHyphens } = require('../lib/helper');

module.exports = {
    create: function(req, res, next) {
        try {
            let { user } = req;
            let data = {
                ...req.body,
                createdBy: user._id,
                private: !user.roles.some(r => r.level === 0)
            };
            ApiKey.create(data, (err, doc, affected) => {
                if(err) return res.status(500).json({ message: err.message || 'Something went wrong' });
                if(affected > 0) {
                    res.status(200).json({
                        message: 'Success',
                        result: {
                            _id: doc._id
                        }
                    });
                }
                else {
                    res.status(500).json({
                        message: 'Something went wrong'
                    });
                }
            });
        } catch (err) {
            next(err);
        }
    },
    get: function(req, res, next) {
        try {
            let { pagination } = req;
            ApiKey.get({
                createdBy: req.user._id
            }, { pagination }, (err, list) => {
                if(err) return res.status(500).json({ message: err.message || 'Something went wrong' });
                let { docs, ...meta } = list;
                res.status(200).json({
                    meta: docs ? meta : {},
                    result: docs || list
                });
            });
        } catch (err) {
            next(err);
        }
    },
    getWebsites: function(req, res, next) {
        try {
            let { pagination } = req;
            ApiKey.get({
                '$or': [
                    {
                        createdBy: req.user._id
                    },
                    {
                        private: false
                    }
                ]
            }, { pagination }, (err, list) => {
                if(err) return res.status(500).json({ message: err.message || 'Something went wrong' });
                let { docs, ...meta } = list;
                res.status(200).json({
                    meta: docs ? meta : {},
                    result: docs || list
                });
            });
        } catch (err) {
            next(err);
        }
    },
    search: async function(req, res, next) {
        try {
            let { pagination, queries } = req;
            let { createdBy } = queries || {};
            let moreQueries = {};
            let noResult = false;
            let seller = null;
            if(!!createdBy) moreQueries['createdBy'] = createdBy;
            if(!!noResult) res.status(200).json({
                meta: {
                    total: 0,
                    pages: 1,
                    page: pagination.page,
                    limit: pagination.limit
                },
                result: []
            });
            ApiKey.get(moreQueries, { pagination }, (err, list) => {
                if(err) return res.status(500).json({ message: err.message || 'Something went wrong' });
                let { docs, ...meta } = list;
                res.status(200).json({
                    meta: docs ? meta : {},
                    result: docs || list
                });
            });
        } catch (err) {
            next(err);
        }
    },
    getById: function(req, res, next) {
        try {
            let { id, language } = req;
            ApiKey.getById(id, (err, doc) => {
                if(err) return res.status(500).json({ message: err.message || 'Something went wrong' });
                res.status(200).json({
                    result: doc
                });
            });
        } catch (err) {
            next(err);
        }
    },
    delete: function(req, res, next) {
        try {
            let { id } = req;
            ApiKey.delete(id, (err, raw) => {
                if(err) return res.status(500).json({ message: err.message || 'Something went wrong' });
                res.status(500).json({
                    message: 'Fail',
                });
            });
        } catch (err) {
            next(err);
        }
    },
    deleteMany: function(req, res, next) {
        try {
            let { deleted } = req.headers;
            const deletedList = deleted.split(',');
            ApiKey.deleteMany(deletedList, (err, raw) => {
                if(err) return res.status(500).json({ message: err.message || 'Something went wrong' });
                res.status(200).json({
                    message: 'Success',
                });
            });
        } catch (err) {
            next(err);
        }
    },
    partialUpdate: function(req, res, next) {
        try {
            let { id, user } = req;
            let data = {
                ...req.body,
                updatedBy: user._id,
            };
            ApiKey.partialUpdate(id, data, (err, raw) => {
                if(err) return res.status(500).json({ message: err.message || 'Something went wrong' });
                res.status(200).json({
                    message: 'Success',
                });
            });
        } catch (err) {
            next(err);
        }
    },
};