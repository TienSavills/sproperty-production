module.exports = {
  apps : [{
    name: 'savills-server',
    script: './server.js',
    mode: "cluster",
    instances: "max",
    env: {
      NODE_ENV: 'development'
    },
    env_production: {
      NODE_ENV: 'production'
    },
    output: './output.log',
    error: './error.log',
    merge_logs: true
  }]
};
