exports.btoa = function(rawString){
    return new Buffer(rawString).toString('base64');
}

exports.atob = function(encodedString){
    return new Buffer(encodedString, 'base64').toString();
}