const request = require('request')
const config = require('config')

async function verify(response) {
    return new Promise(function (resolve, reject) {
        request.post({
            url: 'https://www.google.com/recaptcha/api/siteverify',
            form: { secret: config.reCaptcha.secretKey, response }
        }, function (error, res, body) {
            if (!error && res.statusCode == 200) {
                resolve(body);
            } else {
                reject(error);
            }
        });
    });
}

module.exports = {
    verify
}