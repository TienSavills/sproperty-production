const config = require('config');
// const nodemailer = require('nodemailer');
const fs = require('fs');
const readline = require('readline');
const {google} = require('googleapis');

const SCOPES = ['https://mail.google.com', 'https://www.googleapis.com/auth/gmail.send'];
const TOKEN_PATH = 'credentials.json';

/**
 * Create an OAuth2 client with the given credentials, and then execute the
 * given callback function.
 * @param {Object} credentials The authorization client credentials.
 * @param {function} callback The callback to call with the authorized client.
 */
function authorize(credentials, callback) {
    const {client_secret, client_id, redirect_uris} = credentials.installed;
    const oAuth2Client = new google.auth.OAuth2(
        client_id, client_secret, redirect_uris[0]);
  
    // Check if we have previously stored a token.
    fs.readFile(TOKEN_PATH, (err, token) => {
      if (err) return getNewToken(oAuth2Client, callback);
      oAuth2Client.setCredentials(JSON.parse(token));
      callback(oAuth2Client);
    });
}
  
/**
 * Get and store new token after prompting for user authorization, and then
 * execute the given callback with the authorized OAuth2 client.
 * @param {google.auth.OAuth2} oAuth2Client The OAuth2 client to get token for.
 * @param {getEventsCallback} callback The callback for the authorized client.
 */
function getNewToken(oAuth2Client, callback) {
    const authUrl = oAuth2Client.generateAuthUrl({
      access_type: 'offline',
      scope: SCOPES,
    });
    console.log('Authorize this app by visiting this url:', authUrl);
    const rl = readline.createInterface({
      input: process.stdin,
      output: process.stdout,
    });
    rl.question('Enter the code from that page here: ', (code) => {
      rl.close();
      oAuth2Client.getToken(code, (err, token) => {
        if (err) return callback(err);
        oAuth2Client.setCredentials(token);
        // Store the token to disk for later program executions
        fs.writeFile(TOKEN_PATH, JSON.stringify(token), (err) => {
          if (err) return console.error(err);
          console.log('Token stored to', TOKEN_PATH);
        });
        callback(oAuth2Client);
      });
    });
}
/*
function makeBody(to, from, subject, message) {
    var str = ["Content-Type: text/html; charset=\"UTF-8\"\n",
        "MIME-Version: 1.0\n",
        "Content-Transfer-Encoding: 7bit\n",
        "to: ", to, "\n",
        "from: ", from, "\n",
        "subject: ", subject, "\n\n",
        message
    ].join('');
  
    var encodedMail = new Buffer(str).toString("base64").replace(/\+/g, '-').replace(/\//g, '_');
    return encodedMail;
}
*/
function makeBody(to, from, subject, html = false, message, attachments = []) {
    var boundary = '__cvi__';
    var mimeBody = [
        `Content-Type: multipart/mixed; charset="UTF-8"; boundary="${boundary}"`, '\n',
        'MIME-Version: 1.0\n',
        'To: ', to, '\n',
        'From: ', from, '\n',
        'Subject: ', subject, '\n\n',

        '--', boundary, '\n',
        `Content-Type: text/${html ? 'html' : 'plain'}; charset="UTF-8"`, '\n',
        'Content-Transfer-Encoding: 7bit\n\n',

        message, '\n\n'
    ];//.join('');
    for (var i = 0; i < attachments.length; i++) {
        var attachment = [
            '--' + boundary,
            "Content-Type: " + attachments[i].mimeType,
            'Content-Transfer-Encoding: base64',
            'Content-Disposition: attachment; filename="' + attachments[i].fileName + '"\n',
            attachments[i].bytes + '\n'
        ];
        mimeBody.push(attachment.join('\n'));
    }
    mimeBody.push('--' + boundary + '--');
  
    var encodedMail = new Buffer(mimeBody.join('')).toString("base64").replace(/\+/g, '-').replace(/\//g, '_');
    return encodedMail;
}

  // Insert file attachments from Google Drive
function getAttachments_(ids) {
    var att = [];
    for (var i in ids) {
        var file = DriveApp.getFileById(ids[i]);
        att.push({
            mimeType: file.getMimeType(),
            fileName: file.getName(),
            bytes: Utilities.base64Encode(file.getBlob().getBytes())
      });
    }
    return att;
}
  
  // Create a MIME message that complies with RFC 2822
function createMimeMessage_(msg) {
  
    var nl = "\n";
    var boundary = "__ctrlq_dot_org__";
  
    var mimeBody = [
  
        "MIME-Version: 1.0",
        "To: "      + encode_(msg.to.name) + "<" + msg.to.email + ">",
        "From: "    + encode_(msg.from.name) + "<" + msg.from.email + ">",
        "Subject: " + encode_(msg.subject), // takes care of accented characters
    
        "Content-Type: multipart/alternative; boundary=" + boundary + nl,
        "--" + boundary,
    
        "Content-Type: text/plain; charset=UTF-8",
        "Content-Transfer-Encoding: base64" + nl,
        Utilities.base64Encode(msg.body.text, Utilities.Charset.UTF_8) + nl,
        "--" + boundary,
    
        "Content-Type: text/html; charset=UTF-8",
        "Content-Transfer-Encoding: base64" + nl,
        Utilities.base64Encode(msg.body.html, Utilities.Charset.UTF_8) + nl
  
    ];
  
    for (var i = 0; i < msg.files.length; i++) {
  
        var attachment = [
            "--" + boundary,
            "Content-Type: " + msg.files[i].mimeType + '; name="' + msg.files[i].fileName + '"',
            'Content-Disposition: attachment; filename="' + msg.files[i].fileName + '"',
            "Content-Transfer-Encoding: base64" + nl,
            msg.files[i].bytes
        ];
    
        mimeBody.push(attachment.join(nl));
  
    }
  
    mimeBody.push("--" + boundary + "--");
  
    return mimeBody.join(nl);
  
}
/**
 * Send an email to an email list
 * @param {Array} to List of recipients
 * @param {String} subject 
 * @param {String} bodyContent 
 * @param {String} isHtmlBody 
 * @param {Array} attachments 
 * @param {Function} cb 
 */
function sendMail(to, subject, bodyContent, isHtmlBody = false, attachments = [], cb = function(err, inf) {}){
    return new Promise((resolve, reject) => {

        fs.readFile('client_secret.json', function(err, content) {
            if (err) return console.log('Error loading client secret file:', err);
            // Authorize a client with credentials, then call the Google Sheets API.
            authorize(JSON.parse(content), sendMessage);
        });

        var atts = [];

        attachments.forEach(att => {
            atts.push({
                fileName: att.fileName,
                bytes: fs.readFileSync(att.pathName).toString('base64'),
                mimeType: att.mimeType,
            });
        });
  
        function sendMessage(auth) {
            const gmail = google.gmail({version: 'v1', auth});
            var raw = makeBody([...to.map(recipient => `${recipient.name} <${recipient.email}>`)].join(', '), `${config.get('Emails.NoReply.name')} <${config.get('Emails.NoReply.user')}>`, subject, isHtmlBody, bodyContent, atts);
            gmail.users.messages.send({
                auth: auth,
                userId: 'me',
                resource: {
                    raw: raw
                }
            }, function(err, response) {
                if (err) {
                    if(cb) cb(err, null);
                    else reject(err);
                    return;
                }
                if(cb) cb(null, response);
                else resolve(response);
                // console.log(response);
                // console.log('Message %s sent: %s', info.messageId, info.response);
            });
        }
        
        // let transporter = nodemailer.createTransport({
        //     service: config.get('MailSettings.service'),
        //     auth: {
        //         user: config.get('Emails.NoReply.user'),
        //         pass: config.get('Emails.NoReply.pass')
        //     }
        //     // XOAuth2: {
        //     //     user: config.get('Emails.NoReply.user'), // Your gmail address.
        //     //     clientId: config.get('Emails.NoReply.clientID'),
        //     //     clientSecret: config.get('Emails.NoReply.clientSecret'),
        //     //     refreshToken: config.get('Emails.NoReply.refreshToken')
        //     // }
        // });
        // let bodyType = isHtmlBody ? 'html' : 'text';
        // let mailOptions = {
        //     from: `"${config.get('Emails.NoReply.name')}" <${config.get('Emails.NoReply.user')}>`, // sender address
        //     to: to,
        //     subject: subject,
        //     [bodyType]: bodyContent
        // };
        // if(attachments.length > 0) mailOptions['attachments'] = attachments;
        // transporter.sendMail(mailOptions, (error, info) => {
        //     if (error) {
        //         if(cb) cb(error, null);
        //         else reject(error);
        //         return;
        //     }
        //     if(cb) cb(null, info);
        //     else resolve(info);
        //     console.log('Message %s sent: %s', info.messageId, info.response);
        // });
    });
}

// sendMail([{name: 'Phuong Huynh', email: 'nhutphuongit@gmail.com'}], '[Savills] Booking successful', 'TEST', false, [{fileName: 'Benefits-cvi.pdf', pathName: __dirname + '/Benefits-cvi.pdf', mimeType: 'application/pdf'}], function(err, inf) {
//     if(err) console.log('ERROR: ', err)
//     console.log('INF: ', inf)
// });

module.exports = sendMail;