const mongoose = require('mongoose')
const { deepFind, getByLanguage, forEachAsync } = require('./helper')

/**
 * 
 * @param {mongoose.Schema} schema 
 * @param {String} fields Multilingual fields
 */
module.exports.mapRefModels = (fields = '') => (schema, options) => {
    let refModels = {}
    fields.split(' ').forEach(f => {
        const fs = f.split('.')
        refModels[f] = fs.length > 1 ? deepFind(schema.obj, `${fs.slice(0, fs.length - 1).join('.')}.ref`) : schema.obj[fs[0]].ref
    })
    schema.statics.refModels = refModels
}

/**
 * 
 * @param {mongoose.Schema} schema 
 */
module.exports.translateFields = (schema, options) => {
    schema.statics.translateFields = async (obj, language) => {
        await forEachAsync(Object.keys(schema.statics.refModels), async f => {
            obj = await getByLanguage(obj, language, f, schema.statics.refModels[f])
        })
        return obj
    }
}
