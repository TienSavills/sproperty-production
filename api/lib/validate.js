module.exports = {
    email: value => /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/.test(value),
}