'use strict';

const express = require('express')();
const fs = require('fs');
// const path = require('path');
const routes = fs.readdirSync(__dirname);

/**
 * 
 * @param {express} app 
 * @param {*} opts 
 */
module.exports = function(app, ...opts) {
    routes.filter(r => r !== 'index.js').forEach(route => {
        require(`${__dirname}/${route}`)(app);
    });
};