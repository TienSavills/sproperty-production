'use strict';

// const express = require('express')();
const express = require('express').Router();
const fs = require('fs');
// const { verifyToken } = require('../../middleware/identity');
// const path = require('path');
const routes = fs.readdirSync(__dirname);

/**
 * 
 * @param {express} app 
 * @param {*} opts 
 */
module.exports = function(app, ...opts) {
    routes.filter(r => r !== 'index.js').forEach(ver => {
        const router = require('express').Router();
        require(`${__dirname}/${ver}`)(router);
        app.use(`/${ver}`, router);
    });
};