const Router = require('express').Router();
const { verifyToken } = require('../../../middleware/identity');
const { authorize } = require('../../../middleware/authorization');
const requireId = require('../../../middleware/require-id');
const passData = require('../../../middleware/pass-data');
const multiLanguage = require('../../../middleware/multi-language');
const paginate = require('../../../middleware/paginate');
const query = require('../../../middleware/query');
const ctl =  require('../../../controllers/post');

/**
 * 
 * @param {Router} router 
 */
module.exports = function(router, ...opts) {
    //Public routes
    router.route('/posts')
        .get(verifyToken, authorize({ module: 'posts-cms', permission: 'read' }), query, paginate, ctl.get)
        .post(verifyToken, authorize({ module: 'posts-cms', permission: 'create' }), ctl.create)
        .delete(verifyToken, authorize({ module: 'posts-cms', permission: 'delete' }), ctl.deleteMany);
    router.route('/posts/:id')
        .get(verifyToken, authorize({ module: 'posts-cms', permission: 'read' }), requireId, ctl.getById)
        .patch(verifyToken, authorize({ module: 'posts-cms', permission: 'update' }), requireId, ctl.partialUpdate)
        .delete(verifyToken, authorize({ module: 'posts-cms', permission: 'delete' }), requireId, ctl.delete);
    router.route('/posts/export').patch(verifyToken, query, ctl.get);
};