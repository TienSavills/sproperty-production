const Router = require('express').Router();
const { verifyToken } = require('../../../middleware/identity');
const { authorize } = require('../../../middleware/authorization');
const requireId = require('../../../middleware/require-id');
const requireLanguage = require('../../../middleware/require-language');
const multiLanguage = require('../../../middleware/multi-language');
const paginate = require('../../../middleware/paginate');
const query = require('../../../middleware/query');
const ctl =  require('../../../controllers/organization');

/**
 * 
 * @param {Router} router 
 */
module.exports = function(router, ...opts) {
    //Public routes
    router.route('/company-profile')
        .get(verifyToken, authorize({ role: 'system' }), ctl.getProfile)
        .patch(verifyToken, authorize({ role: 'system' }), ctl.partialUpdate);
};