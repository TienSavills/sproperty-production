const Router = require('express').Router();
const { verifyToken } = require('../../../middleware/identity');
const { authorize } = require('../../../middleware/authorization');
const requireId = require('../../../middleware/require-id');
const multiLanguage = require('../../../middleware/multi-language');
const paginate = require('../../../middleware/paginate');
const query = require('../../../middleware/query');
const ctl =  require('../../../controllers/inbox');

/**
 * 
 * @param {Router} router 
 */
module.exports = function(router, ...opts) {
    //Public routes
    router.route('/inboxes')
        .get(verifyToken, authorize({ module: 'inbox-cms', permission: 'read' }), query, multiLanguage, paginate, ctl.get);
    // router.route('/inboxes/:id').get(verifyToken, authorize(['admin', 'agency-admin', 'agency-staff']), requireId, multiLanguage, ctl.getById);
    router.route('/inboxes/:id/forwarding').patch(verifyToken, authorize({ module: 'inbox-cms', permission: 'update' }), requireId, ctl.forwarding);
    router.route('/inboxes/export').patch(verifyToken, query, ctl.get);
};