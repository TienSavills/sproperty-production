const Router = require('express').Router();
const { verifyToken } = require('../../../middleware/identity');
const { authorize } = require('../../../middleware/authorization');
const paginate = require('../../../middleware/paginate');
const query = require('../../../middleware/query');
const requireId = require('../../../middleware/require-id');
const multiLanguage = require('../../../middleware/multi-language');
const ctl =  require('../../../controllers/userRole');

/**
 * 
 * @param {Router} router 
 */
module.exports = function(router, ...opts) {
    //Public routes
    router.route('/user-roles')
        // .get(verifyToken, multiLanguage, ctl.get);
        .get(verifyToken, authorize({ role: 'system' }), query, paginate, multiLanguage, ctl.get)
        .post(verifyToken, authorize({ role: 'system' }), multiLanguage, ctl.create)
        .delete(verifyToken, authorize({ role: 'system' }), ctl.deleteMany);
    router.route('/user-roles/:id')
        .get(verifyToken, authorize({ role: 'system' }), requireId, multiLanguage, ctl.getById)
        .patch(verifyToken, authorize({ role: 'system' }), requireId, multiLanguage, ctl.partialUpdate)
        .delete(verifyToken, authorize({ role: 'system' }), requireId, ctl.delete);
};