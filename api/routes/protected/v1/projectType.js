const Router = require('express').Router();
const { verifyToken } = require('../../../middleware/identity');
const { authorize } = require('../../../middleware/authorization');
const multiLanguage = require('../../../middleware/multi-language');
const paginate = require('../../../middleware/paginate');
const ctl =  require('../../../controllers/projectType');

/**
 * 
 * @param {Router} router 
 */
module.exports = function(router, ...opts) {
    //Public routes
    router.route('/project-types')
        .get(verifyToken, multiLanguage, paginate, ctl.get);

    // router.route('/projects/:id').get(verifyToken, ctl.getProfile);
    // router.route('/project-types-import').post(verifyToken, ctl.import);
};