const Router = require('express').Router();
const { verifyToken } = require('../../../middleware/identity');
// const { authorize } = require('../../../middleware/authorization');
const paginate = require('../../../middleware/paginate');
const multiLanguage = require('../../../middleware/multi-language');
const ctl =  require('../../../controllers/province');

/**
 * 
 * @param {Router} router 
 */
module.exports = function(router) {
    router.route('/provinces').get(verifyToken, paginate, multiLanguage, ctl.get);
    // router.route('/provinces-import').post(verifyToken, verifyToken, ctl.import);
};