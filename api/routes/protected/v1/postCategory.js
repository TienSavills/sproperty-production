const Router = require('express').Router();
const { verifyToken } = require('../../../middleware/identity');
const { authorize } = require('../../../middleware/authorization');
const multiLanguage = require('../../../middleware/multi-language');
const paginate = require('../../../middleware/paginate');
const query = require('../../../middleware/query');
const requireId = require('../../../middleware/require-id');
const requireModuleId = require('../../../middleware/require-module-id');
const ctl =  require('../../../controllers/postCategory');

/**
 * 
 * @param {Router} router 
 */
module.exports = function(router, ...opts) {
    //Public routes
    router.route('/post-categories')
        // .get(verifyToken, multiLanguage, ctl.get);
        .get(verifyToken, query, requireModuleId, paginate, ctl.get)
        .post(verifyToken, multiLanguage, ctl.create)
        .delete(verifyToken, ctl.deleteMany);
    router.route('/post-categories/:id')
        .get(verifyToken, requireId, ctl.getById)
        .patch(verifyToken, requireId, ctl.partialUpdate)
        .delete(verifyToken, requireId, ctl.delete);
    router.route('/post-categories/export').patch(verifyToken, query, ctl.get);
};