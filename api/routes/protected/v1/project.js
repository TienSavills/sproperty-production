const Router = require('express').Router();
const { verifyToken } = require('../../../middleware/identity');
const { authorize } = require('../../../middleware/authorization');
const paginate = require('../../../middleware/paginate');
const requireId = require('../../../middleware/require-id');
const requireLanguage = require('../../../middleware/require-language');
const query = require('../../../middleware/query');
const multiLanguage = require('../../../middleware/multi-language');
const ctl =  require('../../../controllers/project');

/**
 * 
 * @param {Router} router 
 */
module.exports = function(router, ...opts) {
    //Public routes
    router.route('/projects')
        .get(verifyToken, authorize({ module: 'project-cms', permission: 'read' }), query, paginate, ctl.get)
        .post(verifyToken, authorize({ module: 'project-cms', permission: 'create' }), ctl.create)
        .patch(verifyToken, authorize({ module: 'project-cms', permission: 'update' }), ctl.approveMany)
        .delete(verifyToken, authorize({ module: 'project-cms', permission: 'delete' }), ctl.deleteMany);
    router.route('/projects/:id')
        .get(verifyToken, authorize({ module: 'project-cms', permission: 'read' }), requireId, multiLanguage, ctl.getById)
        .patch(verifyToken, authorize({ module: 'project-cms', permission: 'update' }), requireId, requireLanguage, ctl.partialUpdate)
        .put(verifyToken, authorize({ module: 'project-cms', permission: 'update' }), requireId, multiLanguage, ctl.update)
        .delete(verifyToken, authorize({ module: 'project-cms', permission: 'delete' }), requireId, ctl.delete);
    router.route('/projects/:id/publishing').patch(verifyToken, authorize({ module: 'project-cms', permission: 'update' }), requireId, ctl.publishing);
    router.route('/projects/:id/approving-for-websites').patch(verifyToken, authorize({ module: 'project-cms', permission: 'update' }), requireId, ctl.approvingForWebsites);
    router.route('/projects/export').patch(verifyToken, query, ctl.get);

};