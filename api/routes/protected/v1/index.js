'use strict';

const Router = require('express').Router();
const fs = require('fs');
// const path = require('path');
const routes = fs.readdirSync(__dirname);

/**
 * 
 * @param {Router} router 
 * @param {*} opts 
 */
module.exports = function(router, ...opts) {
    routes.filter(r => r !== 'index.js').forEach(route => {
        require(`${__dirname}/${route}`)(router);
    });
};