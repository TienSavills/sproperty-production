const Router = require('express').Router();
const { verifyToken } = require('../../../middleware/identity');
const { authorize } = require('../../../middleware/authorization');
const requireId = require('../../../middleware/require-id');
const multiLanguage = require('../../../middleware/multi-language');
const paginate = require('../../../middleware/paginate');
const query = require('../../../middleware/query');
const ctl =  require('../../../controllers/listing');

/**
 * 
 * @param {Router} router 
 */
module.exports = function(router, ...opts) {
    //Public routes
    router.route('/listings')
        .get(verifyToken, authorize({ module: 'listing-cms', permission: 'read' }), query, multiLanguage, paginate, ctl.get)
        .post(verifyToken, authorize({ module: 'listing-cms', permission: 'create' }), ctl.create);
    router.route('/listings/:id').get(verifyToken, authorize({ module: 'listing-cms', permission: 'read' }), requireId, multiLanguage, ctl.getById);
    router.route('/listings/:id/forwarding').patch(verifyToken, authorize({ module: 'listing-cms', permission: 'update' }), requireId, ctl.forwarding);
    router.route('/listings/export').patch(verifyToken, query, ctl.get);
};