const Router = require('express').Router();
const { verifyToken } = require('../../../middleware/identity');
const { authorize } = require('../../../middleware/authorization');
const paginate = require('../../../middleware/paginate');
const query = require('../../../middleware/query');
const requireId = require('../../../middleware/require-id');
const ctl =  require('../../../controllers/credential');

/**
 * 
 * @param {Router} router 
 */
module.exports = function(router, ...opts) {
    router.route('/api-keys')
        .get(verifyToken, query, paginate, ctl.get)
        .post(verifyToken, ctl.create)
        .delete(verifyToken, ctl.deleteMany);
    router.route('/api-keys/:id')
        .get(verifyToken, requireId, ctl.getById)
        .patch(verifyToken, requireId, ctl.partialUpdate)
        .delete(verifyToken, requireId, ctl.delete);
    router.route('/api-key-search')
        .get(verifyToken, query, paginate, ctl.search);
    router.route('/api-keys/export').patch(verifyToken, query, ctl.get);
};