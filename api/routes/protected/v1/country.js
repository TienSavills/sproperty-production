const Router = require('express').Router();
const { verifyToken } = require('../../../middleware/identity');
const paginate = require('../../../middleware/paginate');
const multiLanguage = require('../../../middleware/multi-language');
const ctl =  require('../../../controllers/country');

/**
 * 
 * @param {Router} router 
 */
module.exports = function(router) {
    router.route('/countries').get(verifyToken, paginate, multiLanguage, ctl.get);
};