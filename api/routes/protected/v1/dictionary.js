const Router = require('express').Router();
const requireId = require('../../../middleware/require-id');
const { verifyToken } = require('../../../middleware/identity');
const { authorize } = require('../../../middleware/authorization');
const paginate = require('../../../middleware/paginate');
const query = require('../../../middleware/query');
const ctl =  require('../../../controllers/dictionary');

/**
 * 
 * @param {Router} router 
 */
module.exports = function(router) {
    router.route('/dictionaries')
        .get(verifyToken, query, paginate, ctl.get)
        .patch(verifyToken, ctl.translate)
        .delete(verifyToken, ctl.deleteMany);
    router.route('/dictionaries/:id')
        .delete(verifyToken, requireId, ctl.delete);
    router.route('/dictionaries/export').patch(verifyToken, query, ctl.get);
    router.route('/dictionaries/import').patch(verifyToken, ctl.import);
    router.route('/dictionaries/refresh').patch(verifyToken, ctl.refresh);
};