const Router = require('express').Router();
const { verifyToken } = require('../../../middleware/identity');
const query = require('../../../middleware/query');
const ctl =  require('../../../controllers/googlePlace');

/**
 * 
 * @param {Router} router 
 */
module.exports = function(router) {
    router.route('/place-search').get(verifyToken, query, ctl.search);
};