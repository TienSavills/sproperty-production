const Router = require('express').Router();
const { verifyToken } = require('../../../middleware/identity');
const multiLanguage = require('../../../middleware/multi-language');
// const paginate = require('../middleware/paginate');
const ctl = require('../../../controllers/mappingKey');

/**
 * 
 * @param {Router} router 
 */
module.exports = function (router, ...opts) {
    //Public routes
    router.route('/mapping-keys').get(multiLanguage, ctl.get);
    router.route('/mapping-keys').post(multiLanguage, ctl.create);

    // router.route('/projects/:id').get(verifyToken, ctl.getProfile);
};