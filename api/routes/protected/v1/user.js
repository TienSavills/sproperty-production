const Router = require('express').Router();
const { authorization, verifyToken } = require('../../../middleware/identity');
const { authorize } = require('../../../middleware/authorization');
const paginate = require('../../../middleware/paginate');
const requireId = require('../../../middleware/require-id');
const query = require('../../../middleware/query');
const multiLanguage = require('../../../middleware/multi-language');
const ctl =  require('../../../controllers/user');

/**
 * 
 * @param {Router} router 
 */
module.exports = function(router, ...opts) {
    //CMS routes
    router.route('/auth').post(authorization, ctl.login);
    router.route('/profile').get(verifyToken, multiLanguage, ctl.getProfile);
    // router.route('/users')
    //     .get(verifyToken, query, paginate, ctl.get)
    //     .post(verifyToken, ctl.create)
    //     .delete(verifyToken, ctl.deleteMany);
    router.route('/staffs')
        .get(verifyToken, authorize({ module: 'member-cms', permission: 'read' }), query, paginate, multiLanguage, ctl.getMembers)
        .post(verifyToken, authorize({ module: 'member-cms', permission: 'create' }), multiLanguage, ctl.createMember)
        .delete(verifyToken, authorize({ module: 'member-cms', permission: 'delete' }), ctl.deleteMany);
    router.route('/administrators')
        .get(verifyToken, authorize({ module: 'admin-cms', permission: 'read' }), query, paginate, multiLanguage, ctl.getAministrators)
        .post(verifyToken, authorize({ module: 'admin-cms', permission: 'create' }), multiLanguage, ctl.createAdmin)
        .patch(verifyToken, authorize({ module: 'admin-cms', permission: 'update' }), multiLanguage, ctl.updateAdmin)
        .delete(verifyToken, authorize({ module: 'admin-cms', permission: 'delete' }), ctl.deleteManyAdmin);
    router.route('/users/:id')
        .get(verifyToken, requireId, multiLanguage, ctl.getById)
        .patch(verifyToken, requireId, multiLanguage, ctl.update)
        .delete(verifyToken, requireId, ctl.delete);
    router.route('/users/:id/reset-password').patch(verifyToken, requireId, ctl.resetPassword);
    router.route('/users/:id/lock').patch(verifyToken, requireId, ctl.lock);
    router.route('/users/:id/unlock').patch(verifyToken, requireId, ctl.unlock);
    router.route('/users/export').patch(verifyToken, query, ctl.get);
};