const Router = require('express').Router();
const { verifyToken } = require('../../../middleware/identity');
const { authorize } = require('../../../middleware/authorization');
const paginate = require('../../../middleware/paginate');
const query = require('../../../middleware/query');
const ctl =  require('../../../controllers/module');

/**
 * 
 * @param {Router} router 
 */
module.exports = function(router) {
    router.route('/modules').get(verifyToken, query, paginate, ctl.get);
};