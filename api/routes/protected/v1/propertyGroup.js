const Router = require('express').Router();
const { verifyToken } = require('../../../middleware/identity');
const { authorize } = require('../../../middleware/authorization');
const multiLanguage = require('../../../middleware/multi-language');
const paginate = require('../../../middleware/paginate');
const ctl =  require('../../../controllers/propertyGroup');

/**
 * 
 * @param {Router} router 
 */
module.exports = function(router, ...opts) {
    //CMS routes
    router.route('/property-groups')
        .get(verifyToken, multiLanguage, paginate, ctl.get);
        // .post(verifyToken, ctl.create);
    // router.route('/projects/:id').get(verifyToken, ctl.getProfile);
};