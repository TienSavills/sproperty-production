const Router = require('express').Router();
const { verifyToken } = require('../../../middleware/identity');
const { authorize } = require('../../../middleware/authorization');
const multiLanguage = require('../../../middleware/multi-language');
const paginate = require('../../../middleware/paginate');
const ctl =  require('../../../controllers/amenities');

/**
 * 
 * @param {Router} router 
 */
module.exports = function(router, ...opts) {
    //Public routes
    router.route('/agencies')
        .get(verifyToken, authorize({ module: 'agencies-cms' }), paginate, multiLanguage, ctl.get);
        // .post(verifyToken, ctl.create);
    router.route('/agencies/export').patch(verifyToken, ctl.get);
    // router.route('/amenities-import').post(verifyToken, ctl.import);
};