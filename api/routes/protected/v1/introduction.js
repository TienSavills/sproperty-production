const Router = require('express').Router();
const { verifyToken } = require('../../../middleware/identity');
const { authorize } = require('../../../middleware/authorization');
const requireId = require('../../../middleware/require-id');
const passData = require('../../../middleware/pass-data');
const multiLanguage = require('../../../middleware/multi-language');
const paginate = require('../../../middleware/paginate');
const query = require('../../../middleware/query');
const ctl =  require('../../../controllers/post');

/**
 * 
 * @param {Router} router 
 */
module.exports = function(router, ...opts) {
    //Public routes
    router.route('/introductions')
        .get(verifyToken, authorize({ module: 'introductions-cms', permission: 'read' }), query, passData({ module: 'introduction-cms' }), multiLanguage, paginate, ctl.get)
        .post(verifyToken, authorize({ module: 'introductions-cms', permission: 'create' }), passData({ module: 'introduction-cms' }), ctl.create)
        .delete(verifyToken, authorize({ module: 'introductions-cms', permission: 'delete' }), ctl.deleteMany);
    router.route('/introductions/:id')
        .get(verifyToken, authorize({ module: 'introductions-cms', permission: 'read' }), requireId, multiLanguage, ctl.getById)
        .patch(verifyToken, authorize({ module: 'introductions-cms', permission: 'update' }), requireId, ctl.partialUpdate)
        .delete(verifyToken, authorize({ module: 'introductions-cms', permission: 'delete' }), requireId, ctl.delete);
    router.route('/introductions/export').patch(verifyToken, query, ctl.get);
};