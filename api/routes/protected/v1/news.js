const Router = require('express').Router();
const { verifyToken } = require('../../../middleware/identity');
const { authorize } = require('../../../middleware/authorization');
const requireId = require('../../../middleware/require-id');
const passData = require('../../../middleware/pass-data');
const multiLanguage = require('../../../middleware/multi-language');
const paginate = require('../../../middleware/paginate');
const query = require('../../../middleware/query');
const ctl =  require('../../../controllers/post');

/**
 * 
 * @param {Router} router 
 */
module.exports = function(router, ...opts) {
    //Public routes
    router.route('/news')
        .get(verifyToken, authorize({ module: 'news-cms', permission: 'read' }), query, passData({ module: 'news-cms' }), paginate, ctl.get)
        .post(verifyToken, authorize({ module: 'news-cms', permission: 'create' }), passData({ module: 'news-cms' }), ctl.create)
        .delete(verifyToken, authorize({ module: 'news-cms', permission: 'delete' }), ctl.deleteMany);
    router.route('/news/:id')
        .get(verifyToken, authorize({ module: 'news-cms', permission: 'read' }), requireId, ctl.getById)
        .patch(verifyToken, authorize({ module: 'news-cms', permission: 'update' }), requireId, ctl.partialUpdate)
        .delete(verifyToken, authorize({ module: 'news-cms', permission: 'delete' }), requireId, ctl.delete);
    router.route('/news/export').patch(verifyToken, query, ctl.get);
};