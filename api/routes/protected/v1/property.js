const Router = require('express').Router();
const { verifyToken } = require('../../../middleware/identity');
const { authorize } = require('../../../middleware/authorization');
const requireId = require('../../../middleware/require-id');
const requireLanguage = require('../../../middleware/require-language');
const multiLanguage = require('../../../middleware/multi-language');
const paginate = require('../../../middleware/paginate');
const query = require('../../../middleware/query');
const ctl =  require('../../../controllers/property');

/**
 * 
 * @param {Router} router 
 */
module.exports = function(router, ...opts) {
    //Public routes
    router.route('/properties')
        .get(verifyToken, authorize({ module: 'property-cms', permission: 'read' }), query, paginate, ctl.get)
        .post(verifyToken, authorize({ module: 'property-cms', permission: 'create' }), ctl.create)
        .patch(verifyToken, authorize({ module: 'property-cms', permission: 'update' }), ctl.approveMany)
        .delete(verifyToken, authorize({ module: 'property-cms', permission: 'delete' }), ctl.deleteMany);
    router.route('/properties/:id')
        .get(verifyToken, authorize({ module: 'property-cms', permission: 'read' }), requireId, multiLanguage, ctl.getById)
        .patch(verifyToken, authorize({ module: 'property-cms', permission: 'update' }), requireId, requireLanguage, ctl.partialUpdate)
        .put(verifyToken, authorize({ module: 'property-cms', permission: 'update' }), requireId, multiLanguage, ctl.update)
        .delete(verifyToken, authorize({ module: 'property-cms', permission: 'delete' }), requireId, ctl.delete);
    router.route('/properties/:id/publishing').patch(verifyToken, authorize({ module: 'property-cms', permission: 'update' }), requireId, ctl.publishing);
    router.route('/properties/:id/approving-for-websites').patch(verifyToken, authorize({ module: 'property-cms', permission: 'update' }), requireId, ctl.approvingForWebsites);
    router.route('/properties/export').patch(verifyToken, query, ctl.get);

};