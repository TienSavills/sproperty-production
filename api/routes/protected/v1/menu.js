const Router = require('express').Router();
const { verifyToken } = require('../../../middleware/identity');
const paginate = require('../../../middleware/paginate');
const query = require('../../../middleware/query');
const requireId = require('../../../middleware/require-id');
const ctl =  require('../../../controllers/menu');

/**
 * 
 * @param {Router} router 
 */
module.exports = function(router, ...opts) {
    //Public routes
    router.route('/menu')
        .get(verifyToken, query, paginate, ctl.get)
        .post(verifyToken, ctl.create)
        .delete(verifyToken, ctl.deleteMany);
    router.route('/menu/:id')
        .get(verifyToken, requireId, ctl.getById)
        .patch(verifyToken, requireId, ctl.partialUpdate)
        .delete(verifyToken, requireId, ctl.delete);
    router.route('/menu/:id/add-item')
        .patch(verifyToken, requireId, ctl.addMenuItem);
    router.route('/menu/:id/remove-item')
        .patch(verifyToken, requireId, ctl.removeMenuItem);
    router.route('/menu/:id/update-item')
        .patch(verifyToken, requireId, ctl.updateMenuItem);
    router.route('/menu/export').patch(verifyToken, query, ctl.get);
};