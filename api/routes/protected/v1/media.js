const Router = require('express').Router();
const { verifyToken } = require('../../../middleware/identity');
const { authorize } = require('../../../middleware/authorization');
const multiLanguage = require('../../../middleware/multi-language');
const paginate = require('../../../middleware/paginate');
const requireId = require('../../../middleware/require-id');
const ctl =  require('../../../controllers/media');
const crypto = require('crypto');
const multer = require('multer');
const path = require('path');

const UPLOAD_PATH = './uploads';
const uploadStorage = multer.diskStorage({
    destination: UPLOAD_PATH,
    filename: function (req, file, callback) {
        crypto.pseudoRandomBytes(16, function(err, raw) {
            if (err) return callback(err);
          
            callback(null, raw.toString('hex') + path.extname(file.originalname));
        });
    }
});
const upload = multer({
    storage: uploadStorage
});

/**
 * 
 * @param {Router} router 
 */
module.exports = function(router, ...opts) {
    //Public routes
    router.route('/media')
        .get(paginate, multiLanguage, ctl.get);
        // .post(verifyToken, ctl.create);
    router.route('/media/:id').get(requireId, multiLanguage, ctl.getById);
    router.route('/photos').post(verifyToken, upload.single('photo'), ctl.uploadPhoto);
    router.route('/photos/:id').patch(verifyToken, requireId, upload.single('photo'), ctl.uploadPhoto);
    router.route('/videos').post(verifyToken, upload.single('video'), ctl.uploadVideo);
    
    router.route('/upload').post(verifyToken, upload.single('photo'), function(req, res, next) {
        try {
            // console.log(req.headers.filename);
            // console.log(decodeURIComponent(req.headers.filename));
            var tmp_path = req.file.path;
            crypto.pseudoRandomBytes(16, function(err, raw) {
                if (err) return res.status(500).json({ message: err.message || 'Something went wrong' });
            
                // set where the file should actually exists - in this case it is in the "images" directory
                // if(!fs.existsSync('./uploads')) fs.mkdirSync('./uploads');
                var target_path = req.headers.filename ? `.${decodeURIComponent(req.headers.filename)}` : `./uploads/${raw.toString('hex')}${path.extname(req.file.originalname)}`;
                // move the file from the temporary location to the intended location
                // console.log(target_path);
                fs.rename(tmp_path, target_path, function(err) {
                    if (err) return res.status(500).json({ message: err.message || 'Something went wrong' });
                    // delete the temporary file, so that the explicitly set temporary upload dir does not get filled with unwanted files
                    fs.unlink(tmp_path, function() {
                        if (err) return res.status(500).json({ message: err.message || 'Something went wrong' });
                        res.status(200).json({
                            // message: `File uploaded to: ${target_path} - ${req.file.size} bytes`,
                            result: {
                                url: req.headers.filename ? target_path : target_path.replace('.', '')
                            }
                        });
                        // res.send('File uploaded to: ' + target_path + ' - ' + req.file.size + ' bytes');
                    });
                });
            });
        } catch (err) {
            next(err);
        }
    });
};