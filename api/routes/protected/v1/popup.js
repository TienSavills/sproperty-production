const Router = require('express').Router();
const { verifyToken } = require('../../../middleware/identity');
// const { authorize } = require('../../../middleware/authorization');
const requireId = require('../../../middleware/require-id');
const multiLanguage = require('../../../middleware/multi-language');
const paginate = require('../../../middleware/paginate');
const ctl =  require('../../../controllers/popup');

/**
 * 
 * @param {Router} router 
 */
module.exports = function(router, ...opts) {
    //Public routes
    router.route('/pop-ups')
        .get(verifyToken, paginate, ctl.get)
        .post(verifyToken, ctl.create)
        .delete(verifyToken, ctl.deleteMany);
    router.route('/pop-ups/:id')
        .get(verifyToken, requireId, ctl.getById)
        .patch(verifyToken, requireId, ctl.partialUpdate)
        .delete(verifyToken, requireId, ctl.delete);
    router.route('/pop-ups/:id/publishing').patch(verifyToken, requireId, ctl.publishing);
    // router.route('/pop-ups-import').post(verifyToken, ctl.import);
    router.route('/pop-ups/export').patch(verifyToken, ctl.get);
};