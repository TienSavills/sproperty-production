const Router = require('express').Router();
const { verifyToken } = require('../../../middleware/identity');
const { authorize } = require('../../../middleware/authorization');
const requireId = require('../../../middleware/require-id');
const multiLanguage = require('../../../middleware/multi-language');
const paginate = require('../../../middleware/paginate');
const ctl =  require('../../../controllers/invesloper');

/**
 * 
 * @param {Router} router 
 */
module.exports = function(router, ...opts) {
    //CMS routes
    router.route('/inveslopers')
        .get(verifyToken, multiLanguage, paginate, ctl.get)
        .post(verifyToken, multiLanguage, ctl.create)
        .delete(verifyToken, ctl.deleteMany);
    router.route('/inveslopers/:id')//.delete(verifyToken, requireId, ctl.delete);
        .get(verifyToken, authorize({ role: 'system' }), requireId, ctl.getById)
        .patch(verifyToken, authorize({ role: 'system' }), requireId, ctl.partialUpdate)
        .delete(verifyToken, authorize({ role: 'system' }), requireId, ctl.delete);
    router.route('/inveslopers/export').patch(verifyToken, ctl.get);
};