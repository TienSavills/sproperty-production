const Router = require('express').Router();
const { verifyToken } = require('../../../middleware/identity');
const query = require('../../../middleware/query');
const paginate = require('../../../middleware/paginate');
const multiLanguage = require('../../../middleware/multi-language');
const ctl =  require('../../../controllers/district');

/**
 * 
 * @param {Router} router 
 */
module.exports = function(router) {
    router.route('/districts').get(verifyToken, query, paginate, multiLanguage, ctl.get);
    // router.route('/districts-import').post(verifyToken, ctl.import);
    router.route('/districts/export').patch(verifyToken, query, ctl.get);
};