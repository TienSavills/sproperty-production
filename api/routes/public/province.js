const Router = require('express').Router();
const { verifyToken } = require('../../middleware/identity');
const ctl =  require('../../controllers/province');
const multiLanguage = require('../../middleware/multi-language');

/**
 * 
 * @param {Router} router 
 */
module.exports = function(router) {
    router.route('/provinces').get(multiLanguage, ctl.get);
};