const Router = require('express').Router();
const { verifyToken } = require('../../middleware/identity');
const paginate = require('../../middleware/paginate');
const ctl =  require('../../controllers/module');

/**
 * 
 * @param {Router} router 
 */
module.exports = function(router) {
    router.route('/modules').get(verifyToken, paginate, ctl.get);
};