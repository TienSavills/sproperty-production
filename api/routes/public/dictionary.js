const Router = require('express').Router();
const ctl =  require('../../controllers/dictionary');

/**
 * 
 * @param {Router} router 
 */
module.exports = function(router) {
    router.route('/dictionary').get(ctl.get);
};