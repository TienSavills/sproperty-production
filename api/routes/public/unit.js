const Router = require('express').Router();
const { verifyApiKey } = require('../../middleware/identity');
const multiLanguage = require('../../middleware/multi-language');
const paginate = require('../../middleware/paginate');
const ctl =  require('../../controllers/unit');

/**
 * 
 * @param {Router} router 
 */
module.exports = function(router, ...opts) {
    //Public routes
    router.route('/units')
        .get(verifyApiKey, paginate, multiLanguage, ctl.get);
        // .post(verifyToken, ctl.create);
    // router.route('/facing-import').post(verifyToken, ctl.import);
};