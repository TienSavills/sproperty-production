const Router = require('express').Router();
const { verifyApiKey } = require('../../middleware/identity');
const requireId = require('../../middleware/require-id');
const multiLanguage = require('../../middleware/multi-language');
const paginate = require('../../middleware/paginate');
const query = require('../../middleware/query');
const ctl =  require('../../controllers/post');

/**
 * 
 * @param {Router} router 
 */
module.exports = function(router, ...opts) {
    //Public routes
    router.route('/news')
        .get(verifyApiKey, query, multiLanguage, paginate, ctl.get);
    router.route('/news/:id').get(verifyApiKey, requireId, ctl.getById);
};