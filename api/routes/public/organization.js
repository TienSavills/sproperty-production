const Router = require('express').Router();
const { verifyApiKey } = require('../../middleware/identity');
const ctl =  require('../../controllers/organization');

/**
 * 
 * @param {Router} router 
 */
module.exports = function(router, ...opts) {
    //Public routes
    router.route('/company-profile')
        .get(verifyApiKey, ctl.getProfile);
};