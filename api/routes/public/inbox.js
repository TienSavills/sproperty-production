const Router = require('express').Router();
// const { verifyToken } = require('../../middleware/identity');
// const requireId = require('../../middleware/require-id');
// const multiLanguage = require('../../middleware/multi-language');
// const paginate = require('../../middleware/paginate');
// const query = require('../../middleware/query');
const { verify: verifyCaptcha } = require('../../middleware/recaptcha');
const ctl =  require('../../controllers/inbox');

/**
 * 
 * @param {Router} router 
 */
module.exports = function(router, ...opts) {
    //Public routes
    router.route('/project-inbox').post(verifyCaptcha, ctl.projectInbox);
    router.route('/property-inbox').post(verifyCaptcha, ctl.propertyInbox);
    router.route('/contact-inbox').post(verifyCaptcha, ctl.contactInbox);
};