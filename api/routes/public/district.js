const Router = require('express').Router();
const query = require('../../middleware/query');
const ctl =  require('../../controllers/district');
const multiLanguage = require('../../middleware/multi-language');

/**
 * 
 * @param {Router} router 
 */
module.exports = function(router) {
    router.route('/districts').get(query, multiLanguage, ctl.get);
};