const Router = require('express').Router();
const { authorization, verifyToken, verifyApiKey } = require('../../middleware/identity');
const paginate = require('../../middleware/paginate');
const query = require('../../middleware/query');
const requireId = require('../../middleware/require-id');
const multiLanguage = require('../../middleware/multi-language');
const ctl =  require('../../controllers/user');

/**
 * 
 * @param {Router} router 
 */
module.exports = function(router, ...opts) {
    //CMS routes
    router.route('/auth').post(authorization, ctl.login);
    router.route('/profile').get(verifyToken, multiLanguage, ctl.getProfile);
    router.route('/sellers')
        .get(verifyApiKey, query, paginate, multiLanguage, ctl.getSellers);
    router.route('/users/:id')
        .get(verifyToken, requireId, ctl.getById)
        .patch(verifyToken, requireId, ctl.update);
    router.route('/users/:id/reset-password').patch(verifyToken, requireId, ctl.resetPassword);
    router.route('/users/:id/lock').patch(verifyToken, requireId, ctl.lock);
    router.route('/users/:id/unlock').patch(verifyToken, requireId, ctl.unlock);
};