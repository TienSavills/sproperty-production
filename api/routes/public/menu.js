const Router = require('express').Router();
const paginate = require('../../middleware/paginate');
const query = require('../../middleware/query');
const requireId = require('../../middleware/require-id');
const multiLanguage = require('../../middleware/multi-language');
const ctl =  require('../../controllers/menu');

/**
 * 
 * @param {Router} router 
 */
module.exports = function(router, ...opts) {
    //Public routes
    router.route('/menu')
        .get(query, multiLanguage, paginate, ctl.get);
    router.route('/menu/:id')
        .get(requireId, ctl.getById);
};