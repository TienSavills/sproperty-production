const Router = require('express').Router();
const { verifyApiKey } = require('../../middleware/identity');
// const { authorize } = require('../../middleware/authorization');
const requireId = require('../../middleware/require-id');
const multiLanguage = require('../../middleware/multi-language');
const paginate = require('../../middleware/paginate');
const ctl =  require('../../controllers/popup');

/**
 * 
 * @param {Router} router 
 */
module.exports = function(router, ...opts) {
    //Public routes
    router.route('/pop-ups')
        .get(verifyApiKey, paginate, multiLanguage, ctl.get);
    router.route('/pop-ups/:id')
        .get(verifyApiKey, requireId, multiLanguage, ctl.getById);
};