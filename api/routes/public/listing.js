const Router = require('express').Router();
const { verifyToken } = require('../../middleware/identity');
const requireId = require('../../middleware/require-id');
const multiLanguage = require('../../middleware/multi-language');
const paginate = require('../../middleware/paginate');
const query = require('../../middleware/query');
const ctl =  require('../../controllers/listing');

/**
 * 
 * @param {Router} router 
 */
module.exports = function(router, ...opts) {
    //Public routes
    router.route('/listings').post(ctl.create);
};