const Router = require('express').Router();
const { verifyToken } = require('../../middleware/identity');
const requireId = require('../../middleware/require-id');
const requireModuleId = require('../../middleware/require-module-id');
const multiLanguage = require('../../middleware/multi-language');
const paginate = require('../../middleware/paginate');
const query = require('../../middleware/query');
const ctl =  require('../../controllers/post');

/**
 * 
 * @param {Router} router 
 */
module.exports = function(router, ...opts) {
    //Public routes
    router.route('/posts')
        .get(query, requireModuleId, multiLanguage, paginate, ctl.get);
    router.route('/posts/:id').get(requireId, ctl.getById);
};