const Router = require('express').Router();
const multiLanguage = require('../../middleware/multi-language');
const paginate = require('../../middleware/paginate');
const requireId = require('../../middleware/require-id');
const ctl =  require('../../controllers/media');
const crypto = require('crypto');
const multer = require('multer');
const path = require('path');

const UPLOAD_PATH = './uploads';
const uploadStorage = multer.diskStorage({
    destination: UPLOAD_PATH,
    filename: function (req, file, callback) {
        crypto.pseudoRandomBytes(16, function(err, raw) {
            if (err) return callback(err);
          
            callback(null, raw.toString('hex') + path.extname(file.originalname));
        });
    }
});
const upload = multer({
    storage: uploadStorage
});

/**
 * 
 * @param {Router} router 
 */
module.exports = function(router, ...opts) {
    //Public routes
    router.route('/media')
        .get(paginate, multiLanguage, ctl.get);
        // .post(verifyToken, ctl.create);
    router.route('/media/:id').get(requireId, multiLanguage, ctl.getById);
    router.route('/photos').post(upload.single('photo'), ctl.uploadPhoto);
    router.route('/photos/:id').patch(requireId, upload.single('photo'), ctl.uploadPhoto);
    router.route('/videos').post(upload.single('video'), ctl.uploadVideo);
};