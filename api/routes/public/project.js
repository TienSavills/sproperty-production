const Router = require('express').Router();
const { verifyApiKey } = require('../../middleware/identity');
const paginate = require('../../middleware/paginate');
const requireId = require('../../middleware/require-id');
const query = require('../../middleware/query');
const multiLanguage = require('../../middleware/multi-language');
const ctl =  require('../../controllers/project');

/**
 * 
 * @param {Router} router 
 */
module.exports = function(router, ...opts) {
    //Public routes
    router.route('/projects')
        .get(verifyApiKey, query, multiLanguage, paginate, ctl.get);
    router.route('/projects/:id').get(requireId, multiLanguage, ctl.getById);

    router.route('/projects-search')
        .get(verifyApiKey, query, multiLanguage, paginate, ctl.search);
};