const Router = require('express').Router();
const { verifyToken } = require('../../middleware/identity');
// const multiLanguage = require('../middleware/multi-language');
const paginate = require('../../middleware/paginate');
const query = require('../../middleware/query');
const requireId = require('../../middleware/require-id');
const ctl =  require('../../controllers/postCategory');

/**
 * 
 * @param {Router} router 
 */
module.exports = function(router, ...opts) {
    //Public routes
    router.route('/post-categories')
        // .get(verifyToken, multiLanguage, ctl.get);
        .get(query, paginate, ctl.get);
    router.route('/post-categories/:id').get(requireId, ctl.getById);
};