const Router = require('express').Router();
const { verifyToken } = require('../../middleware/identity');
const { authorize } = require('../../middleware/authorization');
const paginate = require('../../middleware/paginate');
const ctl =  require('../../controllers/language');

/**
 * 
 * @param {Router} router 
 */
module.exports = function(router, ...opts) {
    //CMS routes
    router.route('/languages')
        // .get(verifyToken, authorize(['admin', 'agency-admin', 'agency-staff']), paginate, ctl.get)
        .get(paginate, ctl.get);
    // router.route('/projects/:id').get(verifyToken, ctl.getProfile);
};