const Router = require('express').Router();
const { verifyApiKey } = require('../../middleware/identity');
const paginate = require('../../middleware/paginate');
const ctl =  require('../../controllers/currency');

/**
 * 
 * @param {Router} router 
 */
module.exports = function(router, ...opts) {
    //Public routes
    router.route('/currencies')
        .get(verifyApiKey, paginate, ctl.get);
        // .post(verifyToken, ctl.create);
    // router.route('/facing-import').post(verifyToken, ctl.import);
};