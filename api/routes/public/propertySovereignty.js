const Router = require('express').Router();
const multiLanguage = require('../../middleware/multi-language');
const paginate = require('../../middleware/paginate');
const ctl =  require('../../controllers/propertySovereignty');

/**
 * 
 * @param {Router} router 
 */
module.exports = function(router, ...opts) {
    //Public routes
    router.route('/property-sovereignties')
        .get(multiLanguage, paginate, ctl.get);

    // router.route('/projects/:id').get(verifyToken, ctl.getProfile);
};