const Router = require('express').Router();
const multiLanguage = require('../../middleware/multi-language');
const paginate = require('../../middleware/paginate');
const ctl =  require('../../controllers/amenities');

/**
 * 
 * @param {Router} router 
 */
module.exports = function(router, ...opts) {
    //Public routes
    router.route('/amenities')
        .get(paginate, multiLanguage, ctl.get);
        // .post(verifyToken, ctl.create);
    
};