const Router = require('express').Router();
const { verifyToken } = require('../../middleware/identity');
const multiLanguage = require('../../middleware/multi-language');
// const paginate = require('../middleware/paginate');
const ctl =  require('../../controllers/invesloper');

/**
 * 
 * @param {Router} router 
 */
module.exports = function(router, ...opts) {
    //CMS routes
    router.route('/inveslopers')
        .get(multiLanguage, ctl.get);
        // .post(verifyToken, ctl.create);
    // router.route('/projects/:id').get(verifyToken, ctl.getProfile);
};