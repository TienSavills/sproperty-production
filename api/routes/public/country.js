const Router = require('express').Router();
const ctl =  require('../../controllers/country');

/**
 * 
 * @param {Router} router 
 */
module.exports = function(router) {
    router.route('/countries').get(ctl.get);
};