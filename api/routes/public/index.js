'use strict';

const express = require('express')();
const router = require('express').Router();
const fs = require('fs');
// const path = require('path');
const routes = fs.readdirSync(__dirname);

/**
 * 
 * @param {express} app 
 * @param {*} opts 
 */
module.exports = function(app, ...opts) {
    routes.filter(r => r !== 'index.js').forEach(route => {
        require(`${__dirname}/${route}`)(router);
    });
    app.use('/', router);
};