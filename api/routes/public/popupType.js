const Router = require('express').Router();
const { verifyApiKey } = require('../../middleware/identity');
// const { authorize } = require('../../middleware/authorization');
const requireId = require('../../middleware/require-id');
const multiLanguage = require('../../middleware/multi-language');
const paginate = require('../../middleware/paginate');
const ctl =  require('../../controllers/popupType');

/**
 * 
 * @param {Router} router 
 */
module.exports = function(router, ...opts) {
    //Public routes
    router.route('/popup-types')
        .get(verifyApiKey, paginate, multiLanguage, ctl.get)
        // .post(verifyToken, ctl.create)
        // .delete(verifyToken, ctl.deleteMany);
    // router.route('/popup-types/:id')
    //     .get(verifyToken, requireId, multiLanguage, ctl.getById)
    //     .patch(verifyToken, requireId, ctl.partialUpdate)
    //     .delete(verifyToken, requireId, ctl.delete);
    // router.route('/pop-ups-import').post(verifyToken, ctl.import);
};