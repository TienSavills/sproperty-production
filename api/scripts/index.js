#!/usr/bin/env node

var program = require('commander');
var scripts;
program
    .usage('[options] [script]')
    .option('-g, --generate', 'Generate data')
    .option('-f, --fix', 'Fix data')
    .action(function(script) {
        if(script) scripts = script;
    });
program.parse(process.argv);
if(program.generate) {
    if(typeof scripts === 'undefined' || scripts === 'all') {
        // console.log('Command not found');
        // process.exit(1);
        const { fork } = require('child_process');
        const fs = require('fs');
    
        fs.readdirSync(`${__dirname}/generators/`).forEach(sf => {
            console.log('exec "%s" ...', sf);
            const cmd = fork(`${__dirname}/generators/${sf}`);
            cmd.on('message', function(msg) {
                console.log(msg);
            });
            cmd.on('error', function(err) {
                console.log(err);
            });
        });
    } else {
        const { fork } = require('child_process');
        console.log('exec "%s" ...', scripts);
        const cmd = fork(`${__dirname}/generators/${scripts}.js`);
        cmd.on('message', function(msg) {
            console.log(msg);
        });
        cmd.on('error', function(err) {
            console.log(err);
        });
        // cmd.on('close', function(code) {
        //     doneList.push(code);
        // }); 
        // if(doneList.length === ss.length) process.exit();
    }
}
if(program.fix) {
    const scripts = 'dictionary';
    const { fork } = require('child_process');
    console.log('exec "%s" ...', scripts);
    const cmd = fork(`${__dirname}/fix-data/${scripts}.js`);
    cmd.on('message', function(msg) {
        console.log(msg);
    });
    cmd.on('error', function(err) {
        console.log(err);
    });
}

function execProc(sc) {
    const cmd = fork(`${__dirname}/generators/${sc}.js`);
    cmd.on('message', function(msg) {
        console.log(msg);
    });
    cmd.on('error', function(err) {
        console.log(err);
    });
}