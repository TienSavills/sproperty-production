#!/usr/bin/env node

( function() {
	'use strict'

    const mongoose = require('mongoose')
    const Schema = mongoose.Schema;
    const dbConnect = require('../../db/connect')
    const { forEachAsync, deepFind } = require(`${process.cwd()}/lib/helper`)
    const config = require('config')
    const path = require('path');

	dbConnect(async (err, db) => {
        if (err) {
            console.log('Action fail')
        }
        else {
            try {
                const Dictionary = require('../../models/dictionary')
                await forEachAsync(Dictionary.TRANSLATED_MODELS, async (tm) => {
                    const tmodel = require(path.join('../../models', tm))
                    const foundDefaultData = await tmodel.find({ active: true });
                    foundDefaultData.forEach(fdd => {
                        tmodel.MULTILINGUAL_FIELDS.split(' ').forEach(mf => {
                            let matchEl = res.docs.find(el => el.refId === fdd && el.refModel === tmodel.collection.collectionName && el.key === mf)
                            if (!matchEl) res.docs.push({
                                _id: `${mongoose.Types.ObjectId()}-${tmodel.collection.collectionName}-${mf}`,
                                refId: fdd._id,
                                key: mf,
                                refModel: tmodel.collection.collectionName,
                                values: {},
                                defaultValue: fdd[mf]
                            })
                        })
                    })
                })
                const ContentDictionary = require('../../models/contentDictionary')
                const translatingModels = [
                    require('../../models/amenities'),
                    require('../../models/approvalStatus'),
                    require('../../models/currency'),
                    require('../../models/facing'),
                    require('../../models/group'),
                    require('../../models/invesloper'),
                    require('../../models/listingGroup'),
                    require('../../models/module'),
                    require('../../models/organization'),
                    require('../../models/postCategory'),
                    require('../../models/projectStatus'),
                    require('../../models/projectType'),
                    require('../../models/propertyFurniture'),
                    require('../../models/propertyGroup'),
                    require('../../models/propertySovereignty'),
                    require('../../models/propertyType'),
                    require('../../models/publishingStatus'),
                    require('../../models/transportation'),
                    require('../../models/unit'),
                    require('../../models/userRole'),
                    require('../../models/userStatus'),
                    require('../../models/user'),
                    require('../../models/view'),
                    require('../../models/province'),
                    require('../../models/district'),
                ]
                const translatingContentModels = [
                    require('../../models/project'),
                    require('../../models/property'),
                ]
                
                // const data = []
                await forEachAsync(translatingModels, async tm => {
                    const oldData = await tm.find({});
                    await forEachAsync(oldData, async od => {
                        if(tm.MULTILINGUAL_FIELDS) await forEachAsync(tm.MULTILINGUAL_FIELDS.split(' '), async f => {
                            const fss = f.split('.');
                            if(fss.length === 1 && typeof od[f] !== 'undefined' && typeof od[f] === 'object') {
                                console.log(tm.collection.collectionName, f, od[f]);
                                await forEachAsync(Object.keys(od[f]), async l => {
                                    await Dictionary.translate({
                                        language: l,
                                        key: f,
                                        value: od[f][l],
                                        refId: od._id,
                                        refModel: tm.collection.collectionName
                                    });
                                });
                                const theFirstLanguageKey = Object.keys(od[f])[0];
                                if(theFirstLanguageKey) await tm.findByIdAndUpdate(od._id, {
                                    [f]: od[f][theFirstLanguageKey]
                                });
                            }
                            if(typeof od[f] === 'string') {
                                console.log(tm.collection.collectionName, f, od[f]);
                                await Dictionary.translate({
                                    language: config.get('defaultLanguage'),
                                    key: f,
                                    value: od[f],
                                    refId: od._id,
                                    refModel: tm.collection.collectionName
                                });
                            }
                        });
                    });
                    // db.collection(tm.collection.collectionName).find().forEach(element => {
                    //     console.log(element._id);
                    // });
                });
                await forEachAsync(translatingContentModels, async tm => {
                    const oldData = await tm.find({});
                    await forEachAsync(oldData, async od => {
                        if(tm.MULTILINGUAL_FIELDS) await forEachAsync(tm.MULTILINGUAL_FIELDS.split(' '), async f => {
                            const fss = f.split('.');
                            if(fss.length === 1 && typeof od[f] !== 'undefined' && typeof od[f] === 'object') {
                                console.log(tm.collection.collectionName, f, od[f]);
                                await forEachAsync(Object.keys(od[f]), async l => {
                                    await ContentDictionary.translate({
                                        language: l,
                                        key: f,
                                        value: od[f][l],
                                        refId: od._id,
                                        refModel: tm.collection.collectionName
                                    });
                                });
                                const theFirstLanguageKey = Object.keys(od[f])[0];
                                if(theFirstLanguageKey) await tm.findByIdAndUpdate(od._id, {
                                    [f]: od[f][theFirstLanguageKey]
                                });
                            }
                        });
                    });
                    // db.collection(tm.collection.collectionName).find().forEach(element => {
                    //     console.log(element._id);
                    // });
                });
                // Dictionary.import(data, (err, docs, affected) => {
                //     if(err) console.log(err)
                //     else console.log(`Generated ${affected} records`)
                //     process.exit()
                // })
                process.exit()
            } catch (error) {
                console.log(error)
            }
        }
    })
})()