( function() {
	'use strict'

    const mongoose = require('mongoose')
    const Schema = mongoose.Schema;
    const dbConnect = require('../../db/connect')
    const { forEachAsync } = require(`${process.cwd()}/lib/helper`)
    const config = require('config')

	dbConnect(async (err, db) => {
        if (err) {
            console.log('Action fail')
        }
        else {
            try {
                const PropertyUse = require('../../models/propertyUse')
                const data = [
                    {
                        _id: 1,
                        title: 'Để ở',
                        languages: ['vi']
                    },
                    {
                        _id: 2,
                        title: 'Kinh doanh',
                        languages: ['vi']
                    },
                    {
                        _id: 3,
                        title: 'Cho thuê',
                        languages: ['vi']
                    },
                ]
                await forEachAsync(data, async (a) => {
                    await PropertyUse.create(a, {
                        language: config.get('defaultLanguage')
                    }, (err, doc, affected) => {
                        if(err) {
                            if(typeof err !== 'Error' && typeof err !== 'MongoError') {
                                console.log(err.message || 'Error')
                                return;
                            }
                        }
                        console.log(`Generated ${affected} records`)
                    })
                })
                process.exit()
            } catch (error) {
                console.log(error)
            }
        }
    })
})()