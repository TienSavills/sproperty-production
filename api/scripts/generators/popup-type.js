( function() {
	'use strict'

	const dbConnect = require('../../db/connect')
    const { forEachAsync } = require(`${process.cwd()}/lib/helper`)
    const config = require('config')

	dbConnect(async (err, db) => {
        if (err) {
            console.log('Action fail')
        }
        else {
            try {
                const UserStatus = require('../../models/popupType')
                const data = [
                    {
                        _id: 'announcement',
                        title: 'Announcement',
                    },
                    {
                        _id: 'banner',
                        title: 'Banner',
                    },
                ]
                await forEachAsync(data, async (a) => {
                    await UserStatus.create(a, {
                        language: config.get('defaultLanguage')
                    }, (err, doc, affected) => {
                        if(err) {
                            if(typeof err !== 'Error' && typeof err !== 'MongoError') {
                                console.log(err.message || 'Error')
                                return;
                            }
                        }
                        console.log(`Generated ${affected} records`)
                    })
                })
                process.exit()
            } catch (error) {
                console.log(error)
            }
        }
    })
})()