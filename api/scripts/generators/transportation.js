( async function() {
	'use strict'

    const dbConnect = require('../../db/connect')
    const { forEachAsync } = require(`${process.cwd()}/lib/helper`)
    const config = require('config')

	dbConnect(async (err, db) => {
        if (err) {
            console.log('Action fail')
        }
        else {
            try {
                const Transportation = require('../../models/transportation')
                const data = [{
                    "_id": "c0875287-9f2b-49fa-80cb-46043047c9b8",
                    "code": "Ferry",
                    "title": {
                        "en": "Ferry"
                    }
                }, {
                    "_id": "2b4f8adf-ddc2-4041-8dbb-265c3c7e75aa",
                    "code": "MTR",
                    "title": {
                        "en": "MTR"
                    }
                }, {
                    "_id": "47d8d147-8da9-4d2a-8d40-b39b3437a09d",
                    "code": "Buses",
                    "title": {
                        "en": "Buses"
                    }
                }, {
                    "_id": "db6d1855-1994-4b33-b4a1-1786e6facdb5",
                    "code": "Shuttle Bus",
                    "title": {
                        "en": "Shuttle Bus"
                    }
                }, {
                    "_id": "7fde02e1-3e0d-442e-9dd1-6027d57983ac",
                    "code": "BTS",
                    "title": {
                        "en": "BTS"
                    }
                }, {
                    "_id": "2f7f2c11-4ef9-4a50-93c3-68dff18f1d23",
                    "code": "MRT",
                    "title": {
                        "en": "MRT"
                    }
                }, {
                    "_id": "be3a15b4-9724-45d0-ae1c-83b97d8c9afa",
                    "code": "LRT",
                    "title": {
                        "en": "LRT"
                    }
                }, {
                    "_id": "01a6acb9-d537-43db-ac20-5ca0333b99d9",
                    "code": "River Taxi",
                    "title": {
                        "en": "River Taxi"
                    }
                }, {
                    "_id": "dcaf670e-caad-4900-88a2-5203b4f0f5c9",
                    "code": "BRT",
                    "title": {
                        "en": "BRT"
                    }
                }, {
                    "_id": "38ef82de-fafa-4c18-94d2-f7b3fb80dfe7",
                    "code": "Marina",
                    "title": {
                        "en": "Marina"
                    }
                }, {
                    "_id": "ce38469d-3c8c-4cdc-98e0-f7d2fbc1fabb",
                    "code": "Water bus",
                    "title": {
                        "en": "Water bus"
                    }
                }, {
                    "_id": "c8fd7738-c2a7-4f32-b2e6-d323a5648ebd",
                    "code": "River Boat",
                    "title": {
                        "en": "River Boat"
                    }
                }, {
                    "_id": "8028eae0-e8cd-49c0-973f-3310ade7613a",
                    "code": "Golf Cart",
                    "title": {
                        "en": "Golf Cart"
                    }
                }, {
                    "_id": "235745cc-f0d1-4729-b1be-a87d87bc9f50",
                    "code": "Taxi Water",
                    "title": {
                        "en": "Taxi Water"
                    }
                }];
                await forEachAsync(data, async (a) => {
                    await Transportation.create(a, {
                        language: config.get('defaultLanguage')
                    }, (err, doc, affected) => {
                        if(err) {
                            if(typeof err !== 'Error' && typeof err !== 'MongoError') {
                                console.log(err.message || 'Error')
                                return;
                            }
                        }
                        console.log(`Generated ${affected} records`)
                    })
                })
                process.exit()
            } catch (error) {
                console.log(error)
            }
        }
    })
})()