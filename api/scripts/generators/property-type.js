( function() {
	'use strict'

    const mongoose = require('mongoose')
    const Schema = mongoose.Schema;
    const dbConnect = require('../../db/connect')
    const { forEachAsync } = require(`${process.cwd()}/lib/helper`)
    const config = require('config')

	dbConnect(async (err, db) => {
        if (err) {
            console.log('Action fail')
        }
        else {
            try {
                const PropertyType = require('../../models/propertyType')
                const data = [{
                    "_id": "d799ec8f-9040-4ed4-b2f6-3462cb2d4bad",
                    "code": "Apartment / Flat",
                    "title": {
                        "en": "Apartment / Flat"
                    }
                }, {
                    "_id": "cf57bd3b-ccc6-4468-9045-83eb3342742e",
                    "code": "House",
                    "title": {
                        "en": "House"
                    }
                }, {
                    "_id": "f2979edf-6046-46b4-bcd7-6085bfecb2de",
                    "code": "Villa",
                    "title": {
                        "en": "Villa"
                    }
                }, {
                    "_id": "44900250-18a0-416c-96cc-9a3a2b802620",
                    "code": "Village Home",
                    "title": {
                        "en": "Village Home"
                    }
                }, {
                    "_id": "b0a8c6e6-9cf8-4bad-bf3f-0158dd6b1cf7",
                    "code": "Walk Up",
                    "title": {
                        "en": "Walk Up"
                    }
                }, {
                    "_id": "c710ba92-dde2-4ca3-8c3d-e726d6ce5039",
                    "code": "Serviced Apartment",
                    "title": {
                        "en": "Serviced Apartment"
                    }
                }, {
                    "_id": "9edd7bee-7682-4b28-a850-9048527790c1",
                    "code": "Land Plot",
                    "title": {
                        "en": "Land Plot"
                    }
                }, {
                    "_id": "90f5b98c-013f-464e-86e7-d35b3a545ab6",
                    "code": "Condominium",
                    "title": {
                        "en": "Condominium"
                    }
                }, {
                    "_id": "b07e791b-6c93-43a0-969e-417ba3d59185",
                    "code": "Townhouse",
                    "title": {
                        "en": "Townhouse"
                    }
                }, {
                    "_id": "beecd5f6-0ae0-4e4a-8eac-d521531e8313",
                    "code": "Cluster House",
                    "title": {
                        "en": "Cluster House"
                    }
                }, {
                    "_id": "09c5fba3-8e66-4c75-82be-0a165002d5d5",
                    "code": "Executive Condominium",
                    "title": {
                        "en": "Executive Condominium"
                    }
                }, {
                    "_id": "cbec8fed-ae01-4b5e-a26c-9b82699de7cd",
                    "code": "HDB Apartment",
                    "title": {
                        "en": "HDB Apartment"
                    }
                }, {
                    "_id": "e7229f81-8d98-42e7-ae18-b57e5081fcb6",
                    "code": "Detached",
                    "title": {
                        "en": "Detached"
                    }
                }, {
                    "_id": "c0760be3-d9cd-4562-93db-4515939c4631",
                    "code": "Semi-Detached",
                    "title": {
                        "en": "Semi-Detached"
                    }
                }, {
                    "_id": "ee902435-96d5-4cf1-832c-c2f7c4a8cb72",
                    "code": "Bungalow",
                    "title": {
                        "en": "Bungalow"
                    }
                }, {
                    "_id": "25684c67-5e94-4bce-aa26-8d3fe158ef5b",
                    "code": "Shop house",
                    "title": {
                        "en": "Shop house"
                    }
                }, {
                    "_id": "eb3cb10d-44d7-4b25-9516-f35dcc5342d6",
                    "code": "Landed property",
                    "title": {
                        "en": "Landed property"
                    }
                }, {
                    "_id": "e2a91409-b958-49d6-a33d-a4110432eadf",
                    "code": "Officetel",
                    "title": {
                        "en": "Officetel"
                    }
                }, {
                    "_id": "c89092d4-5e40-4b46-a99c-cfc6b188cc22",
                    "code": "Condotel",
                    "title": {
                        "en": "Condotel"
                    }
                }, {
                    "_id": "6fbb6551-4c2a-4e66-bb36-b3c6bde0cca5",
                    "code": "Corner Condo",
                    "title": {
                        "en": "Corner Condo"
                    }
                }, {
                    "_id": "c807edb2-ff7d-41ba-97d9-e2e9e4045032",
                    "code": "Lateral Condominium",
                    "title": {
                        "en": "Lateral Condominium"
                    }
                }, {
                    "_id": "6cea7fe6-c770-4c1a-97d4-f5b127f665d7",
                    "code": "Duplex",
                    "title": {
                        "en": "Duplex"
                    }
                }, {
                    "_id": "aae069d5-d054-4779-a5e5-2f800dd573b1",
                    "code": "Penthouse",
                    "title": {
                        "en": "Penthouse"
                    }
                }, {
                    "_id": "b87a23af-6ede-4107-8917-84dd3dad943b",
                    "code": "Studio",
                    "title": {
                        "en": "Studio"
                    }
                }, {
                    "_id": "89448cc6-6fe4-496d-a629-e3ee05595ada",
                    "code": "Triplex",
                    "title": {
                        "en": "Triplex"
                    }
                }];
                await forEachAsync(data, async (a) => {
                    await PropertyType.create(a, {
                        language: config.get('defaultLanguage')
                    }, (err, doc, affected) => {
                        if(err) {
                            if(typeof err !== 'Error' && typeof err !== 'MongoError') {
                                console.log(err.message || 'Error')
                                return;
                            }
                        }
                        console.log(`Generated ${affected} records`)
                    })
                })
                process.exit()
            } catch (error) {
                console.log(error)
            }
        }
    })
})()