( function() {
	'use strict'

	const dbConnect = require('../../db/connect')
    const { forEachAsync } = require(`${process.cwd()}/lib/helper`)
    const config = require('config')

	dbConnect(async (err, db) => {
        if (err) {
            console.log('Action fail')
        }
        else {
            try {
                const Organization = require('../../models/organization')
                const organizations = [
                    {
                        _id: config.get('defaultOrganization.id'),
                        name: config.get('defaultOrganization.name'),
                        email: config.get('defaultOrganization.email'),
                        tel: '0939693908',
                        socials: {
                            facebook: 'https://www.facebook.com/Savills/'
                        },
                    },
                ]
                await forEachAsync(organizations, async (o) => {
                    await Organization.create(o, (err, doc, affected) => {
                        if(err) {
                            if(typeof err !== 'Error' && typeof err !== 'MongoError') {
                                console.log(err.message || 'Error')
                                return;
                            }
                        }
                        console.log(`Generated ${affected} records`)
                    })
                })
                process.exit()
            } catch (error) {
                console.log(error)
            }
        }
    })
})()