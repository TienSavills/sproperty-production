( async function() {
	'use strict'

    const dbConnect = require('../../db/connect')
    const { forEachAsync } = require(`${process.cwd()}/lib/helper`)
    const config = require('config')

	dbConnect(async (err, db) => {
        if (err) {
            console.log('Action fail')
        }
        else {
            try {
                const Facing = require('../../models/facing')
                const data = [
                    {
                        "_id": "8c6c0664-bdf9-4d0f-ba33-caed9d60c96c",
                        "code": "E",
                        "title": {
                            "en": "East"
                        }
                    }, {
                        "_id": "97cb6be4-0124-41f5-886c-8534097e7b4d",
                        "code": "S",
                        "title": {
                            "en": "South"
                        }
                    }, {
                        "_id": "b30487d2-3928-4a08-8090-e16a39dd2591",
                        "code": "W",
                        "title": {
                            "en": "West"
                        }
                    }, {
                        "_id": "006f4a08-96af-4ba1-8658-fbf4ab1d9c98",
                        "code": "N",
                        "title": {
                            "en": "North"
                        }
                    }, {
                        "_id": "d61bfded-774a-4754-ac2f-22af5945f733",
                        "code": "NE",
                        "title": {
                            "en": "Northeast"
                        }
                    }, {
                        "_id": "699e1937-4cd3-4927-bbd5-a6dc35992e7f",
                        "code": "NW",
                        "title": {
                            "en": "Northwest"
                        }
                    }, {
                        "_id": "03422e69-2bfe-45dc-b80f-1451f34259d6",
                        "code": "SE",
                        "title": {
                            "en": "Southeast"
                        }
                    }, {
                        "_id": "306ac123-f088-4a6a-bdfd-8811a3946760",
                        "code": "SW",
                        "title": {
                            "en": "Southwest"
                        }
                    }
                ];
                await forEachAsync(data, async (a) => {
                    await Facing.create(a, {
                        language: config.get('defaultLanguage')
                    }, (err, doc, affected) => {
                        if(err) {
                            if(typeof err !== 'Error' && typeof err !== 'MongoError') {
                                console.log(err.message || 'Error')
                                return;
                            }
                        }
                        console.log(`Generated ${affected} records`)
                    })
                })
                process.exit()
            } catch (error) {
                console.log(error)
            }
        }
    })
})()