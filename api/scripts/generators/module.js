( async function() {
	'use strict'

	const dbConnect = require('../../db/connect')
    const { forEachAsync } = require(`${process.cwd()}/lib/helper`)
    const config = require('config')

	dbConnect(async (err, db) => {
        if (err) {
            console.log('Action fail')
        }
        else {
            try {
                const Module = require('../../models/module')
                const data = [
                    {
                        name: {
                            en: 'Project',
                            vi: 'Dự án'
                        },
                        permissions: [
                            'create',
                            'read',
                            'update',
                            'delete'
                        ],
                        _id: 'project-cms'
                    },
                    {
                        name: {
                            en: 'Property',
                            vi: 'Căn hộ'
                        },
                        permissions: [
                            'create',
                            'read',
                            'update',
                            'delete'
                        ],
                        _id: 'property-cms'
                    },
                    {
                        name: {
                            en: 'Customer Ticket',
                            vi: 'Thẻ yêu cầu của khách hàng'
                        },
                        permissions: [
                            'create',
                            'read',
                            'update',
                            'delete',
                            'forward'
                        ],
                        _id: 'listing-cms'
                    },
                    {
                        name: {
                            en: 'Inbox',
                            vi: 'Inbox'
                        },
                        permissions: [
                            'create',
                            'read',
                            'update',
                            'delete',
                            'forward'
                        ],
                        _id: 'inbox-cms'
                    },
                    {
                        name: {
                            en: 'Admin',
                            vi: 'Quản trị viên'
                        },
                        permissions: [
                            'create',
                            'read',
                            'update',
                            'delete'
                        ],
                        system: true,
                        _id: 'admin-cms'
                    },
                    // {
                    //     name: {
                    //         en: 'Staff',
                    //         vi: 'Nhân viên'
                    //     },
                    //     permissions: [
                    //         'create',
                    //         'read',
                    //         'update',
                    //         'delete'
                    //     ],
                    //     system: true,
                    //     _id: 'staff-cms'
                    // },
                    {
                        name: {
                            en: 'Member',
                            vi: 'Nhân viên'
                        },
                        permissions: [
                            'create',
                            'read',
                            'update',
                            'delete'
                        ],
                        system: true,
                        _id: 'member-cms'
                    },
                    {
                        name: {
                            en: 'Introductions',
                            vi: 'Giới thiệu'
                        },
                        permissions: [
                            'create',
                            'read',
                            'update',
                            'delete'
                        ],
                        _id: 'introduction-cms'
                    },
                    {
                        name: {
                            en: 'News',
                            vi: 'Tin tức'
                        },
                        permissions: [
                            'create',
                            'read',
                            'update',
                            'delete'
                        ],
                        _id: 'news-cms'
                    },
                    {
                        name: {
                            en: 'Multilingual',
                            vi: 'Quản lý đa ngôn ngữ'
                        },
                        permissions: [
                            'create',
                            'read',
                            'update',
                            'delete'
                        ],
                        system: true,
                        _id: 'multilingual-cms'
                    }
                ];
                await forEachAsync(data, async (a) => {
                    await Module.create(a, {
                        language: config.get('defaultLanguage')
                    }, (err, doc, affected) => {
                        if(err) {
                            if(typeof err !== 'Error' && typeof err !== 'MongoError') {
                                console.log(err.message || 'Error')
                                return;
                            }
                        }
                        console.log(`Generated ${affected} records`)
                    })
                })
                process.exit()
            } catch (error) {
                console.log(error)
            }
        }
    })
})()