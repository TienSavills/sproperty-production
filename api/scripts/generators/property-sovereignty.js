( function() {
	'use strict'

    const mongoose = require('mongoose')
    const Schema = mongoose.Schema;
    const dbConnect = require('../../db/connect')
    const { forEachAsync } = require(`${process.cwd()}/lib/helper`)
    const config = require('config')

	dbConnect(async (err, db) => {
        if (err) {
            console.log('Action fail')
        }
        else {
            try {
                const PropertySovereignty = require('../../models/propertySovereignty')
                const data = [
                    {
                        _id: 1,
                        title: 'Sổ hồng',
                    },
                    {
                        _id: 2,
                        title: 'Hợp đồng',
                    },
                ]
                await forEachAsync(data, async (a) => {
                    await PropertySovereignty.create(a, {
                        language: config.get('defaultLanguage')
                    }, (err, doc, affected) => {
                        if(err) {
                            if(typeof err !== 'Error' && typeof err !== 'MongoError') {
                                console.log(err.message || 'Error')
                                return;
                            }
                        }
                        console.log(`Generated ${affected} records`)
                    })
                })
                process.exit()
            } catch (error) {
                console.log(error)
            }
        }
    })
})()