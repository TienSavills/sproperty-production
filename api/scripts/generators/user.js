( function() {
	'use strict'

    // const mongoose = require('mongoose')
    // const Schema = mongoose.Schema
    const dbConnect = require('../../db/connect')
    const { forEachAsync } = require(`${process.cwd()}/lib/helper`)

	dbConnect(async (err, db) => {
        if (err) {
            console.log('Action fail')
        }
        else {
            try {
                const User = require('../../models/user')
                const users = [
                    {
                        firstName: 'Savills',
                        lastName: 'Admin',
                        password: '123456',
                        email: 'admin@savills.com',
                        roles: ['system'],
                        status: 'active'
                    },
                ]
                await forEachAsync(users, async (u) => {
                    await User.create(u, (err, doc, affected) => {
                        if(err) {
                            if(typeof err !== 'Error' && typeof err !== 'MongoError') {
                                console.log(err.message || 'Error')
                                return
                            }
                        }
                        console.log(`Generated ${affected} records`)
                    })
                })
                process.exit()
            } catch (error) {
                console.log(error)
            }
        }
    })
})()