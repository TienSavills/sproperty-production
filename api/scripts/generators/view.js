( async function() {
	'use strict'

    const dbConnect = require('../../db/connect')
    const { forEachAsync } = require(`${process.cwd()}/lib/helper`)
    const config = require('config')

	dbConnect(async (err, db) => {
        if (err) {
            console.log('Action fail')
        }
        else {
            try {
                const View = require('../../models/view')
                const data = [{
                    "_id": "8e203133-5501-4f13-843a-17548ae29a0d",
                    "code": "Sea View",
                    "title": {
                        "en": "Sea View"
                    }
                }, {
                    "_id": "7923a562-565e-4af3-ad77-23e369b87686",
                    "code": "City View",
                    "title": {
                        "en": "City View"
                    }
                }, {
                    "_id": "46e021da-ed91-4323-aef0-c3e6cd707f33",
                    "code": "Harbour View",
                    "title": {
                        "en": "Harbour View"
                    }
                }, {
                    "_id": "d843a6fc-3c58-4f0a-ac92-d0e195f524cb",
                    "code": "Green View",
                    "title": {
                        "en": "Green View"
                    }
                }, {
                    "_id": "16aacfe9-ed14-4b9e-ad12-2880ff32f83f",
                    "code": "Building View",
                    "title": {
                        "en": "Building View"
                    }
                }, {
                    "_id": "18e29f16-6894-45a6-ad5f-5f6162a34404",
                    "code": "Open View",
                    "title": {
                        "en": "Open View"
                    }
                }, {
                    "_id": "541c700a-ab60-49e0-9bf8-27347ea8f0c9",
                    "code": "Mountain View",
                    "title": {
                        "en": "Mountain View"
                    }
                }, {
                    "_id": "c32d3b87-617f-44b0-8b8b-cd659caa291d",
                    "code": "Garden View",
                    "title": {
                        "en": "Garden View"
                    }
                }, {
                    "_id": "a0578b97-bea9-4bc4-b6a5-9e7b59998f7d",
                    "code": "Pool View",
                    "title": {
                        "en": "Pool View"
                    }
                }, {
                    "_id": "48dcb5e6-5236-45d4-9d99-36654390d220",
                    "code": "Lake View",
                    "title": {
                        "en": "Lake View"
                    }
                }, {
                    "_id": "a6660e45-9d2c-4e00-b474-a8e5c6cf5d1d",
                    "code": "River View",
                    "title": {
                        "en": "River View"
                    }
                }, {
                    "_id": "e32787de-e40b-4e5b-9d55-f9558b5611f3",
                    "code": "Bay View",
                    "title": {
                        "en": "Bay View"
                    }
                }, {
                    "_id": "72b54e85-e049-4d86-a11e-2891bf062bb3",
                    "code": "Greenery View",
                    "title": {
                        "en": "Greenery View"
                    }
                }, {
                    "_id": "82712a0b-f214-4613-8b9f-079c5e8f9132",
                    "code": "Park View",
                    "title": {
                        "en": "Park View"
                    }
                }, {
                    "_id": "26db8133-5457-474c-9852-005e5c96cc61",
                    "code": "Dual Aspect View",
                    "title": {
                        "en": "Dual Aspect View"
                    }
                }, {
                    "_id": "51dce20e-16e5-46a4-92e9-12fd380461db",
                    "code": "Racecourse View",
                    "title": {
                        "en": "Racecourse View"
                    }
                }, {
                    "_id": "ca8074bd-c15a-4159-8f5c-846a3bc78c30",
                    "code": "Beach View",
                    "title": {
                        "en": "Beach View"
                    }
                }];
                await forEachAsync(data, async (a) => {
                    await View.create(a, {
                        language: config.get('defaultLanguage')
                    }, (err, doc, affected) => {
                        if(err) {
                            if(typeof err !== 'Error' && typeof err !== 'MongoError') {
                                console.log(err.message || 'Error')
                                return;
                            }
                        }
                        console.log(`Generated ${affected} records`)
                    })
                })
                process.exit()
            } catch (error) {
                console.log(error)
            }
        }
    })
})()