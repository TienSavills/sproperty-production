( async function() {
	'use strict'

    const dbConnect = require('../../db/connect')

	dbConnect((err, db) => {
        if (err) {
            console.log('Action fail')
        }
        else {
            const Country = require('../../models/country')
            const countries = ["Argentina", "Australia", "Bahrain", "Bangladesh", "Belize", "Bolivia", "Brazil", "Brunei Darussalam", "Chile", "China", "Colombia", "Costa Rica", "Ecuador", "El Salvador", "Guatemala", "Guyana", "Honduras", "Hong Kong", "India", "Indonesia", "Japan", "Kuwait", "Laos", "Macau", "Malaysia", "Maldives", "Mexico", "Mongolia", "Myanmar", "Nepal", "New Zealand", "Nicaragua", "Oman", "Pakistan", "Panama", "Paraguay", "Peru", "Philippines", "Qatar", "South Korea", "Sri Lanka", "Suriname", "Taiwan", "Thailand", "Tibet", "United Arab Emirates", "Uruguay", "Venezuela", "Vietnam"].map((name, i) => Object.assign({},{ _id: i + 1, name}))
            Country.import(countries, (err, docs, affected) => {
                if(err) console.log(err)
                else console.log(`Generated ${affected} records`)
                process.exit()
            })
        }
    })
})()