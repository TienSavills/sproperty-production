( function() {
	'use strict'

    const mongoose = require('mongoose')
    const Schema = mongoose.Schema;
    const dbConnect = require('../../db/connect')
    const { forEachAsync } = require(`${process.cwd()}/lib/helper`)
    const config = require('config')

	dbConnect(async (err, db) => {
        if (err) {
            console.log('Action fail')
        }
        else {
            try {
                const PublishingStatus = require('../../models/publishingStatus')
                const data = [
                    {
                        _id: 0,
                        code: 'Unpublished',
                        title: {
                            en: 'Unpublished'
                        }
                    },
                    {
                        _id: 1,
                        code: 'Published',
                        title: {
                            en: 'Published'
                        }
                    }
                ]
                await forEachAsync(data, async (a) => {
                    await PublishingStatus.create(a, {
                        language: config.get('defaultLanguage')
                    }, (err, doc, affected) => {
                        if(err) {
                            if(typeof err !== 'Error' && typeof err !== 'MongoError') {
                                console.log(err.message || 'Error')
                                return;
                            }
                        }
                        console.log(`Generated ${affected} records`)
                    })
                })
                process.exit()
            } catch (error) {
                console.log(error)
            }
        }
    })
})()