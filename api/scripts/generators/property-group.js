( function() {
	'use strict'

    const mongoose = require('mongoose')
    const Schema = mongoose.Schema;
    const dbConnect = require('../../db/connect')
    const { forEachAsync } = require(`${process.cwd()}/lib/helper`)
    const config = require('config')

	dbConnect(async (err, db) => {
        if (err) {
            console.log('Action fail')
        }
        else {
            try {
                const PropertyGroup = require('../../models/propertyGroup')
                const data = [
                    {
                        _id: 'basic',
                        code: 'Sơ cấp',
                        title: {
                            vi: 'Sơ cấp'
                        },
                        languages: ['vi']
                    },
                    {
                        _id: 'sale',
                        code: 'Bán',
                        title: {
                            vi: 'Bán'
                        },
                        languages: ['vi']
                    },
                    {
                        _id: 'rent',
                        code: 'Cho thuê',
                        title: {
                            vi: 'Cho thuê'
                        },
                        languages: ['vi']
                    }
                ]
                await forEachAsync(data, async (a) => {
                    await PropertyGroup.create(a, {
                        language: config.get('defaultLanguage')
                    }, (err, doc, affected) => {
                        if(err) {
                            if(typeof err !== 'Error' && typeof err !== 'MongoError') {
                                console.log(err.message || 'Error')
                                return;
                            }
                        }
                        console.log(`Generated ${affected} records`)
                    })
                })
                process.exit()
            } catch (error) {
                console.log(error)
            }
        }
    })
})()