( async function() {
	'use strict'

    const dbConnect = require('../../db/connect')

	dbConnect((err, db) => {
        if (err) {
            console.log('Action fail')
        }
        else {
            const Province = require('../../models/province')
            const data = [{
                "_id": "9afcf75e-b5f8-43f3-bf84-87c3a6f0edde",
                "name": "Ha Noi City",
                "status": 2
            }, {
                "_id": "b4798178-45eb-4700-b405-ec5a1724ef95",
                "name": "Ho Chi Minh City",
                "status": 2
            }, {
                "_id": "3d84c4b2-db27-4848-b49b-c9a03fabe759",
                "name": "Hai Phong City"
            }, {
                "_id": "2ccffb27-9693-4465-9953-7eddbf263f8f",
                "name": "Da Nang Province",
                "status": 2
            }, {
                "_id": "486c8833-8679-438c-bd63-712fb04be192",
                "name": "Bac Ninh Province"
            }, {
                "_id": "65072a61-1b45-409a-86ec-34abc07b7efe",
                "name": "Hai Duong Province"
            }, {
                "_id": "48f8b2bb-2c89-4ed1-8d82-f5322997f4b3",
                "name": "Hung Yen Province"
            }, {
                "_id": "953efb88-75c3-4c4a-8661-54aa4499a6c3",
                "name": "Ninh Binh Province"
            }, {
                "_id": "e397d0ae-b4b0-4e78-a445-cab555d40d34",
                "name": "Thanh Hoa Province"
            }, {
                "_id": "6faa3fb6-1ee6-4b40-8929-42adbe69360b",
                "name": "Thua Thien Hue Province"
            }, {
                "_id": "901ac059-7b08-40ee-9a79-b58aa3b11bf0",
                "name": "Quang Nam Province"
            }, {
                "_id": "48e5b9bc-c9d2-4ada-bdbd-039a24926529",
                "name": "Khanh Hoa Province"
            }, {
                "_id": "a4163df2-fc9a-4f7e-befc-dcea47984161",
                "name": "Lam Dong Province"
            }, {
                "_id": "692aac88-8eb6-42ee-af5d-c15143820e07",
                "name": "Binh Phuoc Province"
            }, {
                "_id": "d32da938-7416-4293-acf8-eb8aede4599e",
                "name": "Binh Duong Province"
            }, {
                "_id": "df94761a-f40f-4fb7-9f7c-fc485ce0a170",
                "name": "Tay Ninh Province"
            }, {
                "_id": "76122c40-5613-41c5-bcf6-2b4e4b489f13",
                "name": "Binh Thuan Province"
            }, {
                "_id": "279e120e-e7f8-4520-9152-4e212f5d06fa",
                "name": "Dong Nai Province"
            }, {
                "_id": "898cb79f-7548-4b2c-a8d4-79abda0858a0",
                "name": "Long An Province"
            }, {
                "_id": "4502d3e0-8137-4938-9f38-567c221f7635",
                "name": "Ba Ria Vung Tau Province"
            }, {
                "_id": "3d1a5267-48c5-4b34-a48a-61d07f9ecc87",
                "name": "Kien Giang Province"
            }, {
                "_id": "5ff3f0cc-24e7-403a-a6ee-ad6ed95429eb",
                "name": "Ha Giang City"
            }, {
                "_id": "17ebf75e-f5d1-4c40-814b-9cdd4aafbf56",
                "name": "Cao Bang Province"
            }, {
                "_id": "df644eb8-67bd-4b8b-8dd2-2e30aa16a342",
                "name": "Lai Chau Province"
            }, {
                "_id": "b5b15841-25fe-4037-b845-b91dca3cf8e7",
                "name": "Lao Cai Province"
            }, {
                "_id": "48412844-5c88-4874-9e23-b72eda3d8249",
                "name": "Tuyen Quang Province"
            }, {
                "_id": "f47c5bdc-c0fb-40a7-b4e1-88d511b4850a",
                "name": "Lang Son Province"
            }, {
                "_id": "5a6a7f67-a1f1-4b59-8bd6-0b8e743cf6ea",
                "name": "Bac Kan Province"
            }, {
                "_id": "5382c0eb-b02e-464c-93ef-fd6cbf1b22e9",
                "name": "Thai Nguyen Province"
            }, {
                "_id": "ad3f8915-5a22-4f19-9160-acfadbbe6132",
                "name": "Yen Bai Province"
            }, {
                "_id": "d99d4df7-8e6e-42bf-839d-24d3cd1d2aec",
                "name": "Son La Province"
            }, {
                "_id": "a8e3e626-f695-44d5-82eb-3d3f91a8eae8",
                "name": "Phu Tho Province"
            }, {
                "_id": "e02935b1-81f1-46c8-8db7-9cea77f39fc0",
                "name": "Vinh Phuc Province"
            }, {
                "_id": "0c8ccd22-7b4b-492c-ac96-24890229475a",
                "name": "Quang Ninh Province"
            }, {
                "_id": "1ab35786-c960-4905-b5e6-f368743a6e43",
                "name": "Bac Giang Province"
            }, {
                "_id": "c67ca245-d489-4396-8322-2e4564f05660",
                "name": "Hoa Binh Province"
            }, {
                "_id": "977e8c18-94aa-49e6-9097-7cccb41d31ad",
                "name": "Ha Nam Province"
            }, {
                "_id": "84c4cd6e-893e-42c6-a1a2-e4e0cb0eb52f",
                "name": "Nam Dinh Province"
            }, {
                "_id": "4cb96b2e-9281-4fa3-a513-65d099474ae6",
                "name": "Thai Binh Province"
            }, {
                "_id": "bae33f20-e617-41b3-9961-e3ed26383792",
                "name": "Nghe An Province"
            }, {
                "_id": "a2a4b629-5ac7-479e-9ba7-5aae0a28491e",
                "name": "Ha Tinh Province"
            }, {
                "_id": "5631c2f5-dbdd-402c-bc3d-16f081b330c8",
                "name": "Quang Binh Province"
            }, {
                "_id": "d25d924d-bc40-4cec-bd46-32265263ab04",
                "name": "Quang Tri Province"
            }, {
                "_id": "2886ae4d-88c9-4d26-9c76-89a015c2fe87",
                "name": "Quang Ngai Province"
            }, {
                "_id": "5c7cf387-d1df-4a67-b94b-c538b0aeb478",
                "name": "Kon Tum Province"
            }, {
                "_id": "6aee7058-f37d-497f-b27c-5da2dd9920d8",
                "name": "Binh Dinh Province"
            }, {
                "_id": "c11910a4-5d05-4956-aead-929c56153cf1",
                "name": "Gia Lai Province"
            }, {
                "_id": "e219db86-e832-4961-87d8-69252a34bdf3",
                "name": "Phu Yen Province"
            }, {
                "_id": "7921fbd6-1b35-4b58-8719-1ebdd8ca87a8",
                "name": "Dak Lak Province"
            }, {
                "_id": "7a536a60-933d-4570-a4cf-9d8e3bee8296",
                "name": "Ninh Thuan Province"
            }, {
                "_id": "da24c30c-3af0-48db-94a2-eccfb6a7b72a",
                "name": "Dong Thap Province"
            }, {
                "_id": "3acdcf41-4dd3-4e66-8d9f-7592e554468b",
                "name": "An Giang Province"
            }, {
                "_id": "0a626dba-e8ef-40a9-bd07-75dd55156be0",
                "name": "Tien Giang Province"
            }, {
                "_id": "366d997d-340b-4439-a6b2-68639392636b",
                "name": "Tra Vinh Province"
            }, {
                "_id": "3a08545e-7472-460c-a0c0-71110ad507f7",
                "name": "Ben Tre Province"
            }, {
                "_id": "f8940050-be13-420c-bf6f-1fd4cd206de2",
                "name": "Vinh Long Province"
            }, {
                "_id": "3a957787-97fb-489d-bd29-8ef8439e05af",
                "name": "Tra Vinh Province"
            }, {
                "_id": "97ea0b9f-fddc-4693-bd0d-7296dfb99206",
                "name": "Soc Trang Province"
            }, {
                "_id": "79303750-75c9-429b-acf4-f759510084b2",
                "name": "Bac Lieu Province"
            }, {
                "_id": "fcd9b8be-4ff0-48d7-ade4-96f517d651d7",
                "name": "Ca Mau Province"
            }, {
                "_id": "cf29115c-0597-4dc6-9f2a-13655650dbb1",
                "name": "Dien Bien Province"
            }, {
                "_id": "ec56f9c0-6745-438a-a8c4-41ce8fbcc3ce",
                "name": "Dak Nong Province"
            }, {
                "_id": "e52dadd4-11bc-4531-8106-8112d4ccba39",
                "name": "Hau Giang Province"
            }];
            Province.import(data, (err, docs, affected) => {
                if(err) console.log(err)
                else console.log(`Generated ${affected} records`)
                process.exit()
            })
        }
    })
})()