( async function() {
	'use strict'

    const dbConnect = require('../../db/connect')

	dbConnect((err, db) => {
        if (err) {
            console.log('Action fail')
        }
        else {
            const Currency = require('../../models/currency')
            const data = [{
                "_id": "usd",
                "code": "USD",
                "name": "USD"
            }, {
                "_id": "vnd",
                "code": "VND",
                "name": "VND"
            }, {
                "_id": "mi-vnd",
                "code": "Triệu VND",
                "name": "Triệu VND"
            }, {
                "_id": "bi-vnd",
                "code": "Tỷ VND",
                "name": "Tỷ VND"
            }, {
                "_id": "cny",
                "code": "CNY",
                "name": "CNY"
            }];
            Currency.import(data, (err, docs, affected) => {
                if(err) console.log(err)
                else console.log(`Generated ${affected} records`)
                process.exit()
            })
        }
    })
})()