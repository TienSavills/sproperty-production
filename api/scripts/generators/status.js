( function() {
	'use strict'

    const mongoose = require('mongoose')
    const Schema = mongoose.Schema;
    const dbConnect = require('../../db/connect')
    const { forEachAsync } = require(`${process.cwd()}/lib/helper`)
    const config = require('config')

	dbConnect(async (err, db) => {
        if (err) {
            console.log('Action fail')
        }
        else {
            try {
                const Status = require('../../models/status')
                const data = [
                    {
                        _id: 0,
                        title: {
                            en: 'Deleted',
                            vi: 'Đã xóa'
                        }
                    },
                    {
                        _id: 1,
                        title: {
                            en: 'Default',
                            vi: 'Mặc định'
                        }
                    },
                    {
                        _id: 2,
                        title: {
                            en: 'Activated',
                            vi: 'Đã kích hoạt'
                        }
                    }
                ]
                await forEachAsync(data, async (a) => {
                    await Status.create(a, {
                        language: config.get('defaultLanguage')
                    }, (err, doc, affected) => {
                        if(err) {
                            if(typeof err !== 'Error' && typeof err !== 'MongoError') {
                                console.log(err.message || 'Error')
                                return;
                            }
                        }
                        console.log(`Generated ${affected} records`)
                    })
                })
                process.exit()
            } catch (error) {
                console.log(error)
            }
        }
    })
})()