( function() {
	'use strict'

	const dbConnect = require('../../db/connect')

	dbConnect(async (err, db) => {
        if (err) {
            console.log('Action fail')
        }
        else {
            try {
                const UserRole = require('../../models/userRole')
                UserRole.insertMany([
                    {
                        _id: 'system',
                        name: 'System',
                        level: 0,
                        master: true
                    },
                ], (err, docs) => {
                    if(err) console.log(err)
                    else console.log(`Generated ${docs.length} records`)
                    process.exit()
                })
            } catch (error) {
                console.log(error)
            }
        }
    })
})()