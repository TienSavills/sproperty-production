( function() {
	'use strict'

    const mongoose = require('mongoose')
    const Schema = mongoose.Schema;
    const dbConnect = require('../../db/connect')
    const { forEachAsync } = require(`${process.cwd()}/lib/helper`)
    const config = require('config')

	dbConnect(async (err, db) => {
        if (err) {
            console.log('Action fail')
        }
        else {
            try {
                const Amenities = require('../../models/amenities')
                const data = [{
                    "_id": "b04d2b08-82a7-4904-9c8a-3370ef62f858",
                    "code": "Balcony",
                    "title": {
                        "en": "Balcony"
                    }
                }, {
                    "_id": "781ad389-db00-407d-a969-9f1207f95ede",
                    "code": "Roof Terrace",
                    "title": {
                        "en": "Roof Terrace"
                    }
                }, {
                    "_id": "b6ceea52-5cf0-4a70-817a-88f9babd22c4",
                    "code": "Walk in Closet",
                    "title": {
                        "en": "Walk in Closet"
                    }
                }, {
                    "_id": "4a1a622c-71e8-42df-b5c6-92d26ff635f7",
                    "code": "Study Room",
                    "title": {
                        "en": "Study Room"
                    }
                }, {
                    "_id": "e6e32d66-5784-4f80-acbe-5933972a25f8",
                    "code": "Yard",
                    "title": {
                        "en": "Yard"
                    }
                }, {
                    "_id": "2fc4261f-902d-4612-b71e-23bfd378e87a",
                    "code": "Helper's Quarters",
                    "title": {
                        "en": "Helper's Quarters"
                    }
                }, {
                    "_id": "89bfa92c-2fb7-40a3-8bd9-899f39657b27",
                    "code": "Utility Area",
                    "title": {
                        "en": "Utility Area"
                    }
                }, {
                    "_id": "946b3210-9077-4612-ac15-c83b026986a1",
                    "code": "Jacuzzi",
                    "title": {
                        "en": "Jacuzzi"
                    }
                }, {
                    "_id": "abd6feac-3547-45eb-b2b8-effcace4338a",
                    "code": "Aerobic Room",
                    "title": {
                        "en": "Aerobic Room"
                    }
                }, {
                    "_id": "fba7e205-3d78-4c98-8826-a4f65e8bb762",
                    "code": "Gymnasium",
                    "title": {
                        "en": "Gymnasium"
                    }
                }, {
                    "_id": "b7f80c6e-c230-48b1-84f3-4cfc15e50f68",
                    "code": "Fitness Center",
                    "title": {
                        "en": "Fitness Center"
                    }
                }, {
                    "_id": "365819d5-5e6d-4c53-9f4f-015ecb66560d",
                    "code": "Badminton Court",
                    "title": {
                        "en": "Badminton Court"
                    }
                }, {
                    "_id": "52070d56-1bed-4e4e-8e81-8126f9a64cb0",
                    "code": "Basketball Court",
                    "title": {
                        "en": "Basketball Court"
                    }
                }, {
                    "_id": "31e912d1-d8a6-44fc-b5e3-1f1d2e296a0c",
                    "code": "Function Room",
                    "title": {
                        "en": "Function Room"
                    }
                }, {
                    "_id": "6c7287a3-523b-43bf-81b0-15155a427cfd",
                    "code": "Children's playground",
                    "title": {
                        "en": "Children's playground"
                    }
                }, {
                    "_id": "1adb2382-ec9a-4e65-a0cb-f7d11a517840",
                    "code": "Club house",
                    "title": {
                        "en": "Club house"
                    }
                }, {
                    "_id": "cc4044dc-235a-474c-b562-3850ca68f92f",
                    "code": "Billiards Room",
                    "title": {
                        "en": "Billiards Room"
                    }
                }, {
                    "_id": "5bff59b2-f851-4837-922e-f0407983ca6d",
                    "code": "Golf Driving Range",
                    "title": {
                        "en": "Golf Driving Range"
                    }
                }, {
                    "_id": "6bb78184-3855-494f-90b3-4fc0606a1847",
                    "code": "Squash Court",
                    "title": {
                        "en": "Squash Court"
                    }
                }, {
                    "_id": "283fbcfe-1ab3-4733-91db-fafc5347b9f5",
                    "code": "Table Tennis",
                    "title": {
                        "en": "Table Tennis"
                    }
                }, {
                    "_id": "0b3bff5f-9f94-4cb4-958d-54a072a2747a",
                    "code": "Tennis Court",
                    "title": {
                        "en": "Tennis Court"
                    }
                }, {
                    "_id": "574e8266-668c-4bfa-80ef-e00bfc2505d0",
                    "code": "Swimming Pool",
                    "title": {
                        "en": "Swimming Pool"
                    }
                }, {
                    "_id": "17bb24a6-ea20-49f5-b2a3-94c1ba87137a",
                    "code": "Garden",
                    "title": {
                        "en": "Garden"
                    }
                }, {
                    "_id": "4e9bc299-194b-4012-91ed-f500e1281a04",
                    "code": "Meeting Room",
                    "title": {
                        "en": "Meeting Room"
                    }
                }, {
                    "_id": "01f06947-ffca-4298-b79e-f8aba450e441",
                    "code": "Games Room",
                    "title": {
                        "en": "Games Room"
                    }
                }, {
                    "_id": "62f318d3-f6b5-44f1-b01f-9ecfdae7a7ad",
                    "code": "Sauna Room",
                    "title": {
                        "en": "Sauna Room"
                    }
                }, {
                    "_id": "7430c5a1-02b6-45d0-8f49-295946038f14",
                    "code": "Yoga Room",
                    "title": {
                        "en": "Yoga Room"
                    }
                }, {
                    "_id": "4265b3b6-3004-4920-84b4-39a2030bf64f",
                    "code": "Steam Room",
                    "title": {
                        "en": "Steam Room"
                    }
                }, {
                    "_id": "212f45f5-40f5-4fe1-bbfe-db3aeb66d218",
                    "code": "Library",
                    "title": {
                        "en": "Library"
                    }
                }, {
                    "_id": "50852587-bb0f-4e7d-bee1-98632f7b1693",
                    "code": "BBQ",
                    "title": {
                        "en": "BBQ"
                    }
                }, {
                    "_id": "8e07646d-8c9d-4933-b7b3-89d9eb74fd49",
                    "code": "Spa",
                    "title": {
                        "en": "Spa"
                    }
                }, {
                    "_id": "2a93d32b-8285-486e-9832-0a6eea3c419e",
                    "code": "Lift",
                    "title": {
                        "en": "Lift"
                    }
                }];
                await forEachAsync(data, async (a) => {
                    await Amenities.create(a, {
                        language: config.get('defaultLanguage')
                    }, (err, doc, affected) => {
                        if(err) {
                            if(typeof err !== 'Error' && typeof err !== 'MongoError') {
                                console.log(err.message || 'Error')
                                return;
                            }
                        }
                        console.log(`Generated ${affected} records`)
                    })
                })
                process.exit()
            } catch (error) {
                console.log(error)
            }
        }
    })
})()