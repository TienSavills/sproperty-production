( function() {
	'use strict'

    const mongoose = require('mongoose')
    const Schema = mongoose.Schema;
    const dbConnect = require('../../db/connect')
    const { forEachAsync } = require(`${process.cwd()}/lib/helper`)
    const config = require('config')

	dbConnect(async (err, db) => {
        if (err) {
            console.log('Action fail')
        }
        else {
            try {
                const Menu = require('../../models/menu')
                const data = [
                    {
                        name: 'Header menu - Tiếng Việt',
                        type: 'header',
                        language: 'vi'
                    },
                    {
                        name: 'Header menu - English',
                        type: 'header',
                        language: 'en'
                    },
                    {
                        name: 'Header menu - Chinese',
                        type: 'header',
                        language: 'zh'
                    },
                    {
                        name: 'Footer menu - Tiếng Việt',
                        type: 'footer',
                        language: 'vi'
                    },
                    {
                        name: 'Footer menu - English',
                        type: 'footer',
                        language: 'en'
                    },
                    {
                        name: 'Footer menu - Chinese',
                        type: 'footer',
                        language: 'zh'
                    }
                ]
                await forEachAsync(data, async (a) => {
                    await Menu.create(a, (err, doc, affected) => {
                        if(err) {
                            if(typeof err !== 'Error' && typeof err !== 'MongoError') {
                                console.log(err.message || 'Error')
                                return;
                            }
                        }
                        console.log(`Generated ${affected} records`)
                    })
                })
                process.exit()
            } catch (error) {
                console.log(error)
            }
        }
    })
})()