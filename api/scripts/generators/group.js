( async function() {
	'use strict'

	const dbConnect = require('../../db/connect')
    const config = require('config')

	dbConnect((err, db) => {
        if (err) {
            console.log('Action fail')
        }
        else {
            const Group = require('../../models/group')
            Group.insertMany([
                {
                    _id: config.get('defaultGroup.id'),
                    name: config.get('defaultGroup.name')
                },
                {
                    _id: 'savills',
                    name: 'Savills'
                }
            ], (err, docs) => {
                if(err) console.log(err)
                else console.log(`Generated ${docs.length} records`)
                process.exit()
            })
        }
    })
})()