( async function() {
	'use strict'

    const dbConnect = require('../../db/connect')

	dbConnect((err, db) => {
        if (err) {
            console.log('Action fail')
        }
        else {
            const District = require('../../models/district')
            const data = [{
                "_id": "e83b5cef-20a4-45c6-8ffc-6f619a5cbca2",
                "name": "Ba Dinh District",
                "province": "9afcf75e-b5f8-43f3-bf84-87c3a6f0edde"
            }, {
                "_id": "42825b78-baac-4915-ada7-f30d66b3a4fb",
                "name": "Hoan Kiem District",
                "province": "9afcf75e-b5f8-43f3-bf84-87c3a6f0edde"
            }, {
                "_id": "8b6753d4-b60c-4017-8fad-8e6721734525",
                "name": "Hai Ba Trung District",
                "province": "9afcf75e-b5f8-43f3-bf84-87c3a6f0edde"
            }, {
                "_id": "f0c1f13b-735c-4d2e-a530-475de0b0a423",
                "name": "Dong Da District",
                "province": "9afcf75e-b5f8-43f3-bf84-87c3a6f0edde"
            }, {
                "_id": "3c6a92b4-a1d6-4ee9-9fda-e322cfdeeda5",
                "name": "Tay Ho District",
                "province": "9afcf75e-b5f8-43f3-bf84-87c3a6f0edde"
            }, {
                "_id": "57bb1f3c-9f1c-4c92-aa22-b1a3a2d1baee",
                "name": "Cau Giay District",
                "province": "9afcf75e-b5f8-43f3-bf84-87c3a6f0edde"
            }, {
                "_id": "76f58010-aefc-43f7-b332-7efb2dddcb06",
                "name": "Thanh Xuan District",
                "province": "9afcf75e-b5f8-43f3-bf84-87c3a6f0edde"
            }, {
                "_id": "5481f7ce-b112-4695-bc9e-826e070bb93d",
                "name": "Hoang Mai District",
                "province": "9afcf75e-b5f8-43f3-bf84-87c3a6f0edde"
            }, {
                "_id": "a4a6a02b-8cd5-4fb3-900f-f186dc07b872",
                "name": "Long Bien District",
                "province": "9afcf75e-b5f8-43f3-bf84-87c3a6f0edde"
            }, {
                "_id": "7f9074fe-50b3-4658-a90b-97eef7ecee3f",
                "name": "Tu Liem District",
                "province": "9afcf75e-b5f8-43f3-bf84-87c3a6f0edde"
            }, {
                "_id": "521a9244-ddaf-4fe4-9d07-8eae6573f67b",
                "name": "Thanh Tri District",
                "province": "9afcf75e-b5f8-43f3-bf84-87c3a6f0edde"
            }, {
                "_id": "035e11ae-5378-41e4-93dc-a914228f3493",
                "name": "Gia Lam District",
                "province": "9afcf75e-b5f8-43f3-bf84-87c3a6f0edde"
            }, {
                "_id": "14ad9013-b1c6-40ea-b50a-2147c77e7895",
                "name": "Dong Anh District",
                "province": "9afcf75e-b5f8-43f3-bf84-87c3a6f0edde"
            }, {
                "_id": "a45db657-3666-4eff-9543-671a02c506c9",
                "name": "Soc Son District",
                "province": "9afcf75e-b5f8-43f3-bf84-87c3a6f0edde"
            }, {
                "_id": "4992afcf-23c6-48a9-a17b-179f81a8449c",
                "name": "Ha Dong District",
                "province": "9afcf75e-b5f8-43f3-bf84-87c3a6f0edde"
            }, {
                "_id": "7f0ade30-28f7-4e97-87da-b9e63446cba9",
                "name": "Son Tay District",
                "province": "9afcf75e-b5f8-43f3-bf84-87c3a6f0edde"
            }, {
                "_id": "d4c56231-2a43-4e2d-8cc0-dc8af75ddbe3",
                "name": "Ba Vi District",
                "province": "9afcf75e-b5f8-43f3-bf84-87c3a6f0edde"
            }, {
                "_id": "d9a28a25-f486-4576-a37e-7a44c2a884b9",
                "name": "Phuc Tho District",
                "province": "9afcf75e-b5f8-43f3-bf84-87c3a6f0edde"
            }, {
                "_id": "c0fcf85f-2e70-4067-952a-75879b9c3af3",
                "name": "Thach That Town",
                "province": "9afcf75e-b5f8-43f3-bf84-87c3a6f0edde"
            }, {
                "_id": "45a57ca6-5877-4fbd-9543-1ae7b74dc365",
                "name": "Quoc Oai District",
                "province": "9afcf75e-b5f8-43f3-bf84-87c3a6f0edde"
            }, {
                "_id": "ef56d137-0694-4e36-a812-567f914662c2",
                "name": "Chuong My District",
                "province": "9afcf75e-b5f8-43f3-bf84-87c3a6f0edde"
            }, {
                "_id": "05b2bfaa-dbc8-4121-bf78-eccd0ba3d88f",
                "name": "Dan Phuong District",
                "province": "9afcf75e-b5f8-43f3-bf84-87c3a6f0edde"
            }, {
                "_id": "3c4885e4-7738-4996-af58-aac4458d24cf",
                "name": "Hoai Duc District",
                "province": "9afcf75e-b5f8-43f3-bf84-87c3a6f0edde"
            }, {
                "_id": "1fb4751c-2c67-4991-8325-9fbfa6ecb04a",
                "name": "Thanh Oai District",
                "province": "9afcf75e-b5f8-43f3-bf84-87c3a6f0edde"
            }, {
                "_id": "becc3e34-7e46-4155-9eb8-dab61af787b4",
                "name": "My Duc District",
                "province": "9afcf75e-b5f8-43f3-bf84-87c3a6f0edde"
            }, {
                "_id": "dab27b88-07dd-45eb-b828-8fb2c672792a",
                "name": "Ung Hoa District",
                "province": "9afcf75e-b5f8-43f3-bf84-87c3a6f0edde"
            }, {
                "_id": "29da9ff9-0b3a-48d5-b2ee-c57c3d604c1f",
                "name": "Thuong Tin District",
                "province": "9afcf75e-b5f8-43f3-bf84-87c3a6f0edde"
            }, {
                "_id": "e6acb811-9c47-460a-aa4a-d731cd173b46",
                "name": "Phu Xuyen District",
                "province": "9afcf75e-b5f8-43f3-bf84-87c3a6f0edde"
            }, {
                "_id": "db336d57-e083-4db4-aa61-a67262bb3027",
                "name": "Me Linh District",
                "province": "9afcf75e-b5f8-43f3-bf84-87c3a6f0edde"
            }, {
                "_id": "55c6949a-0c0b-497b-a10a-c3ada80d4249",
                "name": "Thu Duc District",
                "province": "b4798178-45eb-4700-b405-ec5a1724ef95"
            }, {
                "_id": "1253d11b-3f72-458c-a2ae-6bc137d8f2fa",
                "name": "Binh Tan District",
                "province": "b4798178-45eb-4700-b405-ec5a1724ef95"
            }, {
                "_id": "d694ede6-82ff-432f-8db2-d4eec3c7ca84",
                "name": "Binh Chanh District",
                "province": "b4798178-45eb-4700-b405-ec5a1724ef95"
            }, {
                "_id": "2ac74bc2-27fd-4c50-860c-ded51aea087d",
                "name": "Cu Chi District",
                "province": "b4798178-45eb-4700-b405-ec5a1724ef95"
            }, {
                "_id": "aa580c03-68aa-45ed-a0b0-b78b56b7e774",
                "name": "Hoc Mon District",
                "province": "b4798178-45eb-4700-b405-ec5a1724ef95"
            }, {
                "_id": "8f6e8e56-7199-40b2-866c-0034d1268291",
                "name": "Nha Be District",
                "province": "b4798178-45eb-4700-b405-ec5a1724ef95"
            }, {
                "_id": "805e276c-14eb-4fd6-8bb1-3409ce4228e8",
                "name": "Can Gio District",
                "province": "b4798178-45eb-4700-b405-ec5a1724ef95"
            }, {
                "_id": "ff6a7a29-a46c-4d0d-b9b6-01cf628fb3ef",
                "name": "District 9",
                "province": "b4798178-45eb-4700-b405-ec5a1724ef95"
            }, {
                "_id": "7816b3d6-40c0-4975-bfc4-844c5e7dea43",
                "name": "District 10",
                "province": "b4798178-45eb-4700-b405-ec5a1724ef95"
            }, {
                "_id": "36c71ed2-cdcd-4391-8628-deef6565b5f2",
                "name": "District 11",
                "province": "b4798178-45eb-4700-b405-ec5a1724ef95"
            }, {
                "_id": "b0d243a5-7aeb-4234-8837-8ba03150fc8f",
                "name": "District 12",
                "province": "b4798178-45eb-4700-b405-ec5a1724ef95"
            }, {
                "_id": "83372029-67fa-4566-9017-bfbc6bbf31fd",
                "name": "Go Vap District",
                "province": "b4798178-45eb-4700-b405-ec5a1724ef95"
            }, {
                "_id": "71225ae0-c2f0-488f-a716-2e6733d0d96b",
                "name": "Tan Binh District",
                "province": "b4798178-45eb-4700-b405-ec5a1724ef95"
            }, {
                "_id": "b86cc892-bf09-4016-ade7-63ab64a4364d",
                "name": "Tan Phu District",
                "province": "b4798178-45eb-4700-b405-ec5a1724ef95"
            }, {
                "_id": "dc669577-7f08-4a91-a90e-dd01d60bec98",
                "name": "Binh Thanh District",
                "province": "b4798178-45eb-4700-b405-ec5a1724ef95"
            }, {
                "_id": "7709791c-ae08-4470-9bad-a60f6314aad5",
                "name": "Phu Nhuan District",
                "province": "b4798178-45eb-4700-b405-ec5a1724ef95"
            }, {
                "_id": "7a4c314e-dca5-4cf9-b3de-565a0dce3b58",
                "name": "District 1",
                "province": "b4798178-45eb-4700-b405-ec5a1724ef95"
            }, {
                "_id": "48d3b0b1-fb1a-4d17-9189-64472dd4ffce",
                "name": "District 2",
                "province": "b4798178-45eb-4700-b405-ec5a1724ef95"
            }, {
                "_id": "978ed715-e5d8-4b1d-b637-fe3fe2ce5e2c",
                "name": "District 3",
                "province": "b4798178-45eb-4700-b405-ec5a1724ef95"
            }, {
                "_id": "144509ec-19c8-4ab3-92d9-b49a62ca2235",
                "name": "District 4",
                "province": "b4798178-45eb-4700-b405-ec5a1724ef95"
            }, {
                "_id": "d9d8ad5e-0c9c-4c0b-8f72-2676c1cfe301",
                "name": "District 5",
                "province": "b4798178-45eb-4700-b405-ec5a1724ef95"
            }, {
                "_id": "969d36f1-45c7-4bad-9f41-6801d5df986f",
                "name": "District 6",
                "province": "b4798178-45eb-4700-b405-ec5a1724ef95"
            }, {
                "_id": "ae9e5496-80c5-475c-91f2-ac2c9334c670",
                "name": "District 7",
                "province": "b4798178-45eb-4700-b405-ec5a1724ef95"
            }, {
                "_id": "832aca55-ab74-4d9a-8e7e-251245aeb7bf",
                "name": "District 8",
                "province": "b4798178-45eb-4700-b405-ec5a1724ef95"
            }, {
                "_id": "beb88dd2-c46a-4207-bc96-ddb49c33f983",
                "name": "Hong Bang District",
                "province": "3d84c4b2-db27-4848-b49b-c9a03fabe759"
            }, {
                "_id": "69135214-17b0-4e29-a3b9-602c2dc0e931",
                "name": "Le Chan District",
                "province": "3d84c4b2-db27-4848-b49b-c9a03fabe759"
            }, {
                "_id": "ad1ebbaa-f0db-4326-bf4f-1b078e0ec5c0",
                "name": "Ngo Quyen District",
                "province": "3d84c4b2-db27-4848-b49b-c9a03fabe759"
            }, {
                "_id": "3de661c7-f357-4337-91c0-c1aa88aaecd1",
                "name": "Kien An District",
                "province": "3d84c4b2-db27-4848-b49b-c9a03fabe759"
            }, {
                "_id": "691a1bd7-49dc-4ada-a2de-22307e495f57",
                "name": "Hai An District",
                "province": "3d84c4b2-db27-4848-b49b-c9a03fabe759"
            }, {
                "_id": "1bf54d21-f46d-42ff-bac9-3a796302c3ce",
                "name": "Do Son District",
                "province": "3d84c4b2-db27-4848-b49b-c9a03fabe759"
            }, {
                "_id": "edc5e6da-c006-458d-83ef-11108c10f5aa",
                "name": "An Lao District",
                "province": "3d84c4b2-db27-4848-b49b-c9a03fabe759"
            }, {
                "_id": "1ee4c413-8730-4dd4-b504-d45de97c5857",
                "name": "Kien Thuy District",
                "province": "3d84c4b2-db27-4848-b49b-c9a03fabe759"
            }, {
                "_id": "4d540889-d002-4ba5-9d08-73e55cbd75e0",
                "name": "Thuy Nguyen District",
                "province": "3d84c4b2-db27-4848-b49b-c9a03fabe759"
            }, {
                "_id": "d1d98c81-c7f8-473d-b29d-bfd36a1018c6",
                "name": "An Duong District",
                "province": "3d84c4b2-db27-4848-b49b-c9a03fabe759"
            }, {
                "_id": "dda8e705-e043-41ff-b8a8-f7428342b5bc",
                "name": "Tien Lang District",
                "province": "3d84c4b2-db27-4848-b49b-c9a03fabe759"
            }, {
                "_id": "2c4c8203-83a7-4148-bdfc-4f8440dc7990",
                "name": "Vinh Bao District",
                "province": "3d84c4b2-db27-4848-b49b-c9a03fabe759"
            }, {
                "_id": "616a4d32-8674-4cdc-86e0-22def291ab2b",
                "name": "Cat Hai District",
                "province": "3d84c4b2-db27-4848-b49b-c9a03fabe759"
            }, {
                "_id": "f480734e-7a7c-4d86-97ac-6bc51b907b57",
                "name": "Bach Long Vi District",
                "province": "3d84c4b2-db27-4848-b49b-c9a03fabe759"
            }, {
                "_id": "a43ab8b6-cdc4-4bf2-89e2-201244cdd5bd",
                "name": "Duong Kinh District",
                "province": "3d84c4b2-db27-4848-b49b-c9a03fabe759"
            }, {
                "_id": "6885f2f6-398a-476b-8b59-582d1d8d1a4b",
                "name": "Hai Chau District",
                "province": "2ccffb27-9693-4465-9953-7eddbf263f8f"
            }, {
                "_id": "ea7c4622-a9bd-4718-b3f4-fee8d2937e73",
                "name": "Thanh Khe District",
                "province": "2ccffb27-9693-4465-9953-7eddbf263f8f"
            }, {
                "_id": "b167cf66-aaee-4919-9fce-e70548107070",
                "name": "Son Tra District",
                "province": "2ccffb27-9693-4465-9953-7eddbf263f8f"
            }, {
                "_id": "80441fc2-125b-48b0-a43f-fe067562e497",
                "name": "Ngu Hanh Son District",
                "province": "2ccffb27-9693-4465-9953-7eddbf263f8f"
            }, {
                "_id": "56db637d-5491-461b-b4aa-1992bb182969",
                "name": "Lien Chieu District",
                "province": "2ccffb27-9693-4465-9953-7eddbf263f8f"
            }, {
                "_id": "798b41c6-7607-4f2b-a74a-2a34d22518bf",
                "name": "Hoa Vang District",
                "province": "2ccffb27-9693-4465-9953-7eddbf263f8f"
            }, {
                "_id": "777240f2-a34f-4730-b27c-a865b13dc82d",
                "name": "Cam Le District",
                "province": "2ccffb27-9693-4465-9953-7eddbf263f8f"
            }, {
                "_id": "6f8553c0-f141-44b7-b3cd-f78635ea9f78",
                "name": "Bac Ninh City",
                "province": "486c8833-8679-438c-bd63-712fb04be192"
            }, {
                "_id": "842a7c3e-90c2-4d25-994c-0ba3a4275e93",
                "name": "Yen Phong District",
                "province": "486c8833-8679-438c-bd63-712fb04be192"
            }, {
                "_id": "b2b15935-4d2e-47b7-98bb-a01263cc3a59",
                "name": "Que Vo District",
                "province": "486c8833-8679-438c-bd63-712fb04be192"
            }, {
                "_id": "6cf990e6-84cb-4062-88c3-2424d175cdf4",
                "name": "Tien Du District",
                "province": "486c8833-8679-438c-bd63-712fb04be192"
            }, {
                "_id": "f60b861d-1066-401e-9f87-d39cd1f3aa38",
                "name": "Tu Son Town",
                "province": "486c8833-8679-438c-bd63-712fb04be192"
            }, {
                "_id": "13d7d532-c372-4dbc-8b49-c528ceeb9943",
                "name": "Thuan Thanh District",
                "province": "486c8833-8679-438c-bd63-712fb04be192"
            }, {
                "_id": "eb6f97a3-d711-4a00-9d05-860ab229f14a",
                "name": "Gia Binh District",
                "province": "486c8833-8679-438c-bd63-712fb04be192"
            }, {
                "_id": "cd1219e3-098e-4893-9247-c4906e5e21bb",
                "name": "Luong Tai District",
                "province": "486c8833-8679-438c-bd63-712fb04be192"
            }, {
                "_id": "f2794e88-2036-4e8b-9256-c74146217b10",
                "name": "Hai Duong City",
                "province": "65072a61-1b45-409a-86ec-34abc07b7efe"
            }, {
                "_id": "074be11b-1bca-49da-aa54-cd80ee21c0a5",
                "name": "Chi Linh Town",
                "province": "65072a61-1b45-409a-86ec-34abc07b7efe"
            }, {
                "_id": "4f893616-d6dc-4468-9f3c-bcc8cbfcee50",
                "name": "Nam Sach District",
                "province": "65072a61-1b45-409a-86ec-34abc07b7efe"
            }, {
                "_id": "e3d17853-a12f-4172-b739-47861feed065",
                "name": "Kinh Mon District",
                "province": "65072a61-1b45-409a-86ec-34abc07b7efe"
            }, {
                "_id": "43e79c51-4719-4662-9a61-a9cb389c6490",
                "name": "Gia Loc District",
                "province": "65072a61-1b45-409a-86ec-34abc07b7efe"
            }, {
                "_id": "97d44d0d-4929-4b6d-9d96-3ab4c51a8c04",
                "name": "Tu Ky District",
                "province": "65072a61-1b45-409a-86ec-34abc07b7efe"
            }, {
                "_id": "1702389e-254f-4aa4-836d-d7e5cc8cb5bc",
                "name": "Thanh Mien District",
                "province": "65072a61-1b45-409a-86ec-34abc07b7efe"
            }, {
                "_id": "4d1514bf-1253-411c-a25b-75afea00b478",
                "name": "Ninh Giang District",
                "province": "65072a61-1b45-409a-86ec-34abc07b7efe"
            }, {
                "_id": "3196a929-9c27-497a-9ae2-c7f8650ec911",
                "name": "Cam Giang District",
                "province": "65072a61-1b45-409a-86ec-34abc07b7efe"
            }, {
                "_id": "97e1b611-7ecf-4230-b636-caa7d19a5d21",
                "name": "Thanh Ha District",
                "province": "65072a61-1b45-409a-86ec-34abc07b7efe"
            }, {
                "_id": "861b9da7-94b0-4309-8d47-91ff7ae2678d",
                "name": "Kim Thanh District",
                "province": "65072a61-1b45-409a-86ec-34abc07b7efe"
            }, {
                "_id": "c0259574-0610-453a-a0e0-4a5ff9886b1b",
                "name": "Binh Giang District",
                "province": "65072a61-1b45-409a-86ec-34abc07b7efe"
            }, {
                "_id": "06402661-8001-457b-9680-04b5d4098f00",
                "name": "Hung Yen City",
                "province": "48f8b2bb-2c89-4ed1-8d82-f5322997f4b3"
            }, {
                "_id": "b4270598-6cfb-4031-aa71-781b1fd71f63",
                "name": "Kim Dong District",
                "province": "48f8b2bb-2c89-4ed1-8d82-f5322997f4b3"
            }, {
                "_id": "94478023-5e58-4382-ba82-0708d3d918b9",
                "name": "An Thi District",
                "province": "48f8b2bb-2c89-4ed1-8d82-f5322997f4b3"
            }, {
                "_id": "cd24c776-f711-4ed9-a8f9-91fef6146ce8",
                "name": "Khoai Chau District",
                "province": "48f8b2bb-2c89-4ed1-8d82-f5322997f4b3"
            }, {
                "_id": "2e102ba2-ef2e-4e0e-a054-3c188c87a410",
                "name": "Yen My District",
                "province": "48f8b2bb-2c89-4ed1-8d82-f5322997f4b3"
            }, {
                "_id": "73cc5910-9855-44f4-844b-6414403482c2",
                "name": "Tien Lu District",
                "province": "48f8b2bb-2c89-4ed1-8d82-f5322997f4b3"
            }, {
                "_id": "ced1acb1-b0c8-48e5-9dbf-5cd888204f14",
                "name": "Phu Cu District",
                "province": "48f8b2bb-2c89-4ed1-8d82-f5322997f4b3"
            }, {
                "_id": "f9d90081-91fc-4f7d-9e81-11fcd53994d7",
                "name": "My Hao District",
                "province": "48f8b2bb-2c89-4ed1-8d82-f5322997f4b3"
            }, {
                "_id": "6c6f181e-08ca-4864-ba77-b04cac0e0bcb",
                "name": "Van Lam District",
                "province": "48f8b2bb-2c89-4ed1-8d82-f5322997f4b3"
            }, {
                "_id": "548f7b7b-1551-4a73-bf97-da9923a1d71c",
                "name": "Van Giang District",
                "province": "48f8b2bb-2c89-4ed1-8d82-f5322997f4b3"
            }, {
                "_id": "d110b92e-d48c-447f-91f7-6eb6b38922dc",
                "name": "Ninh Binh City",
                "province": "953efb88-75c3-4c4a-8661-54aa4499a6c3"
            }, {
                "_id": "0b46a801-69be-4831-b56c-a8240993c83f",
                "name": "Tam Diep Town",
                "province": "953efb88-75c3-4c4a-8661-54aa4499a6c3"
            }, {
                "_id": "bbe5e2ca-97b6-4196-b34b-cbf5aef96fff",
                "name": "Nho Quan District",
                "province": "953efb88-75c3-4c4a-8661-54aa4499a6c3"
            }, {
                "_id": "37736620-1a48-4c30-937d-956fe79ea95b",
                "name": "Gia Vien District",
                "province": "953efb88-75c3-4c4a-8661-54aa4499a6c3"
            }, {
                "_id": "a151617b-fb28-4d55-84bf-d0f3fbe70e09",
                "name": "Hoa Lu District",
                "province": "953efb88-75c3-4c4a-8661-54aa4499a6c3"
            }, {
                "_id": "e7947488-9084-4191-9e8a-f57f50481f14",
                "name": "Yen Mo District",
                "province": "953efb88-75c3-4c4a-8661-54aa4499a6c3"
            }, {
                "_id": "1091fa8c-aa7d-4456-8541-47930d233de9",
                "name": "Kim Son District",
                "province": "953efb88-75c3-4c4a-8661-54aa4499a6c3"
            }, {
                "_id": "ba20c547-2bd8-4f75-9f7c-53d220fec733",
                "name": "Yen Khanh District",
                "province": "953efb88-75c3-4c4a-8661-54aa4499a6c3"
            }, {
                "_id": "e5a18f84-6690-4e15-8744-3033c73531cf",
                "name": "Thanh Hoa City",
                "province": "e397d0ae-b4b0-4e78-a445-cab555d40d34"
            }, {
                "_id": "5472d80b-3de4-46cb-99f5-123c28a81cde",
                "name": "Bim Son Town",
                "province": "e397d0ae-b4b0-4e78-a445-cab555d40d34"
            }, {
                "_id": "2274fb0b-44e6-4246-919c-9e713659d2f5",
                "name": "Sam Son Town",
                "province": "e397d0ae-b4b0-4e78-a445-cab555d40d34"
            }, {
                "_id": "9a210a6e-d2bb-4236-8958-c82b1ba94c2f",
                "name": "Quan Hoa District",
                "province": "e397d0ae-b4b0-4e78-a445-cab555d40d34"
            }, {
                "_id": "c9f08a25-2030-4c59-85d9-54ab44b9e076",
                "name": "Quan Son District",
                "province": "e397d0ae-b4b0-4e78-a445-cab555d40d34"
            }, {
                "_id": "56b989fc-491b-4a7f-b671-61ec05536ada",
                "name": "Muong Lat District",
                "province": "e397d0ae-b4b0-4e78-a445-cab555d40d34"
            }, {
                "_id": "5b623ed6-fd60-4f25-9347-2f291501ce29",
                "name": "Ba Thuoc District",
                "province": "e397d0ae-b4b0-4e78-a445-cab555d40d34"
            }, {
                "_id": "68e22fbe-b63b-4380-a3e8-c2d37003b868",
                "name": "Thuong Xuan District",
                "province": "e397d0ae-b4b0-4e78-a445-cab555d40d34"
            }, {
                "_id": "10329aa0-e5a0-4000-8c49-1d90f101229d",
                "name": "Nhu Xuan District",
                "province": "e397d0ae-b4b0-4e78-a445-cab555d40d34"
            }, {
                "_id": "34392430-901f-48b8-b82b-616d1ed03787",
                "name": "Nhu Thanh District",
                "province": "e397d0ae-b4b0-4e78-a445-cab555d40d34"
            }, {
                "_id": "01a8c0d4-2bfd-40de-9ec9-51a44c481068",
                "name": "Lang Chanh District",
                "province": "e397d0ae-b4b0-4e78-a445-cab555d40d34"
            }, {
                "_id": "3f9b4595-0011-4410-bf4b-c700286996ed",
                "name": "Ngoc Lac District",
                "province": "e397d0ae-b4b0-4e78-a445-cab555d40d34"
            }, {
                "_id": "c0a327f1-b5a3-4a9d-846f-ac4e752cf045",
                "name": "Thach Thanh District",
                "province": "e397d0ae-b4b0-4e78-a445-cab555d40d34"
            }, {
                "_id": "e7c1589c-121d-4efe-ab39-0c2b45a95ec6",
                "name": "Cam Thuy District",
                "province": "e397d0ae-b4b0-4e78-a445-cab555d40d34"
            }, {
                "_id": "be982126-b077-4742-b1d5-5fdbc4dad585",
                "name": "Tho Xuan District",
                "province": "e397d0ae-b4b0-4e78-a445-cab555d40d34"
            }, {
                "_id": "5455cfc0-b994-4c58-bbf1-9bf13c59d96f",
                "name": "Vinh Loc District",
                "province": "e397d0ae-b4b0-4e78-a445-cab555d40d34"
            }, {
                "_id": "9b0cd6a9-1bbe-4709-b17a-3e2c0e31a0e5",
                "name": "Thieu Hoa District",
                "province": "e397d0ae-b4b0-4e78-a445-cab555d40d34"
            }, {
                "_id": "9568ed1d-c659-49cf-b999-709165d6eae3",
                "name": "Trieu Son District",
                "province": "e397d0ae-b4b0-4e78-a445-cab555d40d34"
            }, {
                "_id": "771f5c9b-4653-4a45-bf3d-46bec4f498ba",
                "name": "Nong Cong District",
                "province": "e397d0ae-b4b0-4e78-a445-cab555d40d34"
            }, {
                "_id": "a69ff478-be1c-4746-8f8d-92cd1aeef0f5",
                "name": "Dong Son District",
                "province": "e397d0ae-b4b0-4e78-a445-cab555d40d34"
            }, {
                "_id": "73e0dfab-b924-4137-8045-c7622e92c4a3",
                "name": "Ha Trung District",
                "province": "e397d0ae-b4b0-4e78-a445-cab555d40d34"
            }, {
                "_id": "03b8a096-2e07-4cfb-91a2-964521873263",
                "name": "Hoang Hoa District",
                "province": "e397d0ae-b4b0-4e78-a445-cab555d40d34"
            }, {
                "_id": "c3b3b04c-9032-4020-b85b-23381b7292c6",
                "name": "Nga Son District",
                "province": "e397d0ae-b4b0-4e78-a445-cab555d40d34"
            }, {
                "_id": "0c775043-f518-42fc-8986-deccb49c454d",
                "name": "Hau Loc District",
                "province": "e397d0ae-b4b0-4e78-a445-cab555d40d34"
            }, {
                "_id": "8e273949-8753-4e98-8b81-fa4b87cff0b1",
                "name": "Quang Xuong District",
                "province": "e397d0ae-b4b0-4e78-a445-cab555d40d34"
            }, {
                "_id": "c54d560a-9322-49d7-a9a5-c5ea3fab85b0",
                "name": "Tinh Gia District",
                "province": "e397d0ae-b4b0-4e78-a445-cab555d40d34"
            }, {
                "_id": "978bff20-9616-4e75-a755-01904b3ef047",
                "name": "Yen Dinh District",
                "province": "e397d0ae-b4b0-4e78-a445-cab555d40d34"
            }, {
                "_id": "4dcac1cd-06a5-47ff-aa48-b57912b89c1a",
                "name": "Hue City",
                "province": "6faa3fb6-1ee6-4b40-8929-42adbe69360b"
            }, {
                "_id": "2ef0a1a6-050b-422c-afe1-31c880b2319d",
                "name": "Phong Dien District",
                "province": "6faa3fb6-1ee6-4b40-8929-42adbe69360b"
            }, {
                "_id": "f1cf78e9-c779-4dab-9b14-88ee62569cda",
                "name": "Quang Dien District",
                "province": "6faa3fb6-1ee6-4b40-8929-42adbe69360b"
            }, {
                "_id": "262b61a6-8ce1-416e-98c8-fb85fcba2c5b",
                "name": "Huong Tra Town",
                "province": "6faa3fb6-1ee6-4b40-8929-42adbe69360b"
            }, {
                "_id": "9e1e390f-668a-464f-8d73-ad218be564ef",
                "name": "Phu Vang District",
                "province": "6faa3fb6-1ee6-4b40-8929-42adbe69360b"
            }, {
                "_id": "dc0d9c3e-be5a-4e94-b96e-73ae31da4c90",
                "name": "Huong Thuy Town",
                "province": "6faa3fb6-1ee6-4b40-8929-42adbe69360b"
            }, {
                "_id": "eead777f-5bcc-4108-8b04-9c17e68e644f",
                "name": "Phu Loc District",
                "province": "6faa3fb6-1ee6-4b40-8929-42adbe69360b"
            }, {
                "_id": "e9a333b7-ed72-4e0b-a276-b50dfefc9970",
                "name": "Nam Dong District",
                "province": "6faa3fb6-1ee6-4b40-8929-42adbe69360b"
            }, {
                "_id": "fed89aa8-89f1-4eae-96d0-d3a7c85d33da",
                "name": "A Luoi District",
                "province": "6faa3fb6-1ee6-4b40-8929-42adbe69360b"
            }, {
                "_id": "6f1d8729-1513-4569-a1ee-6a9b5dbe94cd",
                "name": "Tam Ky City",
                "province": "901ac059-7b08-40ee-9a79-b58aa3b11bf0"
            }, {
                "_id": "53c8395a-a130-4902-928e-d24fb5234a0a",
                "name": "Hoi An City",
                "province": "901ac059-7b08-40ee-9a79-b58aa3b11bf0"
            }, {
                "_id": "22e22270-c8b7-495a-81ae-7c6958a52a7c",
                "name": "Duy Xuyen District",
                "province": "901ac059-7b08-40ee-9a79-b58aa3b11bf0"
            }, {
                "_id": "49f2329d-02a8-41af-9bcd-5357939cdb93",
                "name": "Dien Ban District",
                "province": "901ac059-7b08-40ee-9a79-b58aa3b11bf0"
            }, {
                "_id": "c26ba17f-21e8-489d-883d-f788452e4c6d",
                "name": "Dai Loc District",
                "province": "901ac059-7b08-40ee-9a79-b58aa3b11bf0"
            }, {
                "_id": "f3e2a4f3-81fa-4a11-a60d-f30c566fcf89",
                "name": "Que Son District",
                "province": "901ac059-7b08-40ee-9a79-b58aa3b11bf0"
            }, {
                "_id": "624f54d1-4759-4ec3-8ce5-b1219290f876",
                "name": "Hiep Duc District",
                "province": "901ac059-7b08-40ee-9a79-b58aa3b11bf0"
            }, {
                "_id": "5a43bfa8-ca28-498a-a7c4-1026f9d9a975",
                "name": "Thang Binh District",
                "province": "901ac059-7b08-40ee-9a79-b58aa3b11bf0"
            }, {
                "_id": "72ced963-5a76-4658-897f-2421cbde24fb",
                "name": "Nui Thanh District",
                "province": "901ac059-7b08-40ee-9a79-b58aa3b11bf0"
            }, {
                "_id": "053f13ed-d812-448f-bc1a-add619df77c7",
                "name": "Tien Phuoc District",
                "province": "901ac059-7b08-40ee-9a79-b58aa3b11bf0"
            }, {
                "_id": "93b1027c-9c68-4884-9ab2-fa77fb2e53dd",
                "name": "Bac Tra My District",
                "province": "901ac059-7b08-40ee-9a79-b58aa3b11bf0"
            }, {
                "_id": "dee57c08-ceff-4d3a-bd40-7e564322b655",
                "name": "Dong Giang District",
                "province": "901ac059-7b08-40ee-9a79-b58aa3b11bf0"
            }, {
                "_id": "72ca7087-70bf-430f-ac83-aa27e37f4cb7",
                "name": "Nam Giang District",
                "province": "901ac059-7b08-40ee-9a79-b58aa3b11bf0"
            }, {
                "_id": "b9eacc0a-1583-4513-b007-dcde5faff83c",
                "name": "Phuoc Son District",
                "province": "901ac059-7b08-40ee-9a79-b58aa3b11bf0"
            }, {
                "_id": "4322625f-a899-45fc-907e-c5d17e36e6bb",
                "name": "Nam Tra My District",
                "province": "901ac059-7b08-40ee-9a79-b58aa3b11bf0"
            }, {
                "_id": "7b618da3-06e3-4ec7-999c-9dce3337767c",
                "name": "Tay Giang District",
                "province": "901ac059-7b08-40ee-9a79-b58aa3b11bf0"
            }, {
                "_id": "d8fe45dc-5c42-49b5-ac22-746a0cabdd31",
                "name": "Phu Ninh District",
                "province": "901ac059-7b08-40ee-9a79-b58aa3b11bf0"
            }, {
                "_id": "3ce3c36d-16cd-4e8f-b069-374a9e4a04dc",
                "name": "Nong Son District",
                "province": "901ac059-7b08-40ee-9a79-b58aa3b11bf0"
            }, {
                "_id": "ea90fdc5-2747-4fdb-9c82-b761c931ff3f",
                "name": "Nha Trang City",
                "province": "48e5b9bc-c9d2-4ada-bdbd-039a24926529"
            }, {
                "_id": "e912dbac-dc30-4225-a850-027b4c9af7a2",
                "name": "Van Ninh District",
                "province": "48e5b9bc-c9d2-4ada-bdbd-039a24926529"
            }, {
                "_id": "a66430c4-dcf4-4b59-a0bb-bf402bd6e2da",
                "name": "Ninh Hoa District",
                "province": "48e5b9bc-c9d2-4ada-bdbd-039a24926529"
            }, {
                "_id": "fa428c70-6c3d-4f79-ad91-35398b6a60c2",
                "name": "Dien Khanh District",
                "province": "48e5b9bc-c9d2-4ada-bdbd-039a24926529"
            }, {
                "_id": "74a12c4c-a5ae-4626-88fa-a4586611bf14",
                "name": "Khanh Vinh District",
                "province": "48e5b9bc-c9d2-4ada-bdbd-039a24926529"
            }, {
                "_id": "251c33a9-07ff-429a-83e0-a74a907e9c33",
                "name": "Cam Ranh Town",
                "province": "48e5b9bc-c9d2-4ada-bdbd-039a24926529"
            }, {
                "_id": "49979bfd-270e-4356-8b87-04cff08f4375",
                "name": "Khanh Son District",
                "province": "48e5b9bc-c9d2-4ada-bdbd-039a24926529"
            }, {
                "_id": "fa7e1eeb-708c-4be4-aa3d-78366423cd39",
                "name": "dao Truong Sa District",
                "province": "48e5b9bc-c9d2-4ada-bdbd-039a24926529"
            }, {
                "_id": "e6baa794-3446-4f73-b294-bd50d7383dde",
                "name": "Cam Lam District",
                "province": "48e5b9bc-c9d2-4ada-bdbd-039a24926529"
            }, {
                "_id": "5c29f043-9cfc-4c64-bea4-a7e0cd96fddc",
                "name": "Da Lat City",
                "province": "a4163df2-fc9a-4f7e-befc-dcea47984161"
            }, {
                "_id": "a77dd122-b518-4dde-8717-f688a9370890",
                "name": "Bao Loc City",
                "province": "a4163df2-fc9a-4f7e-befc-dcea47984161"
            }, {
                "_id": "34eef087-09f8-46a6-8415-d093edb02c65",
                "name": "Duc Trong District",
                "province": "a4163df2-fc9a-4f7e-befc-dcea47984161"
            }, {
                "_id": "fcdd68cc-8554-46e7-ab41-020c930f3394",
                "name": "Di Linh District",
                "province": "a4163df2-fc9a-4f7e-befc-dcea47984161"
            }, {
                "_id": "c59e60bd-46b9-4d3b-9a44-3d5980a67f00",
                "name": "Don Duong District",
                "province": "a4163df2-fc9a-4f7e-befc-dcea47984161"
            }, {
                "_id": "acf977a2-199f-4810-85e6-bb0177f8d9dd",
                "name": "Lac Duong District",
                "province": "a4163df2-fc9a-4f7e-befc-dcea47984161"
            }, {
                "_id": "6524fb8f-2f31-439e-8f58-60cf95bfa161",
                "name": "Da Huoai District",
                "province": "a4163df2-fc9a-4f7e-befc-dcea47984161"
            }, {
                "_id": "179f6b5c-135d-4593-8f26-07a5b5f51e86",
                "name": "Da Teh District",
                "province": "a4163df2-fc9a-4f7e-befc-dcea47984161"
            }, {
                "_id": "2d294407-2352-439b-b0de-8a4b3e89848d",
                "name": "Cat Tien District",
                "province": "a4163df2-fc9a-4f7e-befc-dcea47984161"
            }, {
                "_id": "43387675-efe6-481b-990f-acc1cc389f80",
                "name": "Lam Ha District",
                "province": "a4163df2-fc9a-4f7e-befc-dcea47984161"
            }, {
                "_id": "3582d47c-ceec-4a40-ba31-444d65b100b5",
                "name": "Bao Lam District",
                "province": "a4163df2-fc9a-4f7e-befc-dcea47984161"
            }, {
                "_id": "503df463-ba8f-42b4-98e5-c3f11aefa890",
                "name": "Dam Rong District",
                "province": "a4163df2-fc9a-4f7e-befc-dcea47984161"
            }, {
                "_id": "4b0a036b-2555-4e75-99b5-6b5d41229f6e",
                "name": "Dong Xoai Town",
                "province": "692aac88-8eb6-42ee-af5d-c15143820e07"
            }, {
                "_id": "27102f0b-7230-4347-82df-a34aeecde8ec",
                "name": "Dong Phu District",
                "province": "692aac88-8eb6-42ee-af5d-c15143820e07"
            }, {
                "_id": "6e692eaa-b083-44de-953d-7c9d20eb6627",
                "name": "Chon Thanh District",
                "province": "692aac88-8eb6-42ee-af5d-c15143820e07"
            }, {
                "_id": "b0bd3e6d-09f0-4133-baaf-5ecdba52eb32",
                "name": "Binh Long District",
                "province": "692aac88-8eb6-42ee-af5d-c15143820e07"
            }, {
                "_id": "f4d1877b-bade-4512-8f8b-bf3253f9b903",
                "name": "Loc Ninh District",
                "province": "692aac88-8eb6-42ee-af5d-c15143820e07"
            }, {
                "_id": "0edb5362-b51a-40b7-8022-aa501938793a",
                "name": "Bu Dop District",
                "province": "692aac88-8eb6-42ee-af5d-c15143820e07"
            }, {
                "_id": "16f9261a-2679-4ae6-a633-88f206b6153e",
                "name": "Phuoc Long District",
                "province": "692aac88-8eb6-42ee-af5d-c15143820e07"
            }, {
                "_id": "873fd376-cbf0-403c-a004-a731fa2020ee",
                "name": "Bu Dang District",
                "province": "692aac88-8eb6-42ee-af5d-c15143820e07"
            }, {
                "_id": "b5c80f82-6280-42ee-b164-5a5c902f393c",
                "name": "Hon Quan District",
                "province": "692aac88-8eb6-42ee-af5d-c15143820e07"
            }, {
                "_id": "d438c3ac-09b6-4d64-a59f-5cadf8d58ef4",
                "name": "Bu Gia Map District",
                "province": "692aac88-8eb6-42ee-af5d-c15143820e07"
            }, {
                "_id": "2b1b2f50-2f0e-4e76-a9bb-c9ac857c424a",
                "name": "Thu Dau Mot District",
                "province": "d32da938-7416-4293-acf8-eb8aede4599e"
            }, {
                "_id": "e3343d23-adee-42e2-b441-e0c93c3219ff",
                "name": "Ben Cat District",
                "province": "d32da938-7416-4293-acf8-eb8aede4599e"
            }, {
                "_id": "4daf12fc-fabb-4b3e-976c-2f3a1f2b97d7",
                "name": "Tan Uyen District",
                "province": "d32da938-7416-4293-acf8-eb8aede4599e"
            }, {
                "_id": "051c246b-8e54-4dcb-8830-74962c249658",
                "name": "Thuan An Town",
                "province": "d32da938-7416-4293-acf8-eb8aede4599e"
            }, {
                "_id": "e9029d39-82f1-46c4-9f4d-f4bd618fcf22",
                "name": "Di An Town",
                "province": "d32da938-7416-4293-acf8-eb8aede4599e"
            }, {
                "_id": "114b298b-d3f8-456f-82a4-d04687975ac4",
                "name": "Phu Giao District",
                "province": "d32da938-7416-4293-acf8-eb8aede4599e"
            }, {
                "_id": "cf6f8f11-6274-407a-82c7-2fc9374e60e3",
                "name": "Dau Tieng District",
                "province": "d32da938-7416-4293-acf8-eb8aede4599e"
            }, {
                "_id": "003fc50c-02c0-4c03-9395-e512ac1ff04b",
                "name": "Tay Ninh Town",
                "province": "df94761a-f40f-4fb7-9f7c-fc485ce0a170"
            }, {
                "_id": "d9b6561c-2c06-438c-a63b-fd8138ce7d86",
                "name": "Tan Bien District",
                "province": "df94761a-f40f-4fb7-9f7c-fc485ce0a170"
            }, {
                "_id": "fa2cde6d-693e-4a49-a1ef-3018ac95b713",
                "name": "Tan Chau District",
                "province": "df94761a-f40f-4fb7-9f7c-fc485ce0a170"
            }, {
                "_id": "8f1f2957-df73-4456-baec-b54e9a55230f",
                "name": "Duong Minh Chau District",
                "province": "df94761a-f40f-4fb7-9f7c-fc485ce0a170"
            }, {
                "_id": "4985e443-8db9-4684-8d55-b715b7cd4b99",
                "name": "Chau Thanh District",
                "province": "df94761a-f40f-4fb7-9f7c-fc485ce0a170"
            }, {
                "_id": "858bee1a-c052-4815-99e7-f846108e5490",
                "name": "Hoa Thanh District",
                "province": "df94761a-f40f-4fb7-9f7c-fc485ce0a170"
            }, {
                "_id": "2e95204a-1310-443c-bd34-54b927c2d984",
                "name": "Ben Cau District",
                "province": "df94761a-f40f-4fb7-9f7c-fc485ce0a170"
            }, {
                "_id": "db0e49b9-8b9f-4264-8750-39186067ff84",
                "name": "Go Dau District",
                "province": "df94761a-f40f-4fb7-9f7c-fc485ce0a170"
            }, {
                "_id": "03d03c36-0266-4a7e-8488-c95ef1e61655",
                "name": "Trang Bang District",
                "province": "df94761a-f40f-4fb7-9f7c-fc485ce0a170"
            }, {
                "_id": "8a701bac-832e-4813-8bc1-46e0e61b4d84",
                "name": "Phan Thiet City",
                "province": "76122c40-5613-41c5-bcf6-2b4e4b489f13"
            }, {
                "_id": "747e7a8e-e093-426f-9bcd-ce7714acaafb",
                "name": "Tuy Phong District",
                "province": "76122c40-5613-41c5-bcf6-2b4e4b489f13"
            }, {
                "_id": "f31c9d9a-8ff5-4607-a186-3d071e76ed52",
                "name": "Bac Binh District",
                "province": "76122c40-5613-41c5-bcf6-2b4e4b489f13"
            }, {
                "_id": "58fc745f-21ac-40cf-8499-640c8f7a55ef",
                "name": "Ham Thuan Bac District",
                "province": "76122c40-5613-41c5-bcf6-2b4e4b489f13"
            }, {
                "_id": "1c06348c-b58a-402f-a014-02eabae0a5ec",
                "name": "Ham Thuan Nam District",
                "province": "76122c40-5613-41c5-bcf6-2b4e4b489f13"
            }, {
                "_id": "e194b823-1fbe-4a8e-bc78-90c3332af75c",
                "name": "Ham Tan District",
                "province": "76122c40-5613-41c5-bcf6-2b4e4b489f13"
            }, {
                "_id": "30d43116-f436-475a-bd52-08b218f53cfb",
                "name": "Duc Linh District",
                "province": "76122c40-5613-41c5-bcf6-2b4e4b489f13"
            }, {
                "_id": "a1dfd585-7411-4794-b43c-a98f5f286ad1",
                "name": "Tanh Linh District",
                "province": "76122c40-5613-41c5-bcf6-2b4e4b489f13"
            }, {
                "_id": "f526f2bd-4216-4402-beed-76c751422164",
                "name": "dao Phu Quy District",
                "province": "76122c40-5613-41c5-bcf6-2b4e4b489f13"
            }, {
                "_id": "0991fe47-463b-47e0-a9ad-c366507da353",
                "name": "La Gi Town",
                "province": "76122c40-5613-41c5-bcf6-2b4e4b489f13"
            }, {
                "_id": "c55f7a47-0f68-4086-adb5-75910d41a90b",
                "name": "Bien Hoa City",
                "province": "279e120e-e7f8-4520-9152-4e212f5d06fa"
            }, {
                "_id": "6034373a-7e11-43c3-a9f2-cc6dd9c1c56d",
                "name": "Vinh Cuu District",
                "province": "279e120e-e7f8-4520-9152-4e212f5d06fa"
            }, {
                "_id": "8509797f-2828-40ba-a902-9609e6cebf76",
                "name": "Tan Phu District",
                "province": "279e120e-e7f8-4520-9152-4e212f5d06fa"
            }, {
                "_id": "ff83bc13-da94-4c11-a55f-ea47cb0f06aa",
                "name": "Dinh Quan District",
                "province": "279e120e-e7f8-4520-9152-4e212f5d06fa"
            }, {
                "_id": "0727f617-da42-4b37-bfa0-75ec5c69c73b",
                "name": "Thong Nhat District",
                "province": "279e120e-e7f8-4520-9152-4e212f5d06fa"
            }, {
                "_id": "9f9382ea-be12-4ce4-a76d-c888814efee0",
                "name": "Long Khanh Town",
                "province": "279e120e-e7f8-4520-9152-4e212f5d06fa"
            }, {
                "_id": "802340fe-639b-470b-bf84-acc5484ac141",
                "name": "Xuan Loc District",
                "province": "279e120e-e7f8-4520-9152-4e212f5d06fa"
            }, {
                "_id": "5f24317d-1584-4797-aa5c-444c35329467",
                "name": "Long Thanh District",
                "province": "279e120e-e7f8-4520-9152-4e212f5d06fa"
            }, {
                "_id": "4ab87e81-14fa-412f-9e62-f311738d751d",
                "name": "Nhon Trach District",
                "province": "279e120e-e7f8-4520-9152-4e212f5d06fa"
            }, {
                "_id": "06627e9d-4476-4a96-beab-6db9d90674be",
                "name": "Trang Bom District",
                "province": "279e120e-e7f8-4520-9152-4e212f5d06fa"
            }, {
                "_id": "e86c1728-724f-4dfe-9b24-30d953908b54",
                "name": "Cam My District",
                "province": "279e120e-e7f8-4520-9152-4e212f5d06fa"
            }, {
                "_id": "798e6386-11d8-4472-bc52-4941ce99d273",
                "name": "Tan An City",
                "province": "898cb79f-7548-4b2c-a8d4-79abda0858a0"
            }, {
                "_id": "e7fe1dfe-5d15-499f-901f-c16f8c7b6011",
                "name": "Vinh Hung District",
                "province": "898cb79f-7548-4b2c-a8d4-79abda0858a0"
            }, {
                "_id": "d598d51a-e06b-4069-a912-9ba6b9e813ae",
                "name": "Moc Hoa District",
                "province": "898cb79f-7548-4b2c-a8d4-79abda0858a0"
            }, {
                "_id": "08d63593-5e7a-4081-8b4f-acfdcb351ed7",
                "name": "Tan Thanh District",
                "province": "898cb79f-7548-4b2c-a8d4-79abda0858a0"
            }, {
                "_id": "c8089ef2-3961-402a-b520-4becdb688850",
                "name": "Thanh Hoa District",
                "province": "898cb79f-7548-4b2c-a8d4-79abda0858a0"
            }, {
                "_id": "25c3f921-7a0b-4c90-8fa2-6fc2ad89453a",
                "name": "Duc Hue District",
                "province": "898cb79f-7548-4b2c-a8d4-79abda0858a0"
            }, {
                "_id": "76cc0931-e9ba-4095-a7c8-2dcf94b35306",
                "name": "Duc Hoa District",
                "province": "898cb79f-7548-4b2c-a8d4-79abda0858a0"
            }, {
                "_id": "519700b7-1487-4e3e-a01a-d36400da2c98",
                "name": "Ben Luc District",
                "province": "898cb79f-7548-4b2c-a8d4-79abda0858a0"
            }, {
                "_id": "83379628-4e0f-4baf-b0f4-e912af652640",
                "name": "Thu Thua District",
                "province": "898cb79f-7548-4b2c-a8d4-79abda0858a0"
            }, {
                "_id": "52adeb41-2638-4aaa-a246-78a95dfa1857",
                "name": "Chau Thanh District",
                "province": "898cb79f-7548-4b2c-a8d4-79abda0858a0"
            }, {
                "_id": "f41386b7-b1c6-4e85-9049-f88f4012191a",
                "name": "Tan Tru District",
                "province": "898cb79f-7548-4b2c-a8d4-79abda0858a0"
            }, {
                "_id": "04722044-be21-4eee-b49a-78f02cd6a083",
                "name": "Can Duoc District",
                "province": "898cb79f-7548-4b2c-a8d4-79abda0858a0"
            }, {
                "_id": "1f3a8fd0-8423-4d02-b9f8-4cab889c2b3c",
                "name": "Can Giuoc District",
                "province": "898cb79f-7548-4b2c-a8d4-79abda0858a0"
            }, {
                "_id": "84fe4146-723f-44f0-85ef-08179c294f88",
                "name": "Tan Hung District",
                "province": "898cb79f-7548-4b2c-a8d4-79abda0858a0"
            }, {
                "_id": "59a8981b-7467-478a-b04f-dae6db4e5ecd",
                "name": "Kien Tuong Town",
                "province": "898cb79f-7548-4b2c-a8d4-79abda0858a0"
            }, {
                "_id": "df66944d-e736-47dd-8000-3939eccf4a19",
                "name": "Vung Tau City",
                "province": "4502d3e0-8137-4938-9f38-567c221f7635"
            }, {
                "_id": "c3527a19-c7a9-42dc-8004-b4e8918e9f3f",
                "name": "Ba Ria City",
                "province": "4502d3e0-8137-4938-9f38-567c221f7635"
            }, {
                "_id": "b909069c-b0ed-4363-8f11-3de4d079f213",
                "name": "Xuyen Moc District",
                "province": "4502d3e0-8137-4938-9f38-567c221f7635"
            }, {
                "_id": "5bf52699-3f81-434e-805e-7958575219ac",
                "name": "Long Dien District",
                "province": "4502d3e0-8137-4938-9f38-567c221f7635"
            }, {
                "_id": "134aceee-cb44-4223-a56e-c3a86f806206",
                "name": "Con Dao District",
                "province": "4502d3e0-8137-4938-9f38-567c221f7635"
            }, {
                "_id": "0e44fef7-989b-4ed8-b577-36904413553d",
                "name": "Tan Thanh District",
                "province": "4502d3e0-8137-4938-9f38-567c221f7635"
            }, {
                "_id": "8f67374f-fc52-4dc7-bd82-c085dc52ccd8",
                "name": "Chau Duc District",
                "province": "4502d3e0-8137-4938-9f38-567c221f7635"
            }, {
                "_id": "2471a4b5-db72-42e7-a14c-b0312b1ab988",
                "name": "Dat Do District",
                "province": "4502d3e0-8137-4938-9f38-567c221f7635"
            }, {
                "_id": "9cf55d46-192c-4efe-a694-fa20d9fbeade",
                "name": "Rach Gia City",
                "province": "3d1a5267-48c5-4b34-a48a-61d07f9ecc87"
            }, {
                "_id": "64c9d305-cbec-45cc-baee-88e097aa2b03",
                "name": "Ha Tien Town",
                "province": "3d1a5267-48c5-4b34-a48a-61d07f9ecc87"
            }, {
                "_id": "335c19c7-3d6b-4157-b1e1-227605c2c1bd",
                "name": "Kien Luong District",
                "province": "3d1a5267-48c5-4b34-a48a-61d07f9ecc87"
            }, {
                "_id": "207db9b3-fb4b-44eb-8184-36b1b82bc198",
                "name": "Hon Dat District",
                "province": "3d1a5267-48c5-4b34-a48a-61d07f9ecc87"
            }, {
                "_id": "5008c993-eee4-4fe0-804e-77686e203e54",
                "name": "Tan Hiep District",
                "province": "3d1a5267-48c5-4b34-a48a-61d07f9ecc87"
            }, {
                "_id": "f14f9751-b92c-431e-8510-82ba05607636",
                "name": "Chau Thanh District",
                "province": "3d1a5267-48c5-4b34-a48a-61d07f9ecc87"
            }, {
                "_id": "4383e01c-aeb0-40d5-8973-ea40bfb9616f",
                "name": "Giong Rieng District",
                "province": "3d1a5267-48c5-4b34-a48a-61d07f9ecc87"
            }, {
                "_id": "c1c7f122-8ff7-4b82-964d-82a45ef777ca",
                "name": "Go Quao District",
                "province": "3d1a5267-48c5-4b34-a48a-61d07f9ecc87"
            }, {
                "_id": "8c5a89c8-70ca-4aa6-a9d3-13988decd758",
                "name": "An Bien District",
                "province": "3d1a5267-48c5-4b34-a48a-61d07f9ecc87"
            }, {
                "_id": "d18a6603-5378-44f9-9ed3-2db579743997",
                "name": "An Minh District",
                "province": "3d1a5267-48c5-4b34-a48a-61d07f9ecc87"
            }, {
                "_id": "78f9ed46-a471-449b-9a4e-caec85e2a2a2",
                "name": "Vinh Thuan District",
                "province": "3d1a5267-48c5-4b34-a48a-61d07f9ecc87"
            }, {
                "_id": "0f7b7df5-77ba-4262-bcb3-7d6719aeb9fb",
                "name": "Phu Quoc District",
                "province": "3d1a5267-48c5-4b34-a48a-61d07f9ecc87"
            }, {
                "_id": "d810c26c-3936-43e7-84c7-8d613f85c1d5",
                "name": "Kien Hai District",
                "province": "3d1a5267-48c5-4b34-a48a-61d07f9ecc87"
            }, {
                "_id": "4acabdcf-6bda-46d1-add5-608635b9323f",
                "name": "U Minh Thuong District",
                "province": "3d1a5267-48c5-4b34-a48a-61d07f9ecc87"
            }, {
                "_id": "f9142877-0ba6-42a1-9396-50de1edc37f3",
                "name": "Giang Thanh District",
                "province": "3d1a5267-48c5-4b34-a48a-61d07f9ecc87"
            }];
            District.import(data, (err, docs, affected) => {
                if(err) console.log(err)
                else console.log(`Generated ${affected} records`)
                process.exit()
            })
        }
    })
})()