( function() {
	'use strict'

    const mongoose = require('mongoose')
    const Schema = mongoose.Schema;
    const dbConnect = require('../../db/connect')
    const { forEachAsync } = require(`${process.cwd()}/lib/helper`)
    const config = require('config')

	dbConnect(async (err, db) => {
        if (err) {
            console.log('Action fail')
        }
        else {
            try {
                const ListingGroup = require('../../models/listingGroup')
                const data = [
                    {
                        _id: 1,
                        code: 'Cần mua',
                        title: {
                            vi: 'Cần mua'
                        },
                        languages: ['vi']
                    },
                    {
                        _id: 2,
                        code: 'Cần thuê',
                        title: {
                            vi: 'Cần thuê'
                        },
                        languages: ['vi']
                    },
                    {
                        _id: 3,
                        code: 'Cần bán',
                        title: {
                            vi: 'Cần bán'
                        },
                        languages: ['vi']
                    },
                    {
                        _id: 4,
                        code: 'Cần cho thuê',
                        title: {
                            vi: 'Cần cho thuê'
                        },
                        languages: ['vi']
                    }
                ]
                await forEachAsync(data, async (a) => {
                    await ListingGroup.create(a, {
                        language: config.get('defaultLanguage')
                    }, (err, doc, affected) => {
                        if(err) {
                            if(typeof err !== 'Error' && typeof err !== 'MongoError') {
                                console.log(err.message || 'Error')
                                return;
                            }
                        }
                        console.log(`Generated ${affected} records`)
                    })
                })
                process.exit()
            } catch (error) {
                console.log(error)
            }
        }
    })
})()