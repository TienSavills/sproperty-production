( function() {
	'use strict'

    const mongoose = require('mongoose')
    const Schema = mongoose.Schema;
    const dbConnect = require('../../db/connect')
    const { forEachAsync } = require(`${process.cwd()}/lib/helper`)
    const config = require('config')

	dbConnect(async (err, db) => {
        if (err) {
            console.log('Action fail')
        }
        else {
            try {
                const ApprovalStatus = require('../../models/approvalStatus')
                const data = [
                    {
                        _id: 0,
                        code: 'Pending',
                        title: {
                            vi: 'Chờ duyệt',
                            en: 'Pending'
                        }
                    },
                    {
                        _id: 1,
                        code: 'Approved',
                        title: {
                            vi: 'Đã duyệt',
                            en: 'Approved'
                        }
                    },
                    {
                        _id: 2,
                        code: 'Not approved',
                        title: {
                            vi: 'Không duyệt',
                            en: 'Not approved'
                        }
                    }
                ]
                await forEachAsync(data, async (a) => {
                    await ApprovalStatus.create(a, {
                        language: config.get('defaultLanguage')
                    }, (err, doc, affected) => {
                        if(err) {
                            if(typeof err !== 'Error' && typeof err !== 'MongoError') {
                                console.log(err.message || 'Error')
                                return;
                            }
                        }
                        console.log(`Generated ${affected} records`)
                    })
                })
                process.exit()
            } catch (error) {
                console.log(error)
            }
        }
    })
})()