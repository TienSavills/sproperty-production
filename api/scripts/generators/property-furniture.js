( function() {
	'use strict'

    const mongoose = require('mongoose')
    const Schema = mongoose.Schema;
    const dbConnect = require('../../db/connect')
    const { forEachAsync } = require(`${process.cwd()}/lib/helper`)
    const config = require('config')

	dbConnect(async (err, db) => {
        if (err) {
            console.log('Action fail')
        }
        else {
            try {
                const PropertyFurniture = require('../../models/propertyFurniture')
                const data = [
                    {
                        _id: 1,
                        title: 'Đầy đủ',
                        languages: ['vi']
                    },
                    {
                        _id: 2,
                        title: 'Chưa có',
                        languages: ['vi']
                    },
                ]
                await forEachAsync(data, async (a) => {
                    await PropertyFurniture.create(a, {
                        language: config.get('defaultLanguage')
                    }, (err, doc, affected) => {
                        if(err) {
                            if(typeof err !== 'Error' && typeof err !== 'MongoError') {
                                console.log(err.message || 'Error')
                                return;
                            }
                        }
                        console.log(`Generated ${affected} records`)
                    })
                })
                process.exit()
            } catch (error) {
                console.log(error)
            }
        }
    })
})()