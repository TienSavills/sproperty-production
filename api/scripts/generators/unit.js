( async function() {
	'use strict'

    const dbConnect = require('../../db/connect')
    const { forEachAsync } = require(`${process.cwd()}/lib/helper`)
    const config = require('config')

	dbConnect(async (err, db) => {
        if (err) {
            console.log('Action fail')
        }
        else {
            try {
                const Unit = require('../../models/unit')
                const data = [{
                    "_id": "property",
                    "code": "Property",
                    "title": {
                        "vi": "Căn",
                        "en": "Property"
                    }
                }, {
                    "_id": "monthly",
                    "code": "Monthly",
                    "title": {
                        "vi": "Tháng",
                        "en": "Monthly"
                    }
                }, {
                    "_id": "year",
                    "code": "Year",
                    "title": {
                        "vi": "Năm",
                        "en": "Year"
                    }
                }, {
                    "_id": "sqm",
                    "code": "Square meters",
                    "title": {
                        "vi": "Mét vuông",
                        "en": "Square meters"
                    }
                }];
                await forEachAsync(data, async (a) => {
                    await Unit.create(a, {
                        language: config.get('defaultLanguage')
                    }, (err, doc, affected) => {
                        if(err) {
                            if(typeof err !== 'Error' && typeof err !== 'MongoError') {
                                console.log(err.message || 'Error')
                                return;
                            }
                        }
                        console.log(`Generated ${affected} records`)
                    })
                })
                process.exit()
            } catch (error) {
                console.log(error)
            }
        }
    })
})()