( function() {
	'use strict'

	const dbConnect = require('../../db/connect')
    const { forEachAsync } = require(`${process.cwd()}/lib/helper`)
    const config = require('config')

	dbConnect(async (err, db) => {
        if (err) {
            console.log('Action fail')
        }
        else {
            try {
                const Language = require('../../models/language')
                const data = [
                    {
                        _id: 'vi',
                        name: 'Tiếng Việt',
                    },
                    {
                        _id: 'en',
                        name: 'English',
                    },
                    {
                        _id: 'zh',
                        name: '中文',
                    },
                ]
                await forEachAsync(data, async (a) => {
                    await Language.create(a, (err, doc, affected) => {
                        if(err) {
                            if(typeof err !== 'Error' && typeof err !== 'MongoError') {
                                console.log(err.message || 'Error')
                                return;
                            }
                        }
                        console.log(`Generated ${affected} records`)
                    })
                })
                process.exit()
            } catch (error) {
                console.log(error)
            }
        }
    })
})()