const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const mongoosePaginate = require('mongoose-paginate');
const config = require('config');
// const defaultLanguage = config.get('defaultLanguage');
const { getDataFunc, generateIdSync, getByLanguage, setByLanguage } = require('../lib/helper');

const schema = new Schema({
    id: { type: String, default: generateIdSync(6) },
    // language: { type: String, default: 'vi' },
    title: { type: String, default: '' },
    keywords: [String],
    briefContent: { type: String, default: '' }, // pure text
    content: { type: String, default: '' }, // html
    active: { type: Boolean, default: true },
},
{
    timestamps: true
});

schema.plugin(mongoosePaginate);

const PUBLIC_FIELDS = 'id title keywords briefContent content createdAt';
const POPULATED_FIELDS = '';

schema.statics.create = function(detail, cb) {
    return new Promise(async (resolve, reject) => {
        let d = new Content(detail);
        d.save(function(err, doc, affected) {
            if (err) {
                if(cb) resolve(cb(err));
                else reject(err);
            } else {
                if(cb) resolve(cb(null, doc, affected));
                else resolve(affected);
            }
        });
    });
};
schema.statics.getById = function(id, cb) {
    Content.findById(id, async (err, res) => {
        if(err) return cb(err);
        await Content.populate(res, POPULATED_FIELDS);
        return cb(null, res);
    });
};
schema.statics.get = function(cond, options, cb) {
    return getDataFunc(
        Content,
        cond,
        PUBLIC_FIELDS,
        {
            ...options,
            // sort: { createdAt: -1 },
            sort: { updatedAt: -1 },
            populatedFields: POPULATED_FIELDS
        },
        cb
    );
};
schema.statics.delete = function(id, cb) {
    Content.findByIdAndUpdate(id, {
        $set: {
            active: false
        }
    }, { new: true }, (err, res) => {
        if(err) return cb(err);
        return cb(null, res);
    });
};
schema.statics.deleteMany = function(deleted, cb) {
    Content.update({
        _id: {
            $in: deleted
        }
    }, {
        $set: {
            active: false
        }
    }, { multi: true }, (err, res) => {
        if(err) return cb(err);
        return cb(null, res);
    });
};
schema.statics.partialUpdate = function(id, u, cb) {
    Content.findByIdAndUpdate(id, {
        $set: u
    }, { new: true }, (err, res) => {
        if(err) return cb(err);
        return cb(null, res);
    });
};

const Content =  mongoose.model('contents', schema);
module.exports = Content;