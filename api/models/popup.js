const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const mongoosePaginate = require('mongoose-paginate');
const config = require('config');
const defaultLanguage = config.get('defaultLanguage');
const { getDataFunc, getByLanguage, setByLanguage } = require('../lib/helper');
const PopupType = require('./popupType');
const Language = require('./language');
const Content = require('./content');
const PublishingStatus = require('./publishingStatus');

const schema = new Schema({
    // title: { type: String, required: [true, 'Title is required'] },
    type: { type: String, ref: 'popup_types', default: 'announcement' },
    // image: { type: Schema.Types.ObjectId, ref: 'media', required: [true, 'Image is required'] },
    url: { type: String }, // url applying for
    link: { type: String },
    size: {
        type: {
            width: { type: Number },
            height: { type: Number }
        },
        // default: {
        //     width: 700,
        //     height: 600
        // }
    },
    startDate: { type: Date, default: Date.now },
    endDate: Date,
    // content: { type: String, default: '' },
    language: { type: String, ref: 'languages', default: 'vi' },
    content: { type: Schema.Types.ObjectId, ref: 'contents' },
    active: { type: Boolean, default: true },
    publishingStatus: {
        type: Number,
        // required: [true, 'publishingStatus is required'],
        ref: 'publishing_statuses',
        validate: {
            isAsync: true,
            validator: async function(value, respond) {
                const publishing = await PublishingStatus.findOne({ _id: value, active: true });
                return !!publishing;
            },
            message: '{VALUE} not found'
        },
        default: 1
    },
},
{
    timestamps: true
});
schema.plugin(mongoosePaginate);

// const PUBLIC_FIELDS = 'title type size startDate content endDate publishingStatus';
const PUBLIC_FIELDS = 'type url size startDate language content endDate publishingStatus';
const POPULATED_FIELDS = 'type language content publishingStatus';
const MULTILINGUAL_FIELDS = '';

schema.statics.create = function(detail, cb) {
    return new Promise(async (resolve, reject) => {
        try {
            const {
                // language,
                title,
                description: briefContent,
                content,
                keywords,
                ...otherProps
            } = detail;
            // let d = new Popup(language ? await setByLanguage(detail, language, MULTILINGUAL_FIELDS, 'inveslopers') : detail);
            let c = new Content({
                // language,
                title,
                briefContent,
                content,
                keywords,
            })
            let cRes = await c.save();
            let d = new Popup({
                ...otherProps,
                content: cRes._doc._id
            });
            let dRes = await d.save();
            if(cb) resolve(cb(null, dRes, 1));
            else resolve(1);
        } catch (error) {
            if(cb) resolve(cb(error));
            else reject(error);
        }
    });
};
schema.statics.get = function(cond, options, cb) {
    const { language } = options;
    let moreConds = {};
    if (language) moreConds.language = language;
    return getDataFunc(
        Popup,
        {
            ...cond,
            ...moreConds
        },
        PUBLIC_FIELDS,
        {
            ...options,
            sort: { createdAt: -1 },
            // multilingualFields: MULTILINGUAL_FIELDS,
            populatedFields: POPULATED_FIELDS
        },
        async (err, res) => {
            if(err) return cb(err);
            // await Popup.populate(res.docs || res, 'image');
            res.docs = res.docs.map(r => {
                r = r._doc
                const {
                    // language,
                    title,
                    briefContent: description,
                    content,
                    keywords
                } = r.content
                r = {
                    ...r,
                    // language,
                    title,
                    description,
                    content,
                    keywords
                }
                return r
            })
            return cb(null, res);
        }
    );
};
schema.statics.getById = function(id, cb) {
    return Popup.findById(id, PUBLIC_FIELDS, async (err, res) => {
        if(err) return cb(err);
        await Popup.populate(res, POPULATED_FIELDS);
        res = res._doc
        const {
            // language,
            title,
            briefContent: description,
            content,
            keywords
        } = res.content
        res = {
            ...res,
            // language,
            title,
            description,
            content,
            keywords
        }
        return cb(null, res);
    });
};
schema.statics.delete = function(id, cb) {
    Popup.findByIdAndUpdate(id, {
        $set: {
            active: false
        }
    }, { new: true }, (err, res) => {
        if(err) return cb(err);
        return cb(null, res);
    });
};
schema.statics.deleteMany = function(deleted, cb) {
    Popup.update({
        _id: {
            $in: deleted
        }
    }, {
        $set: {
            active: false
        }
    }, { multi: true }, (err, res) => {
        if(err) return cb(err);
        return cb(null, res);
    });
};
schema.statics.partialUpdate = async function(id, u, cb) {
    try {
        const {
            // language,
            title,
            description: briefContent,
            content,
            keywords,
            ...otherProps
        } = u
        const updated = await Popup.findByIdAndUpdate(id, {
            $set: otherProps
        });
        await Content.findByIdAndUpdate(updated.content, {
            // language,
            title,
            briefContent,
            content,
            keywords
        })
        return cb(null, updated);
    } catch (error) {
        return cb(err);
    }
};
schema.statics.publishing = async function(id, publishingStatus, cb) {
    const publishing = await PublishingStatus.findById(publishingStatus);
    if(!!publishing) {
        Popup.findByIdAndUpdate(id, {
            $set: {
                publishingStatus
            }
        }, { new: true }, (err, res) => {
            if(err) return cb(err);
            return cb(null, res);
        });
    } else return cb({
        message: 'The publishing status not found'
    });
};

const Popup =  mongoose.model('popups', schema);
module.exports = Popup;
module.exports.MULTILINGUAL_FIELDS =  MULTILINGUAL_FIELDS;