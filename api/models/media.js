const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const mongoosePaginate = require('mongoose-paginate');
const config = require('config');
const defaultLanguage = config.get('defaultLanguage');
const { getDataFunc, getByLanguage, setByLanguage } = require('../lib/helper');
const User = require('./user');

const schema = new Schema({
    name: String,
    description: { type: String, default: '' },
    path: String,
    mimeType: String,
    active: { type: Boolean, default: true },
    createdBy: { type: Schema.Types.ObjectId, ref: 'users' },
    // createdDate: { type: Date, default: Date.now }
},
{
    timestamps: true
});
schema.plugin(mongoosePaginate);

const PUBLIC_FIELDS = 'description path mimeType createdBy createdAt';
const MULTILINGUAL_FIELDS = 'description'

schema.statics.create = function(detail, { language }, cb) {
    return new Promise(async (resolve, reject) => {
        let d = new Media(language ? await setByLanguage(detail, language, MULTILINGUAL_FIELDS, 'media') : detail);
        // let d = new Media(detail);
        d.save(function(err, doc, affected) {
            if(err) {
                if(cb) resolve(cb(err));
                else reject(err);
            } else {
                if(cb) resolve(cb(null, doc, affected));
                else resolve(affected);
            }
        });
    });
};
schema.statics.get = function(cond, options, cb) {
    const { language } = options;
    return getDataFunc(
        Media,
        cond,
        PUBLIC_FIELDS,
        {
            ...options,
            sort: { createdAt: -1 },
            multilingualFields: MULTILINGUAL_FIELDS
        },
        async (err, res) => {
            await Media.populate(res, {
                path: 'createdBy',
                select: 'firstName lastName username email phone photo'
            });
            cb(err, res);
        }
    );
};
schema.statics.getById = function(id, cb) {
    Media.findById(id, PUBLIC_FIELDS, async (err, res) => {
        if(err) return cb(err);
        await Media.populate(res, {
            path: 'createdBy',
            select: 'firstName lastName username email phone photo'
        });
        return cb(null, res);
    });
};
schema.statics.update = function(id, u, cb) {
    Media.findByIdAndUpdate(id, {
        $set: u
    }, { new: true }, (err, res) => {
        if(err) return cb(err);
        return cb(null, res);
    });
};

const Media =  mongoose.model('media', schema);
module.exports = Media;
module.exports.MULTILINGUAL_FIELDS =  MULTILINGUAL_FIELDS;