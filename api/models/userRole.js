const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate');
const Schema = mongoose.Schema;
const { mapRefModels, translateFields } = require('../lib/mongo.plugin');
// const Organization =  require('./organization');
// const Module =  require('./module');
const { getDataFunc, getByLanguage, setByLanguage } = require('../lib/helper');
const config = require('config');
const defaultLanguage = config.get('defaultLanguage');

const schema = new Schema({
    _id: { type: String },
    name: { type: String, required: true },
    description: { type: String, default: '' },
    level: { type: Number, required: [true, 'Role level is required'] },
    // organization: { type: String, ref: 'organizations', default: 'cvi' },
    // permission: { type: Number, required: true, default: 4 },
    master: { type: Boolean, default: false },
    grant: [{
        module: { type: String, ref: 'modules' },
        permissions: [String] //100 - Unix perms
    }],
    active: { type: Boolean, default: true }
},
{
    timestamps: true
});
schema.plugin(mongoosePaginate);

const PUBLIC_FIELDS = 'name description level master grant';
const POPULATED_FIELDS = 'grant.module';
const MULTILINGUAL_FIELDS = 'name'

schema.plugin(mapRefModels(MULTILINGUAL_FIELDS));
schema.plugin(translateFields);

// schema.pre('save', function(next) {
//     let p = this;
//     if(!p.isModified('_id')) {
//         p._id = toHyphens(p.name)
//     }
//     else next();
// });

schema.statics.create = function(detail, { language }, cb) {
    return new Promise(async (resolve, reject) => {
        // let d = new UserRole(language ? await setByLanguage(detail, language, MULTILINGUAL_FIELDS, 'views') : detail);
        // let d = new UserRole(detail);
        let d = new UserRole(language ? await setByLanguage(detail, language, MULTILINGUAL_FIELDS, 'user_roles') : detail);
        if (!language) {
            d = new UserRole(await setByLanguage(detail, detail.languages.includes(defaultLanguage) ? defaultLanguage : detail.languages[0], MULTILINGUAL_FIELDS, 'user_roles'));
            detail.languages.map(async (l) => await setByLanguage(detail, l, MULTILINGUAL_FIELDS, 'user_roles'));
        } else d = new UserRole(await setByLanguage(detail, language, MULTILINGUAL_FIELDS, 'user_roles'));
        d.save(function(err, doc, affected) {
            if(err) {
                if(cb) resolve(cb(err));
                else reject(err);
            } else {
                if(cb) resolve(cb(null, doc, affected));
                else resolve(affected);
            }
        });
    });
};
schema.statics.get = function(conds, options, cb) {
    return getDataFunc(
        UserRole,
        conds,
        PUBLIC_FIELDS,
        {
            ...options,
            sort: { level: 1 },
            populatedFields: POPULATED_FIELDS,
            multilingualFields: MULTILINGUAL_FIELDS
        },
        (err, res) => cb(err, res)
    );
};

schema.statics.getById = function(id, { language }, cb) {
    return UserRole.findById(id, PUBLIC_FIELDS, async (err, res) => {
        if(err) return cb(err);
        UserRole.populate(res, POPULATED_FIELDS);
        if(language) {
            res = await getByLanguage(res, language, MULTILINGUAL_FIELDS, 'user_roles');
            // res = await UserRole.translateFields(res.toJSON(), language);
        }
        return cb(null, res);
    });
};
schema.statics.delete = function(id, cb) {
    UserRole.findByIdAndRemove(id, (err, res) => {
        if(err) return cb(err);
        return cb(null, res);
    });
};
schema.statics.deleteMany = function(deleted, cb) {
    UserRole.remove({
        _id: {
            $in: deleted
        }
    }, (err, res) => {
        if(err) return cb(err);
        return cb(null, res);
    });
};
schema.statics.partialUpdate = async function(id, u, opts, cb) {
    const { language } = opts;
    let edited = language ? await setByLanguage({...u, _id: id}, language, MULTILINGUAL_FIELDS, 'user_roles') : u;
    UserRole.findByIdAndUpdate(id, {
        $set: edited
    }, { new: true }, (err, res) => {
        if(err) return cb(err);
        return cb(null, res);
    });
};

const UserRole =  mongoose.model('user_roles', schema);
module.exports = UserRole;
// module.exports.MULTILINGUAL_FIELDS =  MULTILINGUAL_FIELDS;