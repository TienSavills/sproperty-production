const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const mongoosePaginate = require('mongoose-paginate');
const config = require('config');
// const defaultLanguage = config.get('defaultLanguage');
const { getDataFunc, generateIdSync, getByLanguage, setByLanguage } = require('../lib/helper');
const Language = require('./language');
const Content = require('./content');

const schema = new Schema({
    _id: { type: String, default: generateIdSync(6) },
    // title: { type: String, required: [true, 'Title is required'] },
    categories: [{ type: Number, ref: 'post_categories' }],
    photo: { type: Schema.Types.ObjectId, ref: 'media' },
    alias: String,
    // keywords: [String],
    module: { type: String, ref: 'modules' },
    // description: { type: String, default: '' },
    // content: { type: String, default: '' },
    language: { type: String, ref: 'languages', default: 'vi' },
    content: { type: Schema.Types.ObjectId, ref: 'contents' },
    startDate: { type: Date, default: Date.now },
    endDate: Date,
    approval: { type: Number, default: 1, ref: 'approval_statuses' },
    approvedBy: { type: Schema.Types.ObjectId, ref: 'users' },
    publishingStatus: { type: Number, ref: 'publishing_statuses', default: 1 },
    active: { type: Boolean, default: true },
    createdBy: { type: Schema.Types.ObjectId, ref: 'users', required: true },
    updatedBy: { type: Schema.Types.ObjectId, ref: 'users' },
},
{
    timestamps: true
});

schema.plugin(mongoosePaginate);

// const PUBLIC_FIELDS = 'title categories photo alias keywords description content createdBy updatedBy publishingStatus createdAt';
const PUBLIC_FIELDS = 'title categories photo alias language content createdBy updatedBy publishingStatus startDate endDate createdAt';
const POPULATED_FIELDS = 'photo categories language content publishingStatus';
const MULTILINGUAL_FIELDS = 'categories.title publishingStatus.title'

schema.statics.create = function(detail, cb) {
    return new Promise(async (resolve, reject) => {
        try {
            const {
                // language,
                title,
                description: briefContent,
                content,
                keywords,
                ...otherProps
            } = detail;
            // let d = new Post(language ? await setByLanguage(detail, language, MULTILINGUAL_FIELDS, 'inveslopers') : detail);
            let c = new Content({
                // language,
                title,
                briefContent,
                content,
                keywords,
            })
            let cRes = await c.save();
            let d = new Post({
                ...otherProps,
                content: cRes._doc._id
            });
            let dRes = await d.save();
            if(cb) resolve(cb(null, dRes, 1));
            else resolve(1);
        } catch (error) {
            if(cb) resolve(cb(error));
            else reject(error);
        }
    });
};

schema.statics.getById = function(id, cb) {
    Post.findById(id, async (err, res) => {
        if(err) return cb(err);
        await Post.populate(res, POPULATED_FIELDS);
        // if(language) res = getByLanguage(res, language, 'category.title');
        await Post.populate(res, {
            path: 'createdBy',
            select: 'fullname username email phone photo'
        });
        res = res._doc
        const {
            // language,
            title,
            briefContent: description,
            content,
            keywords
        } = res.content
        res = {
            ...res,
            // language,
            title,
            description,
            content,
            keywords
        }
        return cb(null, res);
    });
};

schema.statics.get = function(cond, options, cb) {
    const { language } = options;
    let moreConds = {};
    if (language) moreConds.language = language;
    return getDataFunc(
        Post,
        {
            ...cond,
            ...moreConds
        },
        PUBLIC_FIELDS,
        {
            ...options,
            // sort: { createdAt: -1 },
            sort: { updatedAt: -1 },
            // multilingualFields: MULTILINGUAL_FIELDS,
            populatedFields: POPULATED_FIELDS
        },
        async (err, res) => {
            if(err) return cb(err);
            // await Post.populate(res.docs || res, 'image');
            await Post.populate(res.docs || res, {
                path: 'createdBy',
                select: 'fullname username email phone photo'
            });
            res.docs = res.docs.filter(r=>!!r._doc.content).map(r => {
                r = r._doc
                const {
                    // language,
                    title,
                    briefContent: description,
                    content,
                    keywords
                } = r.content;
                r = {
                    ...r,
                    // language,
                    title,
                    description,
                    content,
                    keywords
                }
                return r
            })
            // await Post.populate(res.docs || res, 'categories');
            return cb(null, res);
        }
    );
};
schema.statics.delete = function(id, cb) {
    Post.findByIdAndUpdate(id, {
        $set: {
            active: false
        }
    }, { new: true }, (err, res) => {
        if(err) return cb(err);
        return cb(null, res);
    });
};
schema.statics.deleteMany = function(deleted, cb) {
    Post.update({
        _id: {
            $in: deleted
        }
    }, {
        $set: {
            active: false
        }
    }, { multi: true }, (err, res) => {
        if(err) return cb(err);
        return cb(null, res);
    });
};
schema.statics.partialUpdate = async function(id, u, cb) {
    try {
        const {
            // language,
            title,
            description: briefContent,
            content,
            keywords,
            ...otherProps
        } = u
        const updated = await Post.findByIdAndUpdate(id, {
            $set: otherProps
        });
        await Content.findByIdAndUpdate(updated.content, {
            // language,
            title,
            briefContent,
            content,
            keywords
        })
        return cb(null, updated);
    } catch (error) {
        return cb(error);
    }
};
schema.statics.publishing = async function(id, publishingStatus, cb) {
    const publishing = await PublishingStatus.findById(publishingStatus);
    if(!!publishing) {
        Post.findByIdAndUpdate(id, {
            $set: {
                publishingStatus
            }
        }, { new: true }, (err, res) => {
            if(err) return cb(err);
            return cb(null, res);
        });
    } else return cb({
        message: 'The publishing status not found'
    });
};

const Post =  mongoose.model('posts', schema);
module.exports = Post;
module.exports.MULTILINGUAL_FIELDS =  MULTILINGUAL_FIELDS;