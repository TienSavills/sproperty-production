const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const mongoosePaginate = require('mongoose-paginate');
const { getDataFunc } = require('../lib/helper');

const schema = new Schema({
    _id: { type: String, required: true },
    title: { type: String, required: [true, 'Menu type is required'] },
    description: { type: String, default: '' },
    active: { type: Boolean, default: true }
},
{
    timestamps: true
});
schema.plugin(mongoosePaginate);

const PUBLIC_FIELDS = 'title description createdAt';

schema.statics.create = function(detail, cb) {
    return new Promise(async (resolve, reject) => {
        let d = new MenuType(detail);
        d.save(function(err, doc, affected) {
            if(err) {
                if(cb) resolve(cb(err));
                else reject(err);
            } else {
                if(cb) resolve(cb(null, doc, affected));
                else resolve(affected);
            }
        });
    });
};

schema.statics.getById = function(id, cb) {
    MenuType.findById(id, async (err, res) => {
        if(err) return cb(err);
        await MenuType.populate(res, POPULATED_FIELDS);
        return cb(null, res);
    });
};

schema.statics.get = function(cond, options, cb) {
    return getDataFunc(
        MenuType,
        cond,
        PUBLIC_FIELDS,
        {
            ...options,
            sort: { title: 1 }
        },
        (err, res) => {
            // res = getByLanguage(res, language, 'title');
            cb(err, res);
        }
    );
};

schema.statics.delete = function(id, cb) {
    MenuType.findByIdAndUpdate(id, {
        $set: {
            active: false
        }
    }, { new: true }, (err, res) => {
        if(err) return cb(err);
        return cb(null, res);
    });
};
schema.statics.deleteMany = function(deleted, cb) {
    MenuType.update({
        _id: {
            $in: deleted
        }
    }, {
        $set: {
            active: false
        }
    }, { multi: true }, (err, res) => {
        if(err) return cb(err);
        return cb(null, res);
    });
};
schema.statics.partialUpdate = function(id, edited, cb) {
    MenuType.findByIdAndUpdate(id, {
        $set: edited
    }, { new: true }, (err, res) => {
        if(err) return cb(err);
        return cb(null, res);
    });
};

const MenuType =  mongoose.model('menu_types', schema);
module.exports = MenuType;