const mongoose = require('mongoose');
const Schema = mongoose.Schema;
// const mongoosePaginate = require('mongoose-paginate');
const config = require('config');
const defaultLanguage = config.get('defaultLanguage');
const { getDataFunc, getByLanguage, setByLanguage, forEachAsync } = require('../lib/helper');

const schema = new Schema({
    _id: String,
    referenceId: { type: String, default: '' },
    code: { type: String, default: '' },
    name: { type: String, default: '' },
    active: { type: Boolean, default: true },
}, {
    timestamps: false
});
// schema.plugin(mongoosePaginate);

const PUBLIC_FIELDS = 'referenceId name code';

schema.statics.get = function (cond, options, cb) {
    return getDataFunc(
        MappingKey,
        cond,
        PUBLIC_FIELDS,
        {
            ...options,
            sort: { name: 1 },
        },
        async (err, res) => {
            cb(err, res);
        }
    );
};

schema.statics.create = function(detail, cb) {
    return new Promise(async (resolve, reject) => {
        let d = new MappingKey(detail);
        d.save(function(err, doc, affected) {
            if(err) {
                if(cb) resolve(cb(err));
                else reject(err);
            } else {
                if(cb) resolve(cb(null, doc, affected));
                else resolve(affected);
            }
        });
    });
};

const MappingKey = mongoose.model('mapping_key', schema);
module.exports = MappingKey;
