const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const mongoosePaginate = require('mongoose-paginate');
const path = require('path');
const config = require('config');
const defaultLanguage = config.get('defaultLanguage');
const { getDataFunc, forEachAsync } = require('../lib/helper');

const schema = new Schema({
    // _id: String,
    refId: {
        type: Schema.Types.Mixed,
        refPath: 'refModel'
    },
    refModel: String,
    key: String,
    value: String,
    language: String,
    active: { type: Boolean, default: true }
},
{
    timestamps: true
});
schema.plugin(mongoosePaginate);

const PUBLIC_FIELDS = 'refId refModel key value language';
const TRANSLATED_MODELS = [
    'amenities',
    'approvalStatus',
    'currency',
    'district',
    'facing',
    'inboxGroup',
    'listingGroup',
    'projectStatus',
    'projectType',
    'propertyFurniture',
    'propertyGroup',
    'propertySovereignty',
    'propertyStatus',
    'propertyType',
    'propertyUse',
    'province',
    'publishingStatus',
    'status',
    'transportation',
    'unit',
    'view'
]

schema.statics.create = function(detail, cb) {
    return new Promise((resolve, reject) => {
        let d = new Dictionary(detail);
        d.save(function(err, doc, affected) {
            if(err) {
                if(cb) resolve(cb(err));
                else reject(err);
            } else {
                if(cb) resolve(cb(null, doc, affected));
                else resolve(affected);
            }
        });
    });
};
schema.statics.get = function(cond, options, cb) {
    const { pagination } = options;
    // return getDataFunc(
    //     Dictionary,
    //     cond,
    //     PUBLIC_FIELDS,
    //     {
    //         ...options,
    //         sort: { refModel: 1 }
    //     },
    //     async (err, res) => {
    //         if(err) return cb(err);
    //         return cb(null, res);
    //     }
    // );
    Dictionary.aggregate([
        {
            $match: cond
        },
        {
            $group: {
                _id: {
                    refId: '$refId',
                    refModel: '$refModel',
                    key: '$key'
                },
                // refId: '$refId',
                // key: '$key',
                dictionary: {
                    $push: {
                        k: '$language',
                        v: '$value'
                    }
                }
            },
        },
        {
            $project: {
                _id: 1,
                refId: '$_id.refId',
                key: '$_id.key',
                refModel: '$_id.refModel',
                values: {
                    $arrayToObject: '$dictionary'
                }
            }
        },
        {
            $group: {
                _id: null,
                total: {
                    $sum: 1
                },
                res: {
                    $push: '$$ROOT'
                }
            }
        },
        // {
        //     $skip: pagination.offset || ((pagination.page - 1) * pagination.limit)
        // },
        // {
        //     $limit: pagination.limit
        // },
        {
            $project: {
                _id: 0,
                // offset: {
                //     $literal: pagination.offset
                // },
                page: pagination && pagination.page ? {
                    $literal: pagination.page
                } : null,
                limit: pagination && pagination.limit ? {
                    $literal: pagination.limit
                } : null,
                total: 1,
                docs: pagination && Object.keys(pagination).length >= 2 ? {
                    $slice: ['$res', pagination.offset || ((pagination.page - 1) * pagination.limit), pagination.limit]
                } : '$res'
                // $facet: {
                //     docs: {
                //         $skip: pagination.offset || ((pagination.page - 1) * pagination.limit),
                //         $limit: pagination.limit
                //     }
                // }
            }
        }
    ]).exec(async function(err, res) {
        if(err) return cb(err);
        // let pages = [];
        res = res[0] || {};
        if(Object.keys(res).length < 1) return cb();
        if(pagination) {
            let md = res.total % pagination.limit;
            res.pages = Array.from(Array(Math.floor(res.total / pagination.limit) + (md > 0 ? 1 : 0)).keys()).map(v => v + 1).length;
        } else res.pages = 0;
        res.docs.forEach(el => el._id = `${el._id.refId}-${el._id.refModel}-${el._id.key}`);
        await forEachAsync(res.docs, async el => {
            const { refId, key, refModel } = el;
            el = await Dictionary.populate(el, {
                path: 'refId'
            });
            el.defaultValue = el.refId ? el.refId[el.key] : '';
            const foundRelated = await Dictionary.find({ refId, key, refModel });
            foundRelated.map(fr => fr._doc).forEach(fr => el.values[fr.language] = fr.value);
        });
        return cb(null, res);
    });
};
schema.statics.import = function(data, cb) {
    let affected = 0;
    return new Promise(async (resolve, reject) => {
        await forEachAsync(data, async (translated) => {
            const { refId, key, value, language, _id, ...otherProps } = translated;
            // if(refId === '' || refId === '<null>' || refId === undefined) console.log(translated);
            if(refId !== '' && refId !== '<null>' && refId !== undefined) {
                try {
                    await Dictionary.translate(translated);
                    affected++;
                } catch(err) {
                    console.log(err)
                }
            }
        });
        if(cb) return resolve(cb(null, affected));
        else return resolve(affected);
    });
};
schema.statics.translate = function(target, cb) {
    return new Promise((resolve, reject) => {
        const { refId, refModel, key, value, language, _id, ...otherProps } = target;
        // console.log(target);
        Dictionary.update({
            refId,
            refModel,
            key,
            // value,
            language,
        }, {
            $set: {
                refId,
                refModel,
                key,
                value,
                language,
                // ...otherProps,
                active: true
            }
        }, {
            upsert: true,
            new: true
        }, (err, res) => {
            if(err) {
                if(cb) return resolve(cb(err));
                else return reject(err);
            } else {
                if(cb) return resolve(cb(null, res));
                else return resolve(res);
            }
        });
    });
};
schema.statics.refresh = function(languages, cb) {
    return new Promise(async (resolve, reject) => {
        try {
            await forEachAsync(TRANSLATED_MODELS, async (tm) => {
                const tmodel = require(path.join(__dirname, tm))
                const foundDefaultData = await tmodel.find({ active: true });
                foundDefaultData.forEach(fdd => {
                    tmodel.MULTILINGUAL_FIELDS.split(' ').forEach(async (mf) => {
                        const foundTranslated = await Dictionary.find({
                            refId: fdd._id,
                            refModel: tmodel.collection.collectionName,
                            key: mf,
                        })
                        if (foundTranslated.length < 1) {
                            await forEachAsync(languages.filter(l => l !== defaultLanguage), async (language) => await Dictionary.update({
                                refId: fdd._id,
                                refModel: tmodel.collection.collectionName,
                                key: mf,
                                value: {
                                    $exists: false
                                },
                                language,
                            }, {
                                $set: {
                                    refId: fdd._id,
                                    refModel: tmodel.collection.collectionName,
                                    key: mf,
                                    value: '',
                                    language,
                                    // ...otherProps,
                                    active: true
                                }
                            }, {
                                upsert: true,
                                new: true
                            }));
                            await Dictionary.update({
                                refId: fdd._id,
                                refModel: tmodel.collection.collectionName,
                                key: mf,
                                value: {
                                    $exists: false
                                },
                                language: defaultLanguage,
                            }, {
                                $set: {
                                    refId: fdd._id,
                                    refModel: tmodel.collection.collectionName,
                                    key: mf,
                                    value: fdd[mf],
                                    language: defaultLanguage,
                                    // ...otherProps,
                                    active: true
                                }
                            }, {
                                upsert: true,
                                new: true
                            });
                        }
                    })
                })
            });
            if(cb) return resolve(cb(null));
            else return resolve();
        } catch (err) {
            if(cb) return resolve(cb(err));
            else return reject(err);
        }
    });
};
schema.statics.delete = function(id, cb) {
    Dictionary.findByIdAndRemove(id, (err, res) => {
        if(err) return cb(err);
        return cb(null, res);
    });
};
schema.statics.deleteMany = function(deleted, cb) {
    Dictionary.remove({
        _id: {
            $in: deleted
        }
    }, (err, res) => {
        if(err) return cb(err);
        return cb(null, res);
    });
};

const Dictionary =  mongoose.model('dictionaries', schema);
module.exports = Dictionary;
// module.exports.MULTILINGUAL_FIELDS =  MULTILINGUAL_FIELDS;
module.exports.TRANSLATED_MODELS =  TRANSLATED_MODELS;