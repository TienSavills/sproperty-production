const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const mongoosePaginate = require('mongoose-paginate');
const config = require('config');
const defaultLanguage = config.get('defaultLanguage');
const { getDataFunc, getByLanguage, setByLanguage } = require('../lib/helper');

const schema = new Schema({
    _id: { type: String },
    name: { type: String, default: '' },
    description: { type: String, default: '' },
    path: String,
    // moduleName: String,
    group: { type: String, ref: 'groups', default: config.get('defaultGroup.id') },
    system: { type: Boolean, default: false },
    permissions: [String],
    active: { type: Boolean, default: true },
    // createdDate: { type: Date, default: Date.now }
},
{
    timestamps: true
});
schema.plugin(mongoosePaginate);

const PUBLIC_FIELDS = 'name description path group system permissions';
const MULTILINGUAL_FIELDS = 'name description'

schema.statics.create = function(detail, { language }, cb) {
    return new Promise(async (resolve, reject) => {
        let d = new Module(language ? await setByLanguage(detail, language, MULTILINGUAL_FIELDS, 'modules') : detail);
        d.save(function(err, doc, affected) {
            if(err) {
                if(cb) resolve(cb(err));
                else reject(err);
            } else {
                if(cb) resolve(cb(null, doc, affected));
                else resolve(affected);
            }
        });
    });
};
schema.statics.get = function(cond, options, cb) {
    const { language } = options;
    return getDataFunc(
        Module,
        cond,
        PUBLIC_FIELDS,
        {
            ...options,
            sort: { name: 1 },
            multilingualFields: MULTILINGUAL_FIELDS
        },
        (err, res) => {
            cb(err, res);
        }
    );
};

const Module =  mongoose.model('modules', schema);
module.exports = Module;
module.exports.MULTILINGUAL_FIELDS =  MULTILINGUAL_FIELDS;