const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const mongoosePaginate = require('mongoose-paginate');
const { getDataFunc } = require('../lib/helper');
// const config = require('config');

const schema = new Schema({
    _id: String,
    photo: { type: Schema.Types.ObjectId, ref: 'media' },
    name: { type: String, required: [true, 'Agency name is required'] },
    description: { type: String, default: '' },
    email: String,
    tel: String,
    hotline: String,
    location: {
        address: { type: String },
        district: { type: String, ref: 'districts' },
        province: { type: String, ref: 'provinces' },
        country: { type: String, ref: 'countries' },
        location: {
            latitude: Number,
            longitude: Number
        },
        formattedAddress: { type: String },
    },
    website: String,
    socials: {
        twitter: String,
        facebook: String,
        linkedin: String,
        googlePlus: String,
        youtube: String,
    },
    active: { type: Boolean, default: true },
    // createdDate: { type: Date, default: Date.now }
},
{
    timestamps: true
});
schema.plugin(mongoosePaginate);

const PUBLIC_FIELDS = 'photo name description email tel hotline location website socials';
const POPULATED_FIELDS = 'photo location.province location.district';

schema.statics.create = function(detail, cb) {
    return new Promise((resolve, reject) => {
        let o = new Organization(detail);
        o.save((err, doc, affected) => {
            if(err) {
                if(cb) resolve(cb(err));
                else reject(err);
            } else {
                if(cb) resolve(cb(null, doc, affected));
                else resolve(affected);
            }
        });
    });
};
schema.statics.getById = function(id, cb) {
    Organization.findById(id, PUBLIC_FIELDS, async (err, res) => {
        if(err) return cb(err);
        await Organization.populate(res, POPULATED_FIELDS);
        // if(language) res = getByLanguage(res, language, 'category.title');
        await Organization.populate(res, {
            path: 'createdBy',
            select: 'fullname username email phone photo'
        });
        return cb(null, res);
    });
};
schema.statics.get = function(cond, options, cb) {
    const { language } = options;
    return getDataFunc(
        Organization,
        cond,
        PUBLIC_FIELDS,
        {
            ...options,
            sort: { name: 1 }
        },
        async (err, res) => {
            return cb(err, res);
        }
    );
};
schema.statics.partialUpdate = async function(id, u, cb) {
    Organization.findByIdAndUpdate(id, {
        $set: u
    }, { new: true }, (err, res) => {
        if(err) return cb(err);
        return cb(null, res);
    });
};

const Organization =  mongoose.model('organizations', schema);
module.exports = Organization;
// module.exports.MULTILINGUAL_FIELDS =  MULTILINGUAL_FIELDS;