const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const mongoosePaginate = require('mongoose-paginate');
const config = require('config');
const defaultLanguage = config.get('defaultLanguage');
const { getDataFunc, getByLanguage, setByLanguage } = require('../lib/helper');
const User = require('./user');
const InboxGroup = require('./inboxGroup');
const Organization = require('./organization');

const schema = new Schema({
    fullname: String,
    email: String,
    phone: String,
    message: String,
    refProduct: { type: String, refPath: 'refModel' },
    refModel: String,
    to: { type: Schema.Types.ObjectId, ref: 'users' },
    title: { type: String, default: '' },
    locale: { type: String, default: defaultLanguage },
    agency: { type: String, ref: 'organizations', default: config.get('defaultOrganization.id') },
    active: { type: Boolean, default: true }
},
{
    timestamps: true
});

schema.plugin(mongoosePaginate);

const PUBLIC_FIELDS = 'fullname email phone message refProduct refModel to title locale agency createdAt';
const POPULATED_FIELDS = 'refProduct agency';
// const MULTILINGUAL_FIELDS = 'title'

schema.statics.create = function(detail, { language }, cb) {
    return new Promise((resolve, reject) => {
        // let d = new Property(language ? await setByLanguage(detail, language, MULTILINGUAL_FIELDS) : detail);
        let d = new Inbox({ ...detail, language });
        d.save(async function(err, doc, affected) {
            if(err) {
                if(cb) resolve(cb(err));
                else reject(err);
            } else {
                await Inbox.populate(doc, POPULATED_FIELDS);
                // if(language) res = getByLanguage(res, language, MULTILINGUAL_FIELDS);
                await Inbox.populate(doc, [
                    {
                        path: 'to',
                        select: 'firstName lastName username email phone photo'
                    }
                ]);
                if(cb) resolve(cb(null, doc, affected));
                else resolve(affected);
            }
        });
    });
};

schema.statics.getById = function(id, { language }, cb) {
    Inbox.findById(id, async (err, res) => {
        if(err) return cb(err);
        await Inbox.populate(res, POPULATED_FIELDS);
        // if(language) res = getByLanguage(res, language, MULTILINGUAL_FIELDS);
        await Inbox.populate(res, [
            {
                path: 'to',
                select: 'firstName lastName username email phone photo'
            }
        ]);
        return cb(null, res);
    });
};

schema.statics.get = function(cond, options, cb) {
    return getDataFunc(
        Inbox,
        cond,
        PUBLIC_FIELDS,
        {
            ...options,
            sort: { createdAt: -1 },
            populatedFields: POPULATED_FIELDS,
            // multilingualFields: MULTILINGUAL_FIELDS
        },
        async (err, res) => {
            await Inbox.populate(res.docs || res, [
                {
                    path: 'to',
                    select: 'firstName lastName username email phone photo'
                }
            ]);
            cb(err, res);
        }
    );
};

schema.statics.forwarding = async function(id, to, cb) {
    Inbox.findByIdAndUpdate(id, {
        $set: {
            to
        }
    }, { new: true }, (err, res) => {
        if(err) return cb(err);
        return cb(null, res);
    });
};

const Inbox =  mongoose.model('inboxes', schema);
module.exports = Inbox;
// module.exports.MULTILINGUAL_FIELDS =  MULTILINGUAL_FIELDS;