const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const mongoosePaginate = require('mongoose-paginate');
const config = require('config');
const defaultLanguage = config.get('defaultLanguage');
const { getDataFunc, getByLanguage, setByLanguage } = require('../lib/helper');

const schema = new Schema({
    _id: Number,
    code: { type: String, default: '' },
    title: { type: String, default: '' },
    active: { type: Boolean, default: true },
},
{
    timestamps: true
});
schema.plugin(mongoosePaginate);

const PUBLIC_FIELDS = 'title code';
const MULTILINGUAL_FIELDS = 'title'

schema.pre('save', function(next) {
    let p = this;
    if(!p.isModified('_id')) next();
    ListingGroup.find((err, res) => {
        if(err) next(err);
        else if(res.length > 0) p._id = res[0]._id + 1;
        next();
    }).sort({ _id : -1 }).limit(1);
});

schema.statics.create = function(detail, { language }, cb) {
    return new Promise(async (resolve, reject) => {
        let d = new ListingGroup(language ? await setByLanguage(detail, language, MULTILINGUAL_FIELDS, 'listing_groups') : detail);
        d.save(function(err, doc, affected) {
            if(err) {
                if(cb) resolve(cb(err));
                else reject(err);
            } else {
                if(cb) resolve(cb(null, doc, affected));
                else resolve(affected);
            }
        });
    });
};
schema.statics.get = function(cond, options, cb) {
    const { language } = options;
    return getDataFunc(
        ListingGroup,
        cond,
        PUBLIC_FIELDS,
        {
            ...options,
            sort: { title: 1 },
            multilingualFields: MULTILINGUAL_FIELDS
        },
        async (err, res) => {
            cb(err, res);
        }
    );
};

const ListingGroup =  mongoose.model('listing_groups', schema);
module.exports = ListingGroup;
module.exports.MULTILINGUAL_FIELDS =  MULTILINGUAL_FIELDS;