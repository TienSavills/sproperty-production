const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const mongoosePaginate = require('mongoose-paginate');
const { getDataFunc } = require('../lib/helper');

const schema = new Schema({
    _id: { type: String },
    // code: String,
    name: { type: String, required: true },
    active: { type: Boolean, default: true }
},
{
    timestamps: true
});

schema.plugin(mongoosePaginate);

const PUBLIC_FIELDS = 'name';

// schema.pre('save', function(next) {
//     let p = this;
//     if(!p.isModified('_id')) next();
//     Province.find((err, res) => {
//         if(err) next(err);
//         else if(res.length > 0) p._id = res[0]._id + 1;
//         next();
//     }).sort({ _id : -1 }).limit(1);
// });
schema.statics.create = function(detail, cb) {
    return new Promise((resolve, reject) => {
        let d = new Language(detail);
        d.save(function(err, doc, affected) {
            if(err) {
                if(cb) resolve(cb(err));
                else reject(err);
            } else {
                if(cb) resolve(cb(null, doc, affected));
                else resolve(affected);
            }
        });
    });
};
schema.statics.get = function(cond, options, cb) {
    return getDataFunc(
        Language,
        cond,
        PUBLIC_FIELDS,
        {
            ...options,
            sort: { name: 1 }
        },
        (err, res) => {
            cb(err, res);
        }
    );
};
schema.statics.getLanguages = function(cb) {
    Language.find({}, null, {sort: {name: 1}}, function(err, res) {
        if(err) return cb(err);
        return cb(null, res);
    });
};
schema.statics.getLanguage = function(id, cb) {
    Language.findById(id, function(err, res) {
        if(err) return cb(err);
        return cb(null, res);
    });
};
schema.statics.import = function(data, cb) {
    Language.insertMany(data, (err, docs) => {
        if(err) return cb(err);
        else return cb(null, docs, docs.length);
    });
};

const Language =  mongoose.model('languages', schema);
module.exports = Language;
// module.exports.MULTILINGUAL_FIELDS =  MULTILINGUAL_FIELDS;