const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const Province = require('./province');
const { getDataFunc } = require('../lib/helper');

const schema = new Schema({
    _id: { type: String },
    province: { type: String, ref: 'provinces', required: true },
    name: { type: String, required: true },
    active: { type: Boolean, default: true },
    status: { type: Number, default: 2 },
},
{
    timestamps: true
});

const PUBLIC_FIELDS = 'province name';
const MULTILINGUAL_FIELDS = 'name'

// schema.pre('save', function(next) {
//     let p = this;
//     if(!p.isModified('_id')) next();
//     District.find((err, res) => {
//         if(err) next(err);
//         else if(res.length > 0) p._id = res[0]._id + 1;
//         next();
//     }).sort({ _id : -1 }).limit(1);
// });
schema.statics.get = function(cond, options, cb) {
    return getDataFunc(
        District,
        {
            ...cond,
            status: 2,
        },
        PUBLIC_FIELDS,
        {
            ...options,
            multilingualFields: MULTILINGUAL_FIELDS,
            sort: { name: 1 }
        },
        (err, res) => cb(err, res)
    );
};
schema.statics.getDistricts = function(provinceId, cb) {
    let q = {};
    if(provinceId) q.province = provinceId;
    District.find({ ...q, status: 2 }, null, {sort: {name: 1}}, async function(err, res) {
        if(err) return cb(err);
        res = await Province.populate(res, { path: 'province' });
        return cb(null, res);
    });
};
schema.statics.getDistrict = function(id, cb) {
    District.findById(id, async function(err, res) {
        if(err) return cb(err);
        res = await Province.populate(res, { path: 'province' });
        return cb(null, res);
    });
};
schema.statics.import = function(data, cb) {
    District.insertMany(data, (err, docs) => {
        if(err) return cb(err);
        else return cb(null, docs, docs.length);
    });
};

const District =  mongoose.model('districts', schema);
module.exports = District;
module.exports.MULTILINGUAL_FIELDS =  MULTILINGUAL_FIELDS;