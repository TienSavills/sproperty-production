const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const mongoosePaginate = require('mongoose-paginate');
const config = require('config');
const defaultLanguage = config.get('defaultLanguage');
const { getDataFunc, getByLanguage, setByLanguage } = require('../lib/helper');

const schema = new Schema({
    _id: { type: String },
    title: { type: String, required: [true, 'Title is required'] },
    description: { type: String, default: '' },
    active: { type: Boolean, default: true }
},
{
    timestamps: true
});
schema.plugin(mongoosePaginate);

const PUBLIC_FIELDS = 'title description';
const POPULATED_FIELDS = '';
const MULTILINGUAL_FIELDS = '';

schema.statics.create = function(detail, { language }, cb) {
    return new Promise(async (resolve, reject) => {
        let d = new PopupType(language ? await setByLanguage(detail, language, MULTILINGUAL_FIELDS, 'inveslopers') : detail);
        // let d = new PopupType(detail);
        d.save(function(err, doc, affected) {
            if(err) {
                if(cb) resolve(cb(err));
                else reject(err);
            } else {
                if(cb) resolve(cb(null, doc, affected));
                else resolve(affected);
            }
        });
    });
};
schema.statics.get = function(cond, options, cb) {
    const { language } = options;
    return getDataFunc(
        PopupType,
        cond,
        PUBLIC_FIELDS,
        {
            ...options,
            sort: { title: 1 },
            multilingualFields: MULTILINGUAL_FIELDS,
            populatedFields: POPULATED_FIELDS
        },
        async (err, res) => {
            if(err) return cb(err);
            await PopupType.populate(res.docs || res, 'image');
            return cb(null, res);
        }
    );
};
schema.statics.getById = function(id, { language }, cb) {
    return PopupType.findById(id, PUBLIC_FIELDS, async (err, res) => {
        if(err) return cb(err);
        await PopupType.populate(res, POPULATED_FIELDS);
        return cb(null, res);
    });
};
schema.statics.delete = function(id, cb) {
    PopupType.findByIdAndUpdate(id, {
        $set: {
            active: false
        }
    }, { new: true }, (err, res) => {
        if(err) return cb(err);
        return cb(null, res);
    });
};
schema.statics.deleteMany = function(deleted, cb) {
    PopupType.update({
        _id: {
            $in: deleted
        }
    }, {
        $set: {
            active: false
        }
    }, { multi: true }, (err, res) => {
        if(err) return cb(err);
        return cb(null, res);
    });
};
schema.statics.partialUpdate = function(id, u, cb) {
    PopupType.findByIdAndUpdate(id, {
        $set: u
    }, { new: true }, (err, res) => {
        if(err) return cb(err);
        return cb(null, res);
    });
};

const PopupType =  mongoose.model('popup_types', schema);
module.exports = PopupType;
module.exports.MULTILINGUAL_FIELDS =  MULTILINGUAL_FIELDS;