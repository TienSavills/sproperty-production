const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const { getDataFunc, getByLanguage, setByLanguage, forEachAsync } = require('../lib/helper');

const schema = new Schema({
    user: { type: Schema.Types.ObjectId, ref: 'users', required: true },
    ip: { type: String, required: true },
    os: { type: String, default: 'Unknown' },
    userAgent: { type: String, default: 'Unknown' },
    accessToken: { type: String, required: true },
    expires: { type: Date, required: true, default: new Date(Date.now() + (1 * 24 * 60 * 60 * 1000)) },
    loginDate: { type: Date, required: true, default: Date.now },
    // createdDate: { type: Date, default: Date.now }
},
{
    timestamps: true
});

schema.statics.getLocations = ({ userId, ip }, callback) => {
    UserLoginLocation.find({ user: userId, ip: { $ne: ip } }, callback);
};
schema.statics.addLocation = (location, callback) => {
    const loginDate = Date.now()
    location.expires = loginDate + (1 * 24 * 60 * 60 * 1000);
    UserLoginLocation.findOneAndUpdate({
        user: location.user,
        ip: location.ip,
        userAgent: location.userAgent,
        // loginDate: new Date(Date.now())
    }, location, { upsert: true, new: true }, callback);
};
schema.statics.removeLocations = async (locations) => {
    await forEachAsync(locations, async (location) => {
        await UserLoginLocation.findOneAndRemove({ user: location.userId, ip: location.ip });
    });
};
schema.statics.removeOtherLocations = userId => {
    UserLoginLocation.remove({ user: userId });
};

const UserLoginLocation =  mongoose.model('user_login_locations', schema);
module.exports = UserLoginLocation;
// module.exports.MULTILINGUAL_FIELDS =  MULTILINGUAL_FIELDS;