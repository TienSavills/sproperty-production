const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const mongoosePaginate = require('mongoose-paginate');
const config = require('config');
const defaultLanguage = config.get('defaultLanguage');
const { getDataFunc, getByLanguage, setByLanguage, forEachAsync, getNestedChildren } = require('../lib/helper');
const PublishingStatus = require('./publishingStatus');

const schema = new Schema({
    _id: Number,
    title: { type: String, required: [true, 'Category name is required'] },
    description: { type: String, default: '' },
    photo: { type: Schema.Types.ObjectId, ref: 'media' },
    parent: { type: Number, ref: 'post_categories' },
    alias: String,
    keywords: [String],
    module: { type: String, ref: 'modules' },
    startDate: { type: Date, default: Date.now },
    endDate: Date,
    approval: { type: Number, default: 1, ref: 'approval_statuses' },
    approvedBy: { type: Schema.Types.ObjectId, ref: 'users' },
    publishingStatus: { type: Number, ref: 'publishing_statuses', default: 1 },
    subCount: {
        type: Number,
        isAsync: true,
        get: async function(v) {
            return await PostCategory.count({
                parent: this._id
            });
        }
    },
    active: { type: Boolean, default: true }
},
{
    timestamps: true
});
schema.plugin(mongoosePaginate);

const PUBLIC_FIELDS = 'title description photo parent alias keywords publishingStatus subCount createdAt';
const POPULATED_FIELDS = 'photo publishingStatus';
const MULTILINGUAL_FIELDS = 'title description publishingStatus.title';

schema.pre('save', function(next) {
    let p = this;
    if(!p.isModified('_id')) PostCategory.find((err, res) => {
        if(err) next(err);
        else {
            if(res.length > 0) p._id = res[0]._id + 1;
            else p._id = 1;
        }
        next();
    }).sort({ _id : -1 }).limit(1);
    else next();
});

schema.statics.create = function(detail, { language }, cb) {
    return new Promise(async (resolve, reject) => {
        if(!detail.publishingStatus) detail.publishingStatus = 1;
        let d = new PostCategory(language ? await setByLanguage(detail, language, MULTILINGUAL_FIELDS, 'post_categories') : detail);
        d.save(function(err, doc, affected) {
            if(err) {
                if(cb) resolve(cb(err));
                else reject(err);
            } else {
                if(cb) resolve(cb(null, doc, affected));
                else resolve(affected);
            }
        });
    });
};

schema.statics.getById = function(id, { language }, cb) {
    PostCategory.findById(id, async (err, res) => {
        if(err) return cb(err);
        await PostCategory.populate(res, POPULATED_FIELDS);
        return cb(null, res);
    });
};

schema.statics.get = function(cond, options, cb) {
    const { language } = options;
    return getDataFunc(
        PostCategory,
        cond,
        PUBLIC_FIELDS,
        {
            ...options,
            sort: { title: 1 },
            populatedFields: POPULATED_FIELDS,
            multilingualFields: MULTILINGUAL_FIELDS
        },
        async (err, res) => {
            await forEachAsync(res, async element => {
                element.subCount = await element.subCount;
            });
            // res = getByLanguage(res, language, 'title');
            cb(err, res);
        }
    );
};

schema.statics.getTree = function(cond, options, cb) {
    const { language } = options;
    return getDataFunc(
        PostCategory,
        cond,
        PUBLIC_FIELDS,
        {
            ...options,
            sort: { title: 1 },
            populatedFields: POPULATED_FIELDS,
            multilingualFields: MULTILINGUAL_FIELDS
        },
        async (err, res) => {
            // console.log(res)
            let t = getNestedChildren(res, null);
            // res = getByLanguage(res, language, 'title');
            cb(err, t);
        }
    );
};
schema.statics.delete = function(id, cb) {
    PostCategory.findByIdAndUpdate(id, {
        $set: {
            active: false
        }
    }, { new: true }, (err, res) => {
        if(err) return cb(err);
        return cb(null, res);
    });
};
schema.statics.deleteMany = function(deleted, cb) {
    PostCategory.update({
        _id: {
            $in: deleted
        }
    }, {
        $set: {
            active: false
        }
    }, { multi: true }, (err, res) => {
        if(err) return cb(err);
        return cb(null, res);
    });
};
schema.statics.partialUpdate = function(id, u, opts, cb) {
    const { language } = opts;
    let edited = {};
    if(language) {
        const multilingualFields = MULTILINGUAL_FIELDS.split(' ');
        Object.keys(u).filter(f => !multilingualFields.includes(f)).forEach(f => edited[f] = u[f]);
        multilingualFields.forEach(f => {
            if(f in u) edited[`${f}.${language}`] = u[f];
        });
    } else edited = u;
    
    PostCategory.findByIdAndUpdate(id, {
        $set: edited
    }, { new: true }, (err, res) => {
        if(err) return cb(err);
        return cb(null, res);
    });
};
schema.statics.publishing = async function(id, publishingStatus, cb) {
    const publishing = await PublishingStatus.findById(publishingStatus);
    if(!!publishing) {
        PostCategory.findByIdAndUpdate(id, {
            $set: {
                publishingStatus
            }
        }, { new: true }, (err, res) => {
            if(err) return cb(err);
            return cb(null, res);
        });
    } else return cb({
        message: 'The publishing status not found'
    });
};

const PostCategory =  mongoose.model('post_categories', schema);
module.exports = PostCategory;
// module.exports.MULTILINGUAL_FIELDS =  MULTILINGUAL_FIELDS;