const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const mongoosePaginate = require('mongoose-paginate');
const { mapRefModels, translateFields } = require('../lib/mongo.plugin');
const config = require('config');
const defaultLanguage = config.get('defaultLanguage');
const { getDataFunc, generateIdSync, getContentByLanguage, setContentByLanguage, forEachAsync } = require('../lib/helper');
const PropertyType = require('./propertyType');
const PropertyGroup = require('./propertyGroup');
const PropertyUse = require('./propertyUse');
const PropertySovereignty = require('./propertySovereignty');
const Project = require('./project');
const Amenities = require('./amenities');
const Province = require('./province');
const District = require('./district');
const Media = require('./media');
const PropertyStatus = require('./propertyStatus');
const Organization = require('./organization');
const ApprovalStatus = require('./approvalStatus');
const PublishingStatus = require('./publishingStatus');
const View = require('./view');
const Facing = require('./facing');
const Transportation = require('./transportation');
const Currency = require('./currency');
const Unit = require('./unit');
const Credential = require('./credential');

const schema = new Schema({
    _id: { type: String, default: generateIdSync(6) },
    mbnd: String,
    referenceId: String,
    title: { type: String, required: [true, 'Property name is required'] },
    type: { type: String, ref: 'property_types', required: [true, 'Property type is required'] },
    group: { type: String, default: 'sale', ref: 'property_groups' },
    location: {
        address: { type: String },
        district: { type: String, ref: 'districts' },
        province: { type: String, ref: 'provinces' },
        country: { type: String, ref: 'countries' },
        location: {
            latitude: Number,
            longitude: Number
        },
        formattedAddress: { type: String },
    },
    photo: { type: Schema.Types.ObjectId, ref: 'media' },
    photos: {
        type: [{ type: Schema.Types.ObjectId, ref: 'media' }],
        ref: 'media'
    },
    videos: {
        type: [{ type: Schema.Types.ObjectId, ref: 'media' }],
        ref: 'media'
    },
    numOfBedrooms: Number,
    numOfBathrooms: Number,
    floorArea: {
        value: Number,
        unit: { type: String, ref: 'units', required: [true, 'Unit is required'] },
    },
    furniture: { type: Number, ref: 'property_furnitures' },
    uses: { type: Number, ref: 'property_uses' },
    sovereignty: { type: Number, ref: 'property_sovereignties' },
    project: { type: String, ref: 'projects' },
    block: String,
    floor: Number,
    price: {
        value: Number,
        currency: { type: String, ref: 'currencies', required: [true, 'Currency is required'] },
        unit: { type: String, ref: 'units', required: [true, 'Unit is required'] },
    },
    amenities: { type: [{ type: String, ref: 'amenities' }], ref: 'amenities' },
    facing: { type: String, ref: 'facings' },
    views: { type: [{ type: String, ref: 'views' }], ref: 'views' },
    transportations: { type: [{ type: String, ref: 'transportations' }], ref: 'transportations' },
    shortDescription: { type: String, default: '' },
    description: { type: String, default: '' },
    highlight: { type: Boolean, default: false },
    startDate: { type: Date, default: Date.now },
    endDate: Date,
    // status: { type: Number, default: 1, ref: 'property_statuses' },
    agency: { type: String, ref: 'organizations', default: config.get('defaultOrganization.id') },
    active: { type: Boolean, default: true },
    approval: { type: Number, default: 1, ref: 'approval_statuses' },
    approvedBy: { type: Schema.Types.ObjectId, ref: 'users' },
    publishingStatus: { type: Number, required: [true, 'publishingStatus is required'], ref: 'publishing_statuses', validate: {
        isAsync: true,
        validator: async function(value, respond) {
            const publishing = await PublishingStatus.findOne({ _id: value, active: true });
            return !!publishing;
        },
        message: '{VALUE} not found'
    } },
    // managed: { type: Number, default: 1 },
    createdBy: { type: Schema.Types.ObjectId, ref: 'users', required: true },
    updatedBy: { type: Schema.Types.ObjectId, ref: 'users' },
    seller: { type: Schema.Types.ObjectId, ref: 'users' },
    languages: [String],
    websites: {
        type: Array,
        isAsync: true,
        get: async function(v) {
            return await Credential.find({
                'config.properties.ids': {
                    $elemMatch: {
                        $eq: this._id
                    }
                }
            }, 'domain name');
        }
    },
},
{
    timestamps: true
});

schema.plugin(mongoosePaginate);

const PUBLIC_FIELDS = 'referenceId title type group location photos photo videos agency numOfBedrooms numOfBathrooms floorArea furniture uses sovereignty project block floor price amenities shortDescription description createdBy approval approvedBy publishingStatus seller createdAt views transportations facing languages';
const POPULATED_FIELDS = 'type photos photo videos agency group uses furniture sovereignty project amenities location.province location.district approval publishingStatus views transportations facing price.currency price.unit floorArea.unit';
const MULTILINGUAL_FIELDS = 'title shortDescription description type.title group.title uses.title sovereignty.title project.title project.shortDescription project.description amenities.title approval.title publishingStatus.title photos.description photo.description videos.description views.title transportations.title facing.title price.unit.title floorArea.unit.title location.province.name location.district.name';

schema.plugin(mapRefModels(MULTILINGUAL_FIELDS));
schema.plugin(translateFields);

schema.statics.create = function(detail, { language }, cb) {
  
    return new Promise(async (resolve, reject) => {
        let d = new Property(language ? await setContentByLanguage(detail, language, MULTILINGUAL_FIELDS, 'properties') : detail);
        if (!language) {
            d = new Property(await setContentByLanguage(detail, detail.languages.includes(defaultLanguage) ? defaultLanguage : detail.languages[0], MULTILINGUAL_FIELDS, 'properties'));
            detail.languages.map(async (l) => await setContentByLanguage(detail, l, MULTILINGUAL_FIELDS, 'properties'));
        } else d = new Property(await setContentByLanguage(detail, language, MULTILINGUAL_FIELDS, 'properties'));
        // let d = new Property(detail);
        d.save(function(err, doc, affected) {
            if(err) {
                if(cb) resolve(cb(err));
                else reject(err);
            } else {
                if(cb) resolve(cb(null, doc, affected));
                else resolve(affected);
            }
        });
    });
};
schema.statics.getById = function(id, { language }, cb) {
    Property.findById(id, async (err, res) => {
        if(err) return cb(err);
        if(language && !res.toJSON().languages.includes(language)) return cb({
            code: 404,
            message: 'The property not found'
        });
        if(res) res.websites = await res.websites;
        await Property.populate(res, POPULATED_FIELDS);
        // if(language) res = getContentByLanguage(res, language, MULTILINGUAL_FIELDS);
        await Property.populate(res, [
            {
                path: 'createdBy',
                select: 'firstName lastName username email phone photo'
            },
            {
                path: 'updatedBy',
                select: 'firstName lastName username email phone photo'
            },
            {
                path: 'seller',
                select: 'firstName lastName username email phone photo'
            },
            {
                path: 'approvedBy',
                select: 'firstName lastName username email phone photo'
            }
        ]);
        await Media.populate(res, 'createdBy.photo updatedBy.photo seller.photo approvedBy.photo');
        if(language) {
            res = await getContentByLanguage(res, language, MULTILINGUAL_FIELDS, 'properties');
            res = await Property.translateFields(res.toJSON(), language);
        }
        return cb(null, res);
    });
};
schema.statics.get = function(cond, options, cb) {
    const { language } = options;
    const conds = {};
    // if(language) conds['languages'] = language;
    return getDataFunc(
        Property,
        {
            ...cond,
            ...conds
        },
        PUBLIC_FIELDS,
        {
            // sort: { createdAt: -1 },
            sort: { updatedAt: -1 },
            ...options,
            populatedFields: POPULATED_FIELDS,
            multilingualFields: MULTILINGUAL_FIELDS,
            propertyContent: true
        },
        async (err, res) => {
            if(err) return cb(err);
            if(language) res.docs = res.docs.filter(doc => doc.languages.includes(language));
            // await Property.populate(res.docs || res, 'type photos videos agency group uses sovereignty project amenities location.province location.district approval publishingStatus');
            // res.docs = await Amenities.populate(res.docs || res, 'amenities');
            await Property.populate(res.docs || res, [
                {
                    path: 'createdBy',
                    select: 'firstName lastName username email phone photo'
                },
                {
                    path: 'updatedBy',
                    select: 'firstName lastName username email phone photo'
                },
                {
                    path: 'seller',
                    select: 'firstName lastName username email phone photo'
                },
                {
                    path: 'approvedBy',
                    select: 'firstName lastName username email phone photo'
                }
            ]);
            await Media.populate(res.docs || res, 'createdBy.photo updatedBy.photo seller.photo approvedBy.photo');
            res.docs = await Property.translateFields(res.docs || res, language);
            // // res.total = res.docs.length;
            return cb(null, res);
        }
    );
};
schema.statics.getHighlights = function(cond, options, cb) {
    const { language } = options;
    const conds = {};
    // if(language) conds['languages'] = language;
    return getDataFunc(
        Property,
        {
            ...cond,
            ...conds,
            highlight: true
        },
        PUBLIC_FIELDS,
        {
            ...options,
            // sort: { createdAt: -1 },
            sort: { updatedAt: -1 },
            populatedFields: POPULATED_FIELDS,
            multilingualFields: MULTILINGUAL_FIELDS,
            propertyContent: true
        },
        async (err, res) => {
            if(err) return cb(err);
            if(language) res.docs = res.docs.filter(doc => doc.languages.includes(language));
            // await Property.populate(res.docs || res, 'type photos videos agency group uses sovereignty project amenities location.province location.district approval publishingStatus');
            // res.docs = await Amenities.populate(res.docs || res, 'amenities');
            await Property.populate(res.docs || res, [
                {
                    path: 'createdBy',
                    select: 'firstName lastName username email phone photo'
                },
                {
                    path: 'updatedBy',
                    select: 'firstName lastName username email phone photo'
                },
                {
                    path: 'seller',
                    select: 'firstName lastName username email phone photo'
                },
                {
                    path: 'approvedBy',
                    select: 'firstName lastName username email phone photo'
                }
            ]);
            await Media.populate(res.docs || res, 'createdBy.photo updatedBy.photo seller.photo approvedBy.photo');
            res.docs = await Property.translateFields(res.docs || res, language);
            // res.total = res.docs.length;
            return cb(null, res);
        }
    );
};
schema.statics.getMaps = function(cond, options, cb) {
    const { language } = options;
    const { pagination, ...otherOptions } = options;
    const conds = {};
    // if(language) conds['languages'] = language;
    return getDataFunc(
        Property,
        {
            ...cond,
            ...conds
        },
        'title price type amenities location photo photos languages',
        {
            // sort: { createdAt: -1 },
            sort: { updatedAt: -1 },
            ...otherOptions,
            populatedFields: POPULATED_FIELDS,
            multilingualFields: MULTILINGUAL_FIELDS,
            propertyContent: true
        },
        async (err, res) => {
            if(err) return cb(err);
            if(language) res.docs = res.docs.filter(doc => doc.languages.includes(language));
            res.docs = await Property.translateFields(res.docs || res, language);
            // // res.total = res.docs.length;
            return cb(null, res);
        }
    );
};
schema.statics.delete = function(id, cb) {
    Property.findByIdAndUpdate(id, {
        $set: {
            active: false
        }
    }, { new: true }, (err, res) => {
        if(err) return cb(err);
        return cb(null, res);
    });
};
schema.statics.deleteMany = function(deleted, cb) {
    Property.update({
        _id: {
            $in: deleted
        }
    }, {
        $set: {
            active: false
        }
    }, { multi: true }, (err, res) => {
        if(err) return cb(err);
        return cb(null, res);
    });
};
schema.statics.partialUpdate = async function(id, u, opts, cb) {
    const { language } = opts;
    let edited = language ? await setContentByLanguage({...u, _id: id}, language, MULTILINGUAL_FIELDS, 'properties') : u;
    // let edited = {};
    // if(language) {
    //     const multilingualFields = MULTILINGUAL_FIELDS.split(' ');
    //     Object.keys(u).filter(f => !multilingualFields.includes(f)).forEach(f => edited[f] = u[f]);
    //     multilingualFields.forEach(f => {
    //         if(f in u) edited[`${f}.${language}`] = u[f];
    //     });
    // } else edited = u;
    Property.findByIdAndUpdate(id, {
        $set: edited
    }, { new: true }, (err, res) => {
        if(err) return cb(err);
        return cb(null, res);
    });
};
schema.statics.fullUpdate = async function(id, u, opts, cb) {
    const { language } = opts;
    let edited = language ? await setContentByLanguage({...u, _id: id}, language, MULTILINGUAL_FIELDS, 'properties') : u;
    Property.findByIdAndUpdate(id, edited, { new: true }, (err, res) => {
        if(err) return cb(err);
        return cb(null, res);
    });
};
schema.statics.publishing = async function(id, publishingStatus, cb) {
    const publishing = await PublishingStatus.findById(publishingStatus);
    if(!!publishing) {
        Property.findByIdAndUpdate(id, {
            $set: {
                publishingStatus
            }
        }, { new: true }, (err, res) => {
            if(err) return cb(err);
            return cb(null, res);
        });
    } else return cb({
        message: 'The publishing status not found'
    });
};
schema.statics.approvingForWebsites = async function(id, { websites, removedWebsites, updatedBy, seller }, cb) {
    try {
        await forEachAsync(websites, async w => {
            await Credential.findByIdAndUpdate(w, {
                updatedBy,
                $addToSet: {
                    'config.properties.ids': id
                },
            });
        });
        await forEachAsync(removedWebsites, async w => {
            await Credential.findByIdAndUpdate(w, {
                updatedBy,
                $pull: {
                    'config.properties.ids': id
                },
            });
        });
        await Property.findByIdAndUpdate(id, {
            $set: {
                approval: 1,
                seller,
                updatedBy
            }
        })
        return cb(null);
    } catch (err) {
        if(err) return cb(err);
    }
};
schema.statics.approveMany = function(approved, cb) {
    Property.update({
        _id: {
            $in: approved
        }
    }, {
        $set: {
            approval: 1
        }
    }, { multi: true }, (err, res) => {
        if(err) return cb(err);
        return cb(null, res);
    });
};

const Property =  mongoose.model('properties', schema);
module.exports = Property;
module.exports.MULTILINGUAL_FIELDS =  MULTILINGUAL_FIELDS;