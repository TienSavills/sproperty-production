const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const mongoosePaginate = require('mongoose-paginate');
const config = require('config');

const schema = new Schema({
    _id: { type: String },
    name: { type: String, required: [true, 'Group name is required'] },
    // organization: { type: String, ref: 'organizations', default: config.get('defaultOrganization.id') },
    active: { type: Boolean, default: true },
    // createdDate: { type: Date, default: Date.now }
},
{
    timestamps: true
});
schema.plugin(mongoosePaginate);

const PUBLIC_FIELDS = 'name';

schema.statics.get = function(cond, { pagination }, cb) {
    return new Promise(async (resolve, reject) => {
        if(typeof cond === 'string') {
            Group.findById(id, async (err, res) => {
                if(err) {
                    if(cb) resolve(cb(err));
                    else reject(err);
                } else {
                    if(cb) resolve(cb(null, res));
                    else resolve(res);
                }
            });
        } else {
            // let cond = {};
            let sort = { createdAt: -1 };
            if(pagination) {
                let { offset, page, limit } = pagination;
                let opts = {
                    select: PUBLIC_FIELDS,
                    sort,
                    limit
                };
                if(offset) opts['offset'] = offset;
                else opts['page'] = page;
                let res = await Group.paginate(cond, opts);
                if(cb) resolve(cb(null, res));
                else resolve(res);
            } else Group.find(cond, PUBLIC_FIELDS, { sort }, async (err, res) => {
                if(err) {
                    if(cb) resolve(cb(err));
                    else reject(err);
                } else {
                    if(cb) resolve(cb(null, res));
                    else resolve(res);
                }
            });
        }
    });
};

const Group =  mongoose.model('groups', schema);
module.exports = Group;
// module.exports.MULTILINGUAL_FIELDS =  MULTILINGUAL_FIELDS;