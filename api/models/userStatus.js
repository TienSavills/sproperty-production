const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate');
const Schema = mongoose.Schema;
const { getDataFunc, getByLanguage, setByLanguage, forEachAsync } = require('../lib/helper');

const schema = new Schema({
    _id: { type: String },
    title: { type: String, required: true },
    description: { type: String, default: '' },
    active: { type: Boolean, default: true }
},
{
    timestamps: true
});
schema.plugin(mongoosePaginate);

const PUBLIC_FIELDS = 'title description';

schema.statics.create = function(detail, { language }, cb) {
// schema.statics.create = function(detail, cb) {
    return new Promise((resolve, reject) => {
        // let d = new UserStatus(language ? await setByLanguage(detail, language, MULTILINGUAL_FIELDS, 'user_statuses') : detail);
        let d = new UserStatus(detail);
        d.save(function(err, doc, affected) {
            if(err) {
                if(cb) resolve(cb(err));
                else reject(err);
            } else {
                if(cb) resolve(cb(null, doc, affected));
                else resolve(affected);
            }
        });
    });
};
schema.statics.get = function(conds, cb) {
    return UserStatus.find(conds, PUBLIC_FIELDS, {sort: { title: 1 }}, async (err, res) => {
        if(err) return cb(err);
        return cb(null, res);
    });
    // return getDataFunc(
    //     UserRole,
    //     conds,
    //     PUBLIC_FIELDS,
    //     {
    //         ...options,
    //         sort: { name: 1 }
    //     },
    //     async (err, res) => {
    //         await UserRole.populate(res, 'grant.module');
    //         // await User.populate(res.docs || res, {
    //         //     path: 'createdBy',
    //         //     select: 'fullname username email phone photo'
    //         // });
    //         console.log(res);
    //         cb(err, res);
    //     }
    // );
};

const UserStatus =  mongoose.model('user_statuses', schema);
module.exports = UserStatus;
// module.exports.MULTILINGUAL_FIELDS =  MULTILINGUAL_FIELDS;