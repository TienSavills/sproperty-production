const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const schema = new Schema({
    _id: { type: Number },
    name: { type: String, required: true },
    active: { type: Boolean, default: true }
},
{
    timestamps: true
});

const PUBLIC_FIELDS = 'name';
// const MULTILINGUAL_FIELDS = ''

schema.pre('save', function(next) {
    let p = this;
    if(!p.isModified('_id')) next();
    Country.find((err, res) => {
        console.log(res);
        if(err) next(err);
        else if(res.length > 0) p._id = res[0]._id + 1;
        next();
    }).sort({ _id : -1 }).limit(1);
});
schema.statics.getCountries = function(cb) {
    Country.find({}, null, {sort: {name: 1}}, async function(err, res) {
        if(err) return cb(err);
        return cb(null, res);
    });
};
schema.statics.import = function(data, cb) {
    Country.insertMany(data, (err, docs) => {
        if(err) return cb(err);
        else return cb(null, docs, docs.length);
    });
};

const Country =  mongoose.model('countries', schema);
module.exports = Country;
// module.exports.MULTILINGUAL_FIELDS =  MULTILINGUAL_FIELDS;