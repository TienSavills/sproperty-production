const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const mongoosePaginate = require('mongoose-paginate');
const { getDataFunc } = require('../lib/helper');

const schema = new Schema({
    // _id: String,
    refId: {
        type: Schema.Types.Mixed,
        refPath: 'refModel'
    },
    refModel: String,
    key: String,
    value: String,
    language: String,
    active: { type: Boolean, default: true }
},
{
    timestamps: true
});
schema.plugin(mongoosePaginate);

const PUBLIC_FIELDS = 'refId refModel key value language';

schema.statics.create = function(detail, cb) {
    return new Promise((resolve, reject) => {
        let d = new Dictionary(detail);
        d.save(function(err, doc, affected) {
            if(err) {
                if(cb) resolve(cb(err));
                else reject(err);
            } else {
                if(cb) resolve(cb(null, doc, affected));
                else resolve(affected);
            }
        });
    });
};
schema.statics.get = function(cond, options, cb) {
    return getDataFunc(
        Dictionary,
        cond,
        PUBLIC_FIELDS,
        {
            ...options,
            sort: { refModel: 1 }
        },
        async (err, res) => {
            if(err) return cb(err);
            return cb(null, res);
        }
    );
};
schema.statics.import = function(data, cb) {
    Dictionary.insertMany(data, (err, docs) => {
        if(err) return cb(err);
        else return cb(null, docs, docs.length);
    });
};
schema.statics.translate = function(target, cb) {
    return new Promise((resolve, reject) => {
        const { refId, key, value, language, refModel, _id, ...otherProps } = target;
        Dictionary.findOneAndUpdate({
            refId,
            key,
            language,
            refModel
        }, {
            $set: {
                refId,
                key,
                value,
                language,
                refModel,
                ...otherProps,
                active: true
            }
        }, {
            upsert: true,
            new: true
        }, (err, res) => {
            if(err) {
                if(cb) return resolve(cb(err));
                else return reject(err);
            } else {
                if(cb) return resolve(cb(null, res));
                else return resolve(res);
            }
        });
    });
};
schema.statics.delete = function(id, cb) {
    Dictionary.findByIdAndRemove(id, (err, res) => {
        if(err) return cb(err);
        return cb(null, res);
    });
};
schema.statics.deleteMany = function(deleted, cb) {
    Dictionary.remove({
        _id: {
            $in: deleted
        }
    }, (err, res) => {
        if(err) return cb(err);
        return cb(null, res);
    });
};

const Dictionary =  mongoose.model('content_dictionaries', schema);
module.exports = Dictionary;
// module.exports.MULTILINGUAL_FIELDS =  MULTILINGUAL_FIELDS;