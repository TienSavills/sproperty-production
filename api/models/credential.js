const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate');
const Schema = mongoose.Schema;
const { getDataFunc, getByLanguage, setByLanguage, forEachAsync } = require('../lib/helper');

const schema = new Schema({
    domain: { type: String, required: [true, 'Restricted domain is required'] },
    apiKey: { type: String, required: true },
    name: { type: String, required: [true, 'Key name is required'] },
    config: {
        projects: {
            ids: [String],
            types: [String],
            provinces: [String],
            highlights: [String],
            hots: [String],
            promotions: [String]
        },
        properties: {
            ids: [String],
            types: [String],
            provinces: [String],
            highlights: [String],
            hots: [String],
            promotions: [String]
        },
    },
    enabled: { type: Boolean, default: true },
    createdBy: { type: Schema.Types.ObjectId, ref: 'users', required: true },
    updatedBy: { type: Schema.Types.ObjectId, ref: 'users' },
    private: { type: Boolean, default: true },
    active: { type: Boolean, default: true }
},
{
    timestamps: true
});
schema.plugin(mongoosePaginate);

const PUBLIC_FIELDS = 'domain apiKey name enabled';

schema.statics.create = function(detail, cb) {
    return new Promise((resolve, reject) => {
        let d = new Credential(detail);
        d.save(function(err, doc, affected) {
            if(err) {
                if(cb) resolve(cb(err));
                else reject(err);
            } else {
                if(cb) resolve(cb(null, doc, affected));
                else resolve(affected);
            }
        });
    });
};
schema.statics.get = function(conds, options, cb) {
    return getDataFunc(
        Credential,
        conds,
        PUBLIC_FIELDS,
        {
            ...options,
            sort: { createdAt: 1 },
        },
        async (err, res) => {
            if(err) return cb(err);
            await Credential.populate(res.docs || res, [
                {
                    path: 'createdBy',
                    select: 'firstName lastName username email phone photo'
                },
                {
                    path: 'updatedBy',
                    select: 'firstName lastName username email phone photo'
                },
            ]);
            cb(err, res);
        }
    );
};
schema.statics.getById = function(id, cb) {
    return Credential.findById(id, PUBLIC_FIELDS, async (err, res) => {
        if(err) return cb(err);
        await Credential.populate(res, [
            {
                path: 'createdBy',
                select: 'firstName lastName username email phone photo'
            },
            {
                path: 'updatedBy',
                select: 'firstName lastName username email phone photo'
            },
        ]);
        return cb(null, res);
    });
};
schema.statics.partialUpdate = function(id, u, cb) {
    Credential.findByIdAndUpdate(id, {
        $set: u
    }, { new: true }, (err, res) => {
        if(err) return cb(err);
        return cb(null, res);
    });
};
schema.statics.delete = function(id, cb) {
    Credential.findByIdAndRemove(id, (err, res) => {
        if(err) return cb(err);
        return cb(null, res);
    });
};
schema.statics.deleteMany = function(deleted, cb) {
    Credential.remove({
        _id: {
            $in: deleted
        }
    }, (err, res) => {
        if(err) return cb(err);
        return cb(null, res);
    });
};
schema.statics.verifyApiKey = function(apiKey, cb) {
    Credential.findOne({ apiKey }, PUBLIC_FIELDS + ' config', function(err, res) {
        if(err) return cb(err);
        else if(res) return cb(null, res);
        else return cb({
            code: 498,
            message: 'Invalid API key'
        });
    });
};

const Credential =  mongoose.model('credentials', schema);
module.exports = Credential;
// module.exports.MULTILINGUAL_FIELDS =  MULTILINGUAL_FIELDS;