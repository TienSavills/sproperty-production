const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate');
const bcrypt = require('bcrypt');
const crypto = require('crypto');
const Schema = mongoose.Schema;
const { mapRefModels, translateFields } = require('../lib/mongo.plugin');
// const Gender =  require('./gender');
const UserRole =  require('./userRole');
const UserStatus =  require('./userStatus');
const Module =  require('./module');
const Group =  require('./group');
// const Organization =  require('./organization');
const SALT_WORK_FACTOR = 10;
const { getDataFunc, getByLanguage, setByLanguage, forEachAsync } = require('../lib/helper');
const UserLoginLocation = require('./userLoginLocation');
const Organization = require('./organization');
const Media = require('./media');
const config = require('config');
const Credential = require('./credential');
const defaultLanguage = config.get('defaultLanguage');

const schema = new Schema({
    firstName: { type: String, required: [true, 'Firstname is required'] },
    lastName: {
        type: String,
        required: [
            function() {
                return !this.firstName
            },
            'Lastname is required'
        ]
    },
    username: { type: String, lowercase: true, trim: true },
    gender: { type: String, enum: ['male', 'female'], default: 'male' },
    password: { type: String, minlength: 6, required: [true, 'Password is required'] },
    email: {
        type: String,
        required: true,
        lowercase: true,
        validate: [
            {
                validator: value => /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/.test(value),
                message: '{VALUE} is invalid'
            },
            {
                isAsync: true,
                validator: async function(value, respond) {
                    // const isValid = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/.test(value);
                    // respond(isValid, 'Email address is invalid');
                    const user = await User.findOne({ email: value, active: true });
                    return !user;
                },
                message: '{VALUE} already exists'
            }
        ]
    },
    dateOfBirth: {
        date: Number,
        month: Number,
        year: Number
    },
    phone: {
        type: String,
        validate: {
            isAsync: true,
            validator: async function(value, respond) {
                // const isValid = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/.test(value);
                // respond(isValid, 'Email address is invalid');
                const user = await User.findOne({ phone: value, active: true });
                return !user;
            },
            message: '{VALUE} already exists'
        }
    },
    address: {
        address: { type: String },
        district: { type: String, ref: 'districts' },
        province: { type: String, ref: 'provinces' },
        country: { type: String, ref: 'countries' },
        location: {
            latitude: Number,
            longitude: Number
        }
    },
    photo: { type: Schema.Types.ObjectId, ref: 'media' },
    roles: { type: [{ type: String, ref: 'user_roles' }], ref: 'user_roles', required: [true, 'User role is required'] },
    agency: { type: String, ref: 'organizations', default: config.get('defaultOrganization.id') },
    lock: { type: Boolean, default: false },
    status: { type: String, ref: 'user_statuses', default: 'new' },
    createdBy: { type: Schema.Types.ObjectId, ref: 'users' },
    active: { type: Boolean, default: true },
    websites: {
        type: Array,
        isAsync: true,
        get: async function(v) {
            return await Credential.find({
                '$or': [
                    {
                        createdBy: this._id
                    },
                    {
                        private: false
                    }
                ]
            }, 'domain name private');
        }
    },
},
{
    timestamps: true
});
schema.plugin(mongoosePaginate);

const PUBLIC_FIELDS = 'firstName lastName username gender email dateOfBirth phone address photo roles agency lock status websites';
const POPULATED_FIELDS = 'roles';
const MULTILINGUAL_FIELDS = 'roles.name'

schema.plugin(mapRefModels(MULTILINGUAL_FIELDS));
schema.plugin(translateFields);

// schema.path('email')
//     .validate(function(value, respond) {
//         User.findOne({ email: value }, function(err, user) {
//             if(err) throw err;
//             respond(!user);
//         });
//     }, 'Email already exists', 'exists')
//     .validate(function(value, respond) {
//         respond(/^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/.test(value));
//     }, 'Email address is invalid', 'invalid');

schema.pre('save', function(next) {
    let user = this;
    // if(!user.isModified('password')) next();
    bcrypt.genSalt(SALT_WORK_FACTOR, async function(err, salt) {
        if(err) next(err);
        let pwd = user.password; // || await generatePassword(8);
        bcrypt.hash(pwd, salt, function(err, hash) {
            if(err) next(err);
            user.password = hash;
            next();
        });
    });
});

schema.statics.create = function(detail, { language }, cb) {
    return new Promise(async (resolve, reject) => {
        // if(!detail.username) detail.username = detail.email.split('@')[0];
        let d = new User(detail);
        // let d = new User(language ? await setByLanguage(detail, language, MULTILINGUAL_FIELDS, 'users') : detail);
        // if (!language) {
        //     d = new User(await setByLanguage(detail, detail.languages.includes(defaultLanguage) ? defaultLanguage : detail.languages[0], MULTILINGUAL_FIELDS, 'users'));
        //     detail.languages.map(async (l) => await setByLanguage(detail, l, MULTILINGUAL_FIELDS, 'users'));
        // } else d = new User(await setByLanguage(detail, language, MULTILINGUAL_FIELDS, 'users'));
        d.save(function(err, doc, affected) {
            if(err) {
                if(cb) resolve(cb(err));
                else reject(err);
            } else {
                if(cb) resolve(cb(null, doc, affected));
                else resolve(affected);
            }
        });
    });
};
schema.statics.login = function({ username, password, ip, ua }, cb) {
    User.findOne({
        $or: [
            { username: { $eq: username } },
            // { phone: { $eq: username } },
            { email: { $eq: username } }
        ]
    }, function(err, u) {
        if(err) return cb(err);
        if(u !== null) {
            bcrypt.compare(password, u.password, function(err, isMatch) {
                if(err) return cb(err);
                if(isMatch) {
                    crypto.randomBytes(48, function(err, buffer) {
                        if(err) return cb(err);
                        // await UserLoginLocation.removeOtherLocations(u._id);
                        UserLoginLocation.addLocation({
                            user: u._id,
                            ip,
                            accessToken: buffer.toString('hex'),
                            userAgent: ua
                        }, function(err, l) {
                            if(err) return cb(err);
                            return cb(null, l.accessToken, u._id);
                        });
                    });
                }
                else return cb({
                    code: 401,
                    message: 'Password incorrect.'
                });
            });
        }
        else return cb({
            code: 401,
            message: 'Username incorrect.'
        });
    });
};
schema.statics.getById = function(id, { language }, cb) {
    User.findById(id, PUBLIC_FIELDS, async (err, res) => {
        if(err) return cb(err);
        if(res) res.websites = await res.websites;
        await User.populate(res, 'address.province address.district photo roles agency status');
        await Module.populate(res, 'roles.grant.module');
        await Group.populate(res, 'roles.grant.module.group');
        // await User.populate(res, {
        //     path: 'createdBy',
        //     select: 'fullname username email phone photo'
        // });
        if(language) {
            res = await getByLanguage(res, language, MULTILINGUAL_FIELDS, 'users');
            res = await User.translateFields(res.toJSON(), language);
        }
        return cb(null, res);
    });
};
schema.statics.get = function(cond, options, cb) {
    const { language, filters } = options;
    const conds = {};
    // if(language) conds['languages'] = language;
    return getDataFunc(
        User,
        {
            ...cond,
            ...conds
        },
        PUBLIC_FIELDS,
        {
            ...options,
            sort: { firstName: 1 },
            populatedFields: POPULATED_FIELDS,
            multilingualFields: MULTILINGUAL_FIELDS
        },
        async (err, res) => {
            await forEachAsync(res, async element => {
                element.websites = await element.websites;
            });
            await User.populate(res.docs || res, 'address.province address.district photo roles agency status');
            await Module.populate(res.docs || res, 'roles.grant.module');
            await Group.populate(res.docs || res, 'roles.grant.module.group');
            if(filters) {
                if('master' in filters) {
                    if(res.docs) res.docs = res.docs.filter(u => u.roles.some(r => r.master === filters.master))
                    else res = res.filter(u => u.roles.some(r => r.master === filters.master))
                }
                if('system' in filters) {
                    if(res.docs) res.docs = res.docs.filter(u => u.roles.some(r => !filters.system ? r.level > 0 : r.level === 0))
                    else res = res.filter(u => u.roles.some(r => !filters.system ? r.level > 0 : r.level === 0))
                }
            }
            // await User.populate(res.docs || res, {
            //     path: 'createdBy',
            //     select: 'fullname username email phone photo'
            // });
            res.docs = await User.translateFields(res.docs || res, language);
            // res.total = res.docs.length;
            cb(err, res);
        }
    );
};
schema.statics.updateDetail = async function(id, u, opts, cb) {
    const { password, ...d } = u;
    // const { language } = opts;
    // let edited = language ? await setByLanguage({...u, _id: id}, language, MULTILINGUAL_FIELDS, 'users') : u;
    User.findByIdAndUpdate(id, {
        $set: d
    }, { new: true }, (err, res) => {
        if(err) return cb(err);
        return cb(null, res);
    });
};
schema.statics.changePassword = function(token, oldPwd, newPwd, cb) {
    User.findOne({
        accessToken: token
    }, function(err, u) {
        if(err) return cb(err);
        if(u !== null) {
            bcrypt.compare(oldPwd, u.password, function(err, isMatch) {
                if(err) return cb(err);
                if(isMatch) {
                    bcrypt.genSalt(SALT_WORK_FACTOR, function(err, salt) {
                        if(err) return cb(err);
                        bcrypt.hash(newPwd, salt, function(err, hash) {
                            if(err) next(err);
                            User.findByIdAndUpdate(u._id, {
                                password: hash
                            }, { new: true }, (err, res) => {
                                if(err) return cb(err);
                                return cb(null, res);
                            });
                        });
                    });
                }
                else return cb({message: 'Mật khẩu hiện tại không đúng.'});
            });
        }
        else return cb({message: 'Token không hợp lệ. Vui lòng kiểm tra lại.'});
    });
};
schema.statics.verifyToken = function(accessToken, cb) {
    UserLoginLocation.findOne({ accessToken: accessToken }, function(err, res) {
        if(err) return cb(err);
        else if(res) {
            if(res.expires.getTime() >= Date.now()) {
                User.findById(res.user, PUBLIC_FIELDS, async function(err, res) {
                    if(err) return cb(err);
                    else if(res) {
                        await User.populate(res, 'address.province address.district photo roles agency status');
                        await Module.populate(res, 'roles.grant.module');
                        await Group.populate(res, 'roles.grant.module.group');
                        return cb(null, res);
                        // res = await UserRole.populate(res, { path: 'roles' });
                        // res = await Module.populate(res, { path: 'roles.grant.module' });
                    } else return cb({
                        code: 498,
                        message: 'Invalid Token'
                    });
                })
            } else return cb({
                code: 498,
                message: 'Token expired'
            });
            // return cb(null, res.user);
        } else return cb({
            code: 498,
            message: 'Invalid Token'
        });
    });
};
schema.statics.resetPassword = function(id, newPassword, cb) {
    bcrypt.genSalt(SALT_WORK_FACTOR, async function(err, salt) {
        if(err) next(err);
        bcrypt.hash(newPassword, salt, function(err, hash) {
            if(err) next(err);
            User.findByIdAndUpdate(id, {
                $set: {
                    password: hash
                }
            }, { new: true }, (err, res) => {
                if(err) return cb(err);
                return cb(null, res);
            });
        });
    });
};
schema.statics.lock = function(id, cb) {
    User.findByIdAndUpdate(id, {
        $set: {
            lock: true
        }
    }, { new: true }, (err, res) => {
        if(err) return cb(err);
        return cb(null, res);
    });
};
schema.statics.unlock = function(id, cb) {
    User.findByIdAndUpdate(id, {
        $set: {
            lock: false
        }
    }, { new: true }, (err, res) => {
        if(err) return cb(err);
        return cb(null, res);
    });
};
schema.statics.delete = function(id, cb) {
    User.findByIdAndUpdate(id, {
        $set: {
            active: false
        }
    }, { new: true }, (err, res) => {
        if(err) return cb(err);
        return cb(null, res);
    });
};
schema.statics.deleteMany = function(deleted, cb) {
    User.update({
        _id: {
            $in: deleted
        }
    }, {
        $set: {
            active: false
        }
    }, { multi: true }, (err, res) => {
        if(err) return cb(err);
        return cb(null, res);
    });
};
schema.statics.createAdmin = function({ id, roles }, cb) {
    User.findByIdAndUpdate(id, {
        $set: {
            roles
        }
    }, { new: true }, (err, res) => {
        if(err) return cb(err);
        return cb(null, res);
    });
};
schema.statics.updateAdmin = function({ id, roles }, cb) {
    User.findByIdAndUpdate(id, {
        $set: {
            roles
        }
    }, { new: true }, (err, res) => {
        if(err) return cb(err);
        return cb(null, res);
    });
};

const User =  mongoose.model('users', schema);
module.exports = User;
// module.exports.MULTILINGUAL_FIELDS =  MULTILINGUAL_FIELDS;