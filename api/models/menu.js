const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const mongoosePaginate = require('mongoose-paginate');
const { getDataFunc } = require('../lib/helper');
const MenuType = require('./menuType');

const menuItemSchema = new Schema({
    text: { type: String, required: [true, 'Menu text is required'] },
    url: { type: String, required: [true, 'Menu link is required'] },
    parent: Schema.Types.ObjectId
});

const schema = new Schema({
    name: { type: String, required: [true, 'Menu name is required'] },
    type: { type: String, ref: 'menu_types' },
    language: { type: String, ref: 'languages' },
    items: [menuItemSchema],
    active: { type: Boolean, default: true }
},
{
    timestamps: true
});
schema.plugin(mongoosePaginate);

const PUBLIC_FIELDS = 'name type language items createdAt';
const POPULATED_FIELDS = 'type language';

schema.statics.create = function(detail, cb) {
    return new Promise(async (resolve, reject) => {
        let d = new Menu(detail);
        d.save(function(err, doc, affected) {
            if(err) {
                if(cb) resolve(cb(err));
                else reject(err);
            } else {
                if(cb) resolve(cb(null, doc, affected));
                else resolve(affected);
            }
        });
    });
};

schema.statics.getById = function(id, cb) {
    Menu.findById(id, async (err, res) => {
        if(err) return cb(err);
        await Menu.populate(res, POPULATED_FIELDS);
        return cb(null, res);
    });
};

schema.statics.get = function(cond, options, cb) {
    return getDataFunc(
        Menu,
        cond,
        PUBLIC_FIELDS,
        {
            ...options,
            sort: { type: 1 },
            populatedFields: POPULATED_FIELDS
        },
        (err, res) => {
            // res = getByLanguage(res, language, 'title');
            cb(err, res);
        }
    );
};

schema.statics.delete = function(id, cb) {
    Menu.findByIdAndUpdate(id, {
        $set: {
            active: false
        }
    }, { new: true }, (err, res) => {
        if(err) return cb(err);
        return cb(null, res);
    });
};
schema.statics.deleteMany = function(deleted, cb) {
    Menu.update({
        _id: {
            $in: deleted
        }
    }, {
        $set: {
            active: false
        }
    }, { multi: true }, (err, res) => {
        if(err) return cb(err);
        return cb(null, res);
    });
};
schema.statics.partialUpdate = function(id, edited, cb) {
    Menu.findByIdAndUpdate(id, {
        $set: edited
    }, { new: true }, (err, res) => {
        if(err) return cb(err);
        return cb(null, res);
    });
};
schema.statics.addMenuItem = function(id, menuItem, cb) {
    Menu.findByIdAndUpdate(id, {
        $push: {
            items: menuItem
        }
    }, { new: true }, (err, res) => {
        if(err) return cb(err);
        return cb(null, res);
    });
};
schema.statics.removeMenuItem = function(id, menuItem, cb) {
    Menu.findById(id, function(err, res) {
        if(err) return cb(err);
        let subMenuItems = [];
        if(res) {
            subMenuItems = res.items.filter(mi => mi.parent + '' === menuItem._id).map(mi => mi._id);
            // console.log([...subMenuItems, menuItem._id])
        }
        Menu.findByIdAndUpdate(id, {
            $pull: {
                items: {
                    _id: {
                        $in: [...subMenuItems, menuItem._id]
                    }
                }
            }
        }, { new: true }, function(err, res) {
            if(err) return cb(err);
            return cb(null, res);
        });
    });
};
schema.statics.updateMenuItem = function(id, edited, cb) {
    Menu.findOneAndUpdate({
        _id: mongoose.Types.ObjectId(id),
        'items._id': edited._id
    }, {
        $set: {
            'items.$.text': edited.text,
            'items.$.url': edited.url
        }
    }, { new: true }, (err, res) => {
        if(err) return cb(err);
        return cb(null, res);
    });
};

const Menu =  mongoose.model('menu', schema);
module.exports = Menu;