const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const mongoosePaginate = require('mongoose-paginate');
const config = require('config');
const defaultLanguage = config.get('defaultLanguage');
const { getDataFunc, getByLanguage, setByLanguage } = require('../lib/helper');
const PropertyType = require('./propertyType');
const ListingGroup = require('./listingGroup');
const Province = require('./province');
const Amenities = require('./amenities');
const User = require('./user');
const Organization = require('./organization');

const schema = new Schema({
    fullname: String,
    email: String,
    phone: String,
    type: { type: String, ref: 'property_types' },
    group: { type: String, ref: 'listing_groups' },
    province: { type: String, ref: 'provinces' },
    numOfBathrooms: Number,
    numOfBedrooms: Number,
    floorArea: {
        min: Number,
        max: Number
    },
    price: {
        min: Number,
        max: Number
    },
    title: String,
    photos: [{ type: Schema.Types.ObjectId, ref: 'media' }],
    videos: [{ type: Schema.Types.ObjectId, ref: 'media' }],
    amenities: [{ type: String, ref: 'amenities' }],
    seller: { type: Schema.Types.ObjectId, ref: 'users' },
    locale: { type: String, default: defaultLanguage },
    agency: { type: String, ref: 'organizations', default: config.get('defaultOrganization.id') },
    active: { type: Boolean, default: true }
},
{
    timestamps: true
});

schema.plugin(mongoosePaginate);

const PUBLIC_FIELDS = 'fullname email phone type group province numOfBathrooms numOfBedrooms floorArea price title photos videos amenities locale seller agency createdAt';
const POPULATED_FIELDS = 'type group province photos videos amenities agency';
const MULTILINGUAL_FIELDS = 'type.title group.title amenities.title';

schema.statics.create = function(detail, { language }, cb) {
    return new Promise(async (resolve, reject) => {
        let d = new Listing(language ? await setByLanguage(detail, language, MULTILINGUAL_FIELDS, 'listings') : detail);
        // let d = new Listing({ ...detail, language });
        d.save(function(err, doc, affected) {
            if(err) {
                if(cb) resolve(cb(err));
                else reject(err);
            } else {
                if(cb) resolve(cb(null, doc, affected));
                else resolve(affected);
            }
        });
    });
};

schema.statics.getById = function(id, { language }, cb) {
    Listing.findById(id, async (err, res) => {
        if(err) return cb(err);
        await Listing.populate(res, POPULATED_FIELDS);
        // res = getByLanguage(res, language, MULTILINGUAL_FIELDS);
        await Listing.populate(res, [
            {
                path: 'seller',
                select: 'firstName lastName username email phone photo'
            }
        ]);
        return cb(null, res);
    });
};

schema.statics.get = function(cond, options, cb) {
    const { language } = options;
    return getDataFunc(
        Listing,
        cond,
        PUBLIC_FIELDS,
        {
            ...options,
            sort: { createdAt: -1 },
            populatedFields: POPULATED_FIELDS,
            multilingualFields: MULTILINGUAL_FIELDS
        },
        async (err, res) => {
            await Listing.populate(res.docs || res, [
                {
                    path: 'seller',
                    select: 'firstName lastName username email phone photo'
                }
            ]);
            cb(err, res);
        }
    );
};

schema.statics.forwarding = async function(id, seller, cb) {
    Listing.findByIdAndUpdate(id, {
        $set: {
            seller
        }
    }, { new: true }, (err, res) => {
        if(err) return cb(err);
        return cb(null, res);
    });
};

const Listing =  mongoose.model('listings', schema);
module.exports = Listing;
module.exports.MULTILINGUAL_FIELDS=  MULTILINGUAL_FIELDS;