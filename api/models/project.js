const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const mongoosePaginate = require('mongoose-paginate');
const { mapRefModels, translateFields } = require('../lib/mongo.plugin');
const config = require('config');
const defaultLanguage = config.get('defaultLanguage');
const { getDataFunc, generateIdSync, getContentByLanguage, setContentByLanguage, forEachAsync } = require('../lib/helper');
const ProjectType = require('./projectType');
const Invesloper = require('./invesloper');
const ProjectStatus = require('./projectStatus');
const Amenities = require('./amenities');
const User = require('./user');
const Province = require('./province');
const District = require('./district');
const Media = require('./media');
const Organization = require('./organization');
const ApprovalStatus = require('./approvalStatus');
const PublishingStatus = require('./publishingStatus');
const Currency = require('./currency');
const Unit = require('./unit');
const Transportation = require('./transportation');
const Credential = require('./credential');

const schema = new Schema({
    _id: { type: String, default: generateIdSync(6) },
    referenceId: String,
    title: { type: String, required: [true, 'Project name is required'] },
    type: { type: String, ref: 'project_types', required: [true, 'Project type is required'] },
    numOfProperties: Number,
    location: {
        address: { type: String },
        district: { type: String, ref: 'districts' },
        province: { type: String, ref: 'provinces' },
        country: { type: String, ref: 'countries' },
        location: {
            latitude: Number,
            longitude: Number
        },
        formattedAddress: { type: String },
    },
    photo: { type: Schema.Types.ObjectId, ref: 'media' },
    photos: {
        type: [{ type: Schema.Types.ObjectId, ref: 'media' }],
        ref: 'media'
    },
    videos: {
        type: [{ type: Schema.Types.ObjectId, ref: 'media' }],
        ref: 'media'
    },
    inveslopers: {
        type: [{ type: Schema.Types.ObjectId, ref: 'inveslopers' }],
        ref: 'inveslopers'
    },
    dateOfBuilding: Date,
    dateOfDelivery: Date,
    investment: {
        value: Number,
        currency: { type: String, ref: 'currencies' },
    },
    // numOfBlocks: Number,
    blocks: [String],
    numOfFloors: Number,
    greenSpace: Number,
    densityOfBuilding: Number,
    floorArea: {
        min: {
            value: Number,
            unit: { type: String, ref: 'units', required: [true, 'Unit is required'] },
        },
        max: {
            value: Number,
            unit: { type: String, ref: 'units', required: [true, 'Unit is required'] },
        }
    },
    price: {
        min: {
            value: Number,
            currency: { type: String, ref: 'currencies', required: [true, 'Currency is required'] },
            unit: { type: String, ref: 'units', required: [true, 'Unit is required'] },
        },
        max: {
            value: Number,
            currency: { type: String, ref: 'currencies', required: [true, 'Currency is required'] },
            unit: { type: String, ref: 'units', required: [true, 'Unit is required'] },
        }
    },
    status: { type: Number, default: 1, ref: 'project_statuses' },
    agency: { type: String, ref: 'organizations', default: config.get('defaultOrganization.id') },
    amenities: { type: [{ type: String, ref: 'amenities' }], ref: 'amenities' },
    shortDescription: { type: String, default: '' },
    description: { type: String, default: '' },
    highlight: { type: Boolean, default: false },
    startDate: { type: Date, default: Date.now },
    endDate: Date,
    transportations: {
        type: [{ type: String, ref: 'transportations' }],
        ref: 'transportations'
    },
    active: { type: Boolean, default: true },
    approval: { type: Number, default: 1, ref: 'approval_statuses' },
    approvedBy: { type: Schema.Types.ObjectId, ref: 'users' },
    publishingStatus: { type: Number, required: [true, 'publishingStatus is required'], ref: 'publishing_statuses', validate: {
        isAsync: true,
        validator: async function(value, respond) {
            const publishing = await PublishingStatus.findOne({ _id: value, active: true });
            return !!publishing;
        },
        message: '{VALUE} not found'
    } },
    // managed: { type: Number, default: 1 },
    createdBy: { type: Schema.Types.ObjectId, ref: 'users', required: true },
    updatedBy: { type: Schema.Types.ObjectId, ref: 'users' },
    seller: { type: Schema.Types.ObjectId, ref: 'users' },
    languages: [String],
    websites: {
        type: Array,
        isAsync: true,
        get: async function(v) {
            return await Credential.find({
                'config.projects.ids': {
                    $elemMatch: {
                        $eq: this._id
                    }
                }
            }, 'domain name');
        }
    },
},
{
    timestamps: true
});

schema.plugin(mongoosePaginate);

const PUBLIC_FIELDS = 'referenceId title type agency numOfProperties location photos photo videos inveslopers dateOfBuilding dateOfDelivery investment blocks numOfFloors greenSpace densityOfBuilding floorArea price status amenities shortDescription description transportations createdBy approval approvedBy publishingStatus seller createdAt languages';
const POPULATED_FIELDS = 'type photos photo videos inveslopers status agency amenities location.province location.district approval publishingStatus investment.currency price.min.currency price.max.currency price.min.unit price.max.unit floorArea.min.unit floorArea.max.unit transportations';
const MULTILINGUAL_FIELDS = 'title shortDescription description type.title inveslopers.description status.title amenities.title approval.title publishingStatus.title photos.description photo.description videos.description price.min.unit.title price.max.unit.title floorArea.min.unit.title floorArea.max.unit.title transportations.title location.province.name location.district.name';

schema.plugin(mapRefModels(MULTILINGUAL_FIELDS));
schema.plugin(translateFields);

schema.statics.create = function(detail, { language }, cb) {
    return new Promise(async (resolve, reject) => {
        let d = new Project(language ? await setContentByLanguage(detail, language, MULTILINGUAL_FIELDS, 'projects') : detail);
        if (!language) {
            d = new Project(await setContentByLanguage(detail, detail.languages.includes(defaultLanguage) ? defaultLanguage : detail.languages[0], MULTILINGUAL_FIELDS, 'projects'));
            detail.languages.map(async (l) => await setContentByLanguage(detail, l, MULTILINGUAL_FIELDS, 'projects'));
        } else d = new Project(await setContentByLanguage(detail, language, MULTILINGUAL_FIELDS, 'projects'));
        // let d = new Project(detail);
        d.save(function(err, doc, affected) {
            if(err) {
                if(cb) resolve(cb(err));
                else reject(err);
            } else {
                if(cb) resolve(cb(null, doc, affected));
                else resolve(affected);
            }
        });
    });
};
schema.statics.getById = function(id, { language }, cb) {
    Project.findById(id, async (err, res) => {
        if(err) return cb(err);
        if(language && !res.toJSON().languages.includes(language)) return cb({
            code: 404,
            message: 'The property not found'
        });
        if(res) res.websites = await res.websites;
        await Project.populate(res, POPULATED_FIELDS);
        await Project.populate(res.inveslopers, 'photo');
        await Project.populate(res, [
            {
                path: 'createdBy',
                select: 'firstName lastName username email phone photo'
            },
            {
                path: 'updatedBy',
                select: 'firstName lastName username email phone photo'
            },
            {
                path: 'seller',
                select: 'firstName lastName username email phone photo'
            },
            {
                path: 'approvedBy',
                select: 'firstName lastName username email phone photo'
            }
        ]);
        await Media.populate(res, 'createdBy.photo updatedBy.photo seller.photo approvedBy.photo');
        if(language) {
            res = await getContentByLanguage(res, language, MULTILINGUAL_FIELDS, 'projects');
            res = await Project.translateFields(res.toJSON(), language);
        }
        return cb(null, res);
    });
};
schema.statics.get = function(cond, options, cb) {
    const { language } = options;
    const conds = {};
    // if(language) conds['languages'] = language;
    return getDataFunc(
        Project,
        {
            ...cond,
            ...conds
        },
        PUBLIC_FIELDS,
        {
            ...options,
            // sort: { createdAt: -1 },
            sort: { updatedAt: -1 },
            populatedFields: POPULATED_FIELDS,
            multilingualFields: MULTILINGUAL_FIELDS,
            propertyContent: true
        },
        async (err, res) => {
            // console.log({err, res});
            if(err) return cb(err);
            if(language) res.docs = res.docs.filter(doc => doc.languages.includes(language));
            // await Project.populate(res.docs || res, 'type photos videos inveslopers status agency amenities location.province location.district approval publishingStatus');
            await Project.populate(res.docs || res, [
                {
                    path: 'createdBy',
                    select: 'firstName lastName username email phone photo'
                },
                {
                    path: 'updatedBy',
                    select: 'firstName lastName username email phone photo'
                },
                {
                    path: 'seller',
                    select: 'firstName lastName username email phone photo'
                },
                {
                    path: 'approvedBy',
                    select: 'firstName lastName username email phone photo'
                }
            ]);
            await Media.populate(res.docs || res, 'createdBy.photo updatedBy.photo seller.photo approvedBy.photo');
            res.docs = await Project.translateFields(res.docs || res, language);
            // res.total = res.docs.length;
            return cb(null, res);
        }
    );
};
schema.statics.getHighlights = function(cond, options, cb) {
    const { language } = options;
    const conds = {};
    // if(language) conds['languages'] = language;
    return getDataFunc(
        Project,
        {
            ...cond,
            ...conds,
            highlight: true
        },
        PUBLIC_FIELDS,
        {
            ...options,
            // sort: { createdAt: -1 },
            sort: { updatedAt: -1 },
            populatedFields: POPULATED_FIELDS,
            multilingualFields: MULTILINGUAL_FIELDS,
            propertyContent: true
        },
        async (err, res) => {
            if(err) return cb(err);
            if(language) res.docs = res.docs.filter(doc => doc.languages.includes(language));
            // await Project.populate(res.docs || res, 'type photos videos inveslopers status agency amenities location.province location.district approval publishingStatus');
            await Project.populate(res.docs || res, [
                {
                    path: 'createdBy',
                    select: 'firstName lastName username email phone photo'
                },
                {
                    path: 'updatedBy',
                    select: 'firstName lastName username email phone photo'
                },
                {
                    path: 'seller',
                    select: 'firstName lastName username email phone photo'
                },
                {
                    path: 'approvedBy',
                    select: 'firstName lastName username email phone photo'
                }
            ]);
            await Media.populate(res.docs || res, 'createdBy.photo updatedBy.photo seller.photo approvedBy.photo');
            res.docs = await Project.translateFields(res.docs || res, language);
            // res.total = res.docs.length;
            return cb(null, res);
        }
    );
};
schema.statics.delete = function(id, cb) {
    Project.findByIdAndUpdate(id, {
        $set: {
            active: false
        }
    }, { new: true }, (err, res) => {
        if(err) return cb(err);
        return cb(null, res);
    });
};
schema.statics.deleteMany = function(deleted, cb) {
    Project.update({
        _id: {
            $in: deleted
        }
    }, {
        $set: {
            active: false
        }
    }, { multi: true }, (err, res) => {
        if(err) return cb(err);
        return cb(null, res);
    });
};
schema.statics.partialUpdate = async function(id, u, opts, cb) {
    const { language } = opts;
    let edited = language ? await setContentByLanguage({...u, _id: id}, language, MULTILINGUAL_FIELDS, 'projects') : u;
    // let edited = {};
    // if(language) {
    //     const multilingualFields = MULTILINGUAL_FIELDS.split(' ');
    //     Object.keys(u).filter(f => !multilingualFields.includes(f)).forEach(f => edited[f] = u[f]);
    //     multilingualFields.forEach(f => {
    //         if(f in u) edited[`${f}.${language}`] = u[f];
    //     });
    // } else edited = u;
    Project.findByIdAndUpdate(id, {
        $set: edited
    }, { new: true }, (err, res) => {
        if(err) return cb(err);
        return cb(null, res);
    });
};
schema.statics.fullUpdate = async function(id, u, opts, cb) {
    const { language } = opts;
    let edited = language ? await setContentByLanguage({...u, _id: id}, language, MULTILINGUAL_FIELDS, 'projects') : u;
    Project.findByIdAndUpdate(id, edited, { new: true }, (err, res) => {
        if(err) return cb(err);
        return cb(null, res);
    });
};
schema.statics.publishing = async function(id, publishingStatus, cb) {
    const publishing = await PublishingStatus.findById(publishingStatus);
    if(!!publishing) {
        Project.findByIdAndUpdate(id, {
            $set: {
                publishingStatus
            }
        }, { new: true }, (err, res) => {
            if(err) return cb(err);
            return cb(null, res);
        });
    } else return cb({
        message: 'The publishing status not found'
    });
};
schema.statics.approveMany = function(approved, cb) {
    Project.update({
        _id: {
            $in: approved
        }
    }, {
        $set: {
            approval: 1
        }
    }, { multi: true }, (err, res) => {
        if(err) return cb(err);
        return cb(null, res);
    });
};
schema.statics.approvingForWebsites = async function(id, { websites, removedWebsites, updatedBy, seller }, cb) {
    try {
        await forEachAsync(websites, async w => {
            await Credential.findByIdAndUpdate(w, {
                updatedBy,
                $addToSet: {
                    'config.projects.ids': id
                }
            });
        });
        await forEachAsync(removedWebsites, async w => {
            await Credential.findByIdAndUpdate(w, {
                updatedBy,
                $pull: {
                    'config.projects.ids': id
                },
            });
        });
        await Project.findByIdAndUpdate(id, {
            $set: {
                approval: 1,
                seller,
                updatedBy
            }
        })
        return cb(null);
    } catch (err) {
        if(err) return cb(err);
    }
};

const Project =  mongoose.model('projects', schema);
module.exports = Project;
module.exports.MULTILINGUAL_FIELDS =  MULTILINGUAL_FIELDS;