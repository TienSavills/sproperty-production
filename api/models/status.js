const mongoose = require('mongoose');
const Schema = mongoose.Schema;
// const mongoosePaginate = require('mongoose-paginate');
const config = require('config');
const defaultLanguage = config.get('defaultLanguage');
const { getDataFunc, getByLanguage, setByLanguage, forEachAsync } = require('../lib/helper');

const schema = new Schema({
    _id: Number,
    title: { type: String, default: '' }
});
// schema.plugin(mongoosePaginate);

const PUBLIC_FIELDS = 'title';
const MULTILINGUAL_FIELDS = 'title'

schema.pre('save', function(next) {
    let p = this;
    if(!p.isModified('_id')) next();
    Status.find((err, res) => {
        if(err) next(err);
        else if(res.length > 0) p._id = res[0]._id + 1;
        next();
    }).sort({ _id : -1 }).limit(1);
});

schema.statics.create = function(detail, { language }, cb) {
// schema.statics.create = function(detail, cb) {
    return new Promise(async (resolve, reject) => {
        let d = new Status(language ? await setByLanguage(detail, language, MULTILINGUAL_FIELDS, 'statuses') : detail);
        // let d = new Status(detail);
        d.save(function(err, doc, affected) {
            if(err) {
                if(cb) resolve(cb(err));
                else reject(err);
            } else {
                if(cb) resolve(cb(null, doc, affected));
                else resolve(affected);
            }
        });
    });
};
schema.statics.get = function(cond, options, cb) {
    const { language } = options;
    Status.find(cond, PUBLIC_FIELDS, async (err, res) => {
       if(language) await getByLanguage(res, language, MULTILINGUAL_FIELDS, 'statuses');
        cb(err, res);
    });
};

const Status =  mongoose.model('statuses', schema);
module.exports = Status;
module.exports.MULTILINGUAL_FIELDS =  MULTILINGUAL_FIELDS;