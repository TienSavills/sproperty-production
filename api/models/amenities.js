const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const mongoosePaginate = require('mongoose-paginate');
const config = require('config');
const defaultLanguage = config.get('defaultLanguage');
const { getDataFunc, getByLanguage, setByLanguage } = require('../lib/helper');

const schema = new Schema({
    _id: { type: String },
    code: { type: String, default: '' },
    title: String,
    active: { type: Boolean, default: true }
},
{
    timestamps: true
});
schema.plugin(mongoosePaginate);

const PUBLIC_FIELDS = 'title code';
const MULTILINGUAL_FIELDS = 'title'

schema.statics.create = function(detail, { language }, cb) {
    return new Promise(async (resolve, reject) => {
        let d = new Amenities(language ? await setByLanguage(detail, language, MULTILINGUAL_FIELDS, 'amenities') : detail);
        d.save(function(err, doc, affected) {
            if(err) {
                if(cb) resolve(cb(err));
                else reject(err);
            } else {
                if(cb) resolve(cb(null, doc, affected));
                else resolve(affected);
            }
        });
    });
};
schema.statics.get = function(cond, options, cb) {
    const { language } = options;
    return getDataFunc(
        Amenities,
        cond,
        PUBLIC_FIELDS,
        {
            ...options,
            sort: { title: 1 },
            multilingualFields: MULTILINGUAL_FIELDS
        },
        async (err, res) => {
            if(err) return cb(err);
            return cb(null, res);
        }
    );
};
schema.statics.import = function(data, cb) {
    Amenities.insertMany(data, (err, docs) => {
        if(err) return cb(err);
        else return cb(null, docs, docs.length);
    });
};

const Amenities =  mongoose.model('amenities', schema);
module.exports = Amenities;
module.exports.MULTILINGUAL_FIELDS =  MULTILINGUAL_FIELDS;