const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const mongoosePaginate = require('mongoose-paginate');
const { getDataFunc } = require('../lib/helper');
// const { getByLanguage, setByLanguage } = require('../lib/helper');

const schema = new Schema({
    _id: { type: String },
    // code: String,
    name: { type: String, required: true },
    active: { type: Boolean, default: true },
    status: { type: Number, default: 1 },
});

schema.plugin(mongoosePaginate);

const PUBLIC_FIELDS = 'name';
const MULTILINGUAL_FIELDS = 'name'

// schema.pre('save', function(next) {
//     let p = this;
//     if(!p.isModified('_id')) next();
//     Province.find((err, res) => {
//         if(err) next(err);
//         else if(res.length > 0) p._id = res[0]._id + 1;
//         next();
//     }).sort({ _id : -1 }).limit(1);
// });
schema.statics.get = function(cond, options, cb) {
    return getDataFunc(
        Province,
        {
            ...cond,
            status: 2,
        },
        PUBLIC_FIELDS,
        {
            ...options,
            multilingualFields: MULTILINGUAL_FIELDS,
            sort: { name: 1 }
        },
        (err, res) => cb(err, res)
    );
};
schema.statics.getProvinces = function(cb) {
    Province.find({status: 2}, null, {sort: {name: 1}}, function(err, res) {
        if(err) return cb(err);
        return cb(null, res);
    });
};
schema.statics.getProvince = function(id, cb) {
    Province.findById(id, function(err, res) {
        if(err) return cb(err);
        return cb(null, res);
    });
};
schema.statics.import = function(data, cb) {
    Province.insertMany(data, (err, docs) => {
        if(err) return cb(err);
        else return cb(null, docs, docs.length);
    });
};

const Province =  mongoose.model('provinces', schema);
module.exports = Province;
module.exports.MULTILINGUAL_FIELDS =  MULTILINGUAL_FIELDS;