const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const mongoosePaginate = require('mongoose-paginate');
const config = require('config');
const defaultLanguage = config.get('defaultLanguage');
const { getDataFunc, getByLanguage, setByLanguage } = require('../lib/helper');
const Media = require('./media');

const schema = new Schema({
    name: { type: String, required: [true, 'Invesloper name is required'] },
    description: { type: String, default: '' },
    photo: { type: Schema.Types.ObjectId, ref: 'media' },
    active: { type: Boolean, default: true }
},
{
    timestamps: true
});
schema.plugin(mongoosePaginate);

const PUBLIC_FIELDS = 'name description photo';
const POPULATED_FIELDS = 'photo';
const MULTILINGUAL_FIELDS = 'description';

schema.statics.create = function(detail, { language }, cb) {
    return new Promise(async (resolve, reject) => {
        let d = new Invesloper(language ? await setByLanguage(detail, language, MULTILINGUAL_FIELDS, 'inveslopers') : detail);
        // let d = new Invesloper(detail);
        d.save(function(err, doc, affected) {
            if(err) {
                if(cb) resolve(cb(err));
                else reject(err);
            } else {
                if(cb) resolve(cb(null, doc, affected));
                else resolve(affected);
            }
        });
    });
};
schema.statics.get = function(cond, options, cb) {
    const { language } = options;
    return getDataFunc(
        Invesloper,
        cond,
        PUBLIC_FIELDS,
        {
            ...options,
            sort: { name: 1 },
            multilingualFields: MULTILINGUAL_FIELDS,
            populatedFields: POPULATED_FIELDS
        },
        async (err, res) => {
            if(err) return cb(err);
            await Invesloper.populate(res.docs || res, 'photo');
            return cb(null, res);
        }
    );
};
schema.statics.getById = function(id, { language }, cb) {
    return Invesloper.findById(id, PUBLIC_FIELDS, async (err, res) => {
        if(err) return cb(err);
        await Invesloper.populate(res, POPULATED_FIELDS);
        return cb(null, res);
    });
};
schema.statics.delete = function(id, cb) {
    Invesloper.findByIdAndUpdate(id, {
        $set: {
            active: false
        }
    }, { new: true }, (err, res) => {
        if(err) return cb(err);
        return cb(null, res);
    });
};
schema.statics.deleteMany = function(deleted, cb) {
    Invesloper.update({
        _id: {
            $in: deleted
        }
    }, {
        $set: {
            active: false
        }
    }, { multi: true }, (err, res) => {
        if(err) return cb(err);
        return cb(null, res);
    });
};
schema.statics.partialUpdate = function(id, u, cb) {
    Invesloper.findByIdAndUpdate(id, {
        $set: u
    }, { new: true }, (err, res) => {
        if(err) return cb(err);
        return cb(null, res);
    });
};

const Invesloper =  mongoose.model('inveslopers', schema);
module.exports = Invesloper;
module.exports.MULTILINGUAL_FIELDS =  MULTILINGUAL_FIELDS;