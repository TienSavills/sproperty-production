const User =  require('../models/user');
const Credential =  require('../models/credential');
// const helper = require(`${process.cwd()}/lib/helper`);
const { atob } = require(`${process.cwd()}/lib/common`);

module.exports = {
    authorization: function(req, res, next) {
        try {
            if(typeof req.headers.authorization !== 'undefined') {
                let  auth_str = atob(req.headers.authorization.split(' ')[1]).split(':');
                req.user = {
                    username: auth_str[0],
                    password: auth_str[1]
                };
                return next();
            } else {
                res.status(401).json({
                    message: 'Unauthorized'
                });
            }
        } catch (err) {
            next(err);
        }
    },
    verifyToken: function(req, res, next) {
        try {
            let token = req.headers.token || req.headers['x-access-token'];
            if(typeof token !== 'undefined') {
                User.verifyToken(token, function(err, u) {
                    if(err) {
                        if(typeof err !== 'Error' && typeof err !== 'MongoError') return res.status(err.code || 500).json({ message: err.message || 'Something went wrong' });
                        return res.status(500).json({ message: err.message || 'Something went wrong' });
                    }
                    req.user = u;
                    return next();
                });
            } else {
                res.status(499).json({
                    message: 'Token Required'
                });
            }
        }
        catch (err) {
            next(err);
        }
    },
    verifyApiKey: function(req, res, next) {
        try {
            let hostname = req.headers['origin'] || req.headers['host'] || req.hostname
            // let ip = req.ip
            // let ip = req.headers['origin'] || req.headers['host']
            // ip = ip.replace('https://', '').replace('http://', '')
            // console.log(`req.headers['x-forwarded-for']`, req.headers['x-forwarded-for'])
            // console.log('req.connection.remoteAddress', req.connection.remoteAddress)
            hostname = hostname.split('/')[2] || hostname.split('/')[0]
            hostname = hostname.split(':')[0]
            hostname = hostname.replace(/^www./g, '')
            let token = req.headers['x-api-key']
            if(typeof token !== 'undefined') {
                Credential.verifyApiKey(token, function(err, u) {
                    if(err) {
                        if(typeof err !== 'Error' && typeof err !== 'MongoError') return res.status(err.code || 500).json({ message: err.message || 'Something went wrong' })
                        return res.status(500).json({ message: err.message || 'Something went wrong' })
                    }
                    let domain = u.domain.split(';').map(dm => {
                        // dm = dm.replace('https://', '').replace('http://', '')
                        dm = dm.split('/')[2] || dm.split('/')[0]
                        dm = dm.split(':')[0]
                        dm = dm.replace(/^www./g, '')
                        return dm
                    })
                    // if (domain !== ip) return res.status(403).json({ message: 'Access denied' })
                    // req.credential = u
                    // return next()
                    if(domain.includes(hostname) || domain.includes('localhost')) {
                        req.credential = u
                        return next()
                    } else return res.status(403).json({ message: 'Access denied' })
                });
            } else {
                res.status(499).json({
                    message: 'API key Required'
                })
            }
        }
        catch (err) {
            next(err)
        }
    }
};