module.exports = function(req, res, next) {
    if(req.headers.module) req.module = req.headers.module
    return next()
}