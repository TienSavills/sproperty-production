/**
 * Restrict access.
 * @param {Array<String>} restrictedIP Restricted IP list
 */
module.exports = restrictedIP => (req, res, next) => {
    // var ip = req.ip || req.connection.remoteAddress || req.socket.remoteAddress || req.connection.socket.remoteAddress
    // if(ip.substr(0, 7) === '::ffff:') ip = ip.substr(7)
    // if(ip === '::1') ip = 'localhost'
    var ip = req.headers['origin'] || req.headers['host']
    ip = ip.replace('https://', '').replace('http://', '')
    ip = ip.split(':')[0]
    // console.log('IP: ', ip)
    if (restrictedIP.indexOf(ip) === -1) return res.end()
    else return next()
}