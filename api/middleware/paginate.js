module.exports = function(req, res, next) {
    let offset = req.query.offset || req.headers.offset
    let page = req.query.page || req.headers.page
    let limit = req.query.limit || req.headers.limit
    
    req.pagination = {}

    if(!offset && !page && !limit) {
        req.pagination.page = 1
        req.pagination.limit = 10
    } else {
        if(offset) req.pagination.offset = parseInt(offset)
        if(page) req.pagination.page = parseInt(page)
        if(limit) req.pagination.limit = parseInt(limit)
    }
    return next()
}