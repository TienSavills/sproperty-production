module.exports = function(req, res, next) {
    const { page, limit, offset, ...otherQueries } = req.query
    let queries = {}
    Object.keys(otherQueries).forEach(f => {
        queries[f] = decodeURIComponent(req.query[f])
        // if(queries[f].length > 0 && !isNaN(queries[f])) queries[f] = new Number(queries[f]).valueOf()
    })
    req.queries = queries
    return next()
}