module.exports = {
    authorize: ({ role, module: moduleId, permission }) => (req, res, next) => {
        const { user } = req
        if(user && user.roles) {
            if(role) {
                if(user.roles.some(r => r._id === role)) {
                    req.master = user.roles.find(r => r._id === role).master
                    return next()
                } else res.status(403).json({
                    message: 'Access denied'
                })
            } else {
                if(user.roles.some(r => r.level === 0)) {
                    req.master = user.roles.find(r => r.level === 0).master
                    return next()
                } else {
                    if(user.roles.some(r => r.grant.some(g => g.module._id === moduleId))) {
                        req.master = user.roles.find(r => r.grant.some(g => g.module._id === moduleId)).master
                        return next()
                    } else res.status(403).json({
                        message: 'Access denied'
                    })
                }
            }
        } else res.status(403).json({
            message: 'Access denied'
        })
    }
}