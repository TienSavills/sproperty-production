const config = require('config')

module.exports = function(req, res, next) {
    if(req.headers.language) {
        req.language = req.headers.language || config.get('defaultLanguage')
        return next()
    } else {
        return res.status(400).json({
            message: 'Language required'
        })
    }
}