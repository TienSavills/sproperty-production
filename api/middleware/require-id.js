module.exports = function(req, res, next) {
    if(req.params.id !== 'undefined' && typeof req.params.id !== 'undefined') {
        req.id = req.params.id
        return next()
    } else {
        return res.status(400).json({
            message: 'ID required'
        })
    }
}