const recaptcha = require(`${process.cwd()}/lib/utils/recaptcha`);

module.exports = {
    verify: async function(req, res, next) {
        try {
            if (req.body.captchaEnabled === true) {
                const data = await recaptcha.verify(req.body.captchaResponse);
                const result = JSON.parse(data);

                if (result.success && result.score >= 0.5) return next();
                else res.status(500).json({
                    message: result['error-codes']
                });
            }
            next();
        } catch (err) {
            next(err);
        }
    }
}