module.exports = {
    errorHandler: function(err, req, res, next) {
        if(typeof err !== 'Error' && typeof err !== 'MongoError') {
            // console.error(err.message);
            res.status(err.code || 500).json({
                message: err.message || 'Something went wrong'
            });
        } else {
            console.error(err.stack);
            res.status(500).json({
                message: 'Something went wrong'
            });
        }
    }
};