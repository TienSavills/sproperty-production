const config = require('config')

module.exports = function(req, res, next) {
    req.language = req.headers.language || config.get('defaultLanguage')
    next()
}