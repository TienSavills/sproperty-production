module.exports = data => (req, res, next) => {
    Object.keys(data).forEach(k => {
        req[k] = data[k]
    })
    return next()
}