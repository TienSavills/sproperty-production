module.exports = {
    authorize: allowedRoles => (req, res, next) => {
        const { user } = req
        if(user && user.roles && user.roles.some(r => allowedRoles.includes(r._id || r))) return next()
        else res.status(403).json({
            message: 'Access denied'
        })
    }
}