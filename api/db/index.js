( async function() {
	'use strict';

	const dbConnect = require('./connect');
	const db = await dbConnect();
	//model register
	const Status =  require('../models/status');

	module.exports = db;
})();