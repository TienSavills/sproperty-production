'use strict';

const config = require('config');
const dbConfig = config.get('database');
const mongoose = require('mongoose');

const options = {
    server: { auto_reconnect: true }
};

if(!!dbConfig.user && !!dbConfig.pwd) {
    options.user = dbConfig.user;
    options.pass = dbConfig.pwd;
}

const connect = (cb = null) => {
    return new Promise((resolve, reject) => {
        mongoose.connect(`${dbConfig.protocol}://${dbConfig.host}:${dbConfig.port}/${dbConfig.database}`, options, (err, db) => {
            if (err) {
                console.log('Connect to database failure');
                if(cb) resolve(cb(err));
                else reject(err);
            } else {
                console.log('Connection to database established');
                if(cb) resolve(cb(null, mongoose.connection));
                else resolve(mongoose.connection);
            }
        });
    });
};

module.exports = connect;