# Documentation

- [Landing Page](#Landing)
- [Admin Page (Portal)](#Admin)
- [API Gateway](#API)
- [Environment Variables](#environment-variables)
- [Configurations](#configurations)
- [Deployment](#deployment)
---
# Technical
Project         | Technical
-------         | ---------
Landing page    | React, Redux, Material-UI
Admin page      | React, Redux, Material-UI
API gateway     | Node, Express, MongoDB

---
# Landing
Source code was built as production mode. 
- Deployment: Copy source and put it in IIS folder or smt like that

# Admin Page:
Source code was built as production mode. 
- Deployment: Copy source and put it in IIS folder or smt like that

# API gateway:
```sh
npm install
npm run build
node server

NOTED: edit env to production mode
```
---
## PM2
Install PM2 have not installed yet.
```sh
npm install -g pm2
```
Chỉnh sửa file `ecosystem.config.js` ở thư mục project:
```json
module.exports = {
  apps : [{
    name: 'savills-server',
    script: './server.js',
    mode: "cluster",
    instances: "max",
    env: {
      NODE_ENV: 'development'
    },
    env_production: {
      NODE_ENV: 'production'
    },
    output: './output.log',
    error: './error.log',
    merge_logs: true
  }]
}
```
Chạy lệnh sau:
```
pm2 start
```
Update code:
```
npm run build
pm2 reload savills-server
